desc 'index merchant data'
task run_data_indexing: :environment do
	account = Account.find(ENV['ACCOUNT_ID'])	
	account.start_indexing
end


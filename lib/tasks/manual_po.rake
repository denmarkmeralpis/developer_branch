desc 'manual purchase order'
task run_manual_po: :environment do
	account = Account.find(ENV['ACCOUNT_ID'])
	account.manual_po(ENV['USER_BRANCHES'].split(','), ENV['USER_ID'], ENV['EMAIL'])
end

desc 'manual purchase order per branch'
task run_manual_po_per_branch: :environment do
	account = Account.find(ENV['ACCOUNT_ID'])
	account.manual_po_per_branch(ENV['BRANCH_ID'], ENV['USER_ID'], ENV['EMAIL'], ENV['USER_SUPPLIERS'].split(','))
end
# noinspection ALL
module Import::ImportBranches

  def start_import_branches(fileName = 'users.csv')

    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Users"
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile   = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })

    start_log file_name: fileName
    timeStart = Time.now

    # User(id: integer, parent_id: integer, supervisor_id: integer, account_id: integer, email: string, password_hash: string, password_salt: string, reset_password_token: string, first_name: string, last_name: string, role: string, status: string, user_access: string, created_at: datetime, updated_at: datetime)
    # Role(id: integer, user_id: integer, name: string)
    # rpo, apo, cpo, epo, ssu, spr
    
    #first_name,last_name,email,password,user_types,supervisor,confirm_po,approve_void_pos,copy_po,edit_price_in_po,suspend_suppliers,suspend_products
    # Start of Branch CSV Import
   
    csvFile.each_with_index do |row, i|
      #company = self.companies.find_by_code(row[:company_code])
      #if company
      parent_id = @account.uers.first.id
      if (row[:user_type] == 'buyer') ? supervisor_id = @account.users.find_by_email(row[:supervisor_email]).id : supervisor_id = nil

      UserAttr = {
          first_name: row[:first_name],
          last_name: row[:last_name],
          parent_id: parent_id,
          supervisor_id: supervisor_id,
          account_id: @account.id,
          email: row[:email],
          password: row[:password]
      }
      
      user = self.user.create(UserAttr)

      if (row[:can_confirm_po] == 'YES')
        self.role.create(:user_id => user.id, :name => 'rpo' )
      if (row[:can_approve_void_po] == 'YES')
        self.role.create(:user_id => user.id, :name => 'apo' )
      if (row[:can_copy_po] == 'YES')
        self.role.create(:user_id => user.id, :name => 'cpo' )
      if (row[:can_edit_price_in_po] == 'YES')
        self.role.create(:user_id => user.id, :name => 'epo' )
      if (row[:can_suspend_suppliers] == 'YES')
          self.role.create(:user_id => user.id, :name => 'ssu' )
      if (row[:can_suspend_products] == 'YES')
        self.role.create(:user_id => user.id, :name => 'spr' )

    end

    # End of User CSV Import

    timeEnd   = Time.now
    stop_log file_name: fileName
    FileUtils.mv(filePath, self.archives_dir)
    "Import Branch completed. Time spend #{timeEnd - timeStart} seconds"

  end

end
class MerchantMailer < ActionMailer::Base

  default from: 'cloudbuynotify@cryodata.it'

  def welcome_mailer(merchant, subdomain, password)
    @user     = merchant
    @url      = "#{root_url(subdomain: subdomain)}"
    @password = password
    mail(to: @user.email, subject: 'Welcome to CloudBuy', bcc: 'denmark@nueca.net')
  end

  def reset_password(email, newPassword, subdomain)
    @passwordResetLink = recover_password_user_url(subdomain: subdomain, id: newPassword)
    mail(to: email, subject: 'Recovery Password')
  end

  def purchase_order(email)
    @email = email
    mail(to: email, subject: 'Purchase Order Status', cc: 'denmark@nueca.net')
  end

end

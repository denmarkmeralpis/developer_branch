module FastImport::FastImportProductCategories

  def fi_product_categories(fileName = 'product_categories_?????????????????.csv')
    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath2 = Dir.glob("#{temp_filePath}")

    filePath2.each do |filePath|

        puts "-----------------------------------------------------------"
        puts "PRODUCT CATEGORY IMPORT STARTED"
        puts "-----------------------------------------------------------"

        columns = [:id, :account_id, :code, :name]
        values  = []

        csv_file = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })
        csv_file.each_with_index { |row, i| values.push(["#{self.id}-#{row[:code]}", self.id, row[:code], row[:name]]) }

        ProductCategory.import columns, values, { validate: false, on_duplicate_key_update: [:name] }
        puts ProductCategory.last.name
        FileUtils.mv(filePath, self.archives_dir)
    end
  end

end
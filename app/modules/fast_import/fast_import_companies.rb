module FastImport::FastImportCompanies
	
	def fi_companies(file_name = 'companies_?????????????????.csv')
		temp_filePath ||= File.join(self.account_dir, 'uploads', file_name)
	   filePath2 = Dir.glob("#{temp_filePath}")

	   filePath2.each do |filePath|

		   puts "-----------------------------------------------------------"
		   puts "COMPANY IMPORT STARTED"
		   puts "-----------------------------------------------------------"

		   columns  = [:id, :account_id, :code, :name, :reference, :status, :address, :telephone, :remark]
		   values   = []
		   
		   csv_file = CSV.open(filePath, 'r:ISO-8859-1', {headers: true, header_converters: :symbol})
		   csv_file.each_with_index do |row, i|
		     values.push( ["#{self.id}-#{row[:code]}", self.id, row[:code], row[:name], row[:reference], row[:status], row[:address], row[:telephone], row[:remark]] )
		   end

		   Company.import columns, values, { validate: false, on_duplicate_key_update: [:name, :reference, :status, :address, :telephone, :remark] }
		   puts Company.last.to_json
		   FileUtils.mv(filePath, self.archives_dir)

		end
	end

end
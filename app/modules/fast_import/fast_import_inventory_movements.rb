module FastImport::FastImportInventoryMovements
	
	def fi_inventory_movements(file_name = 'inventory_movements_?????????????????.csv')
	   temp_filePath ||= File.join(self.account_dir, 'uploads', file_name)
	   filePath2 = Dir.glob("#{temp_filePath}")

	   filePath2.each do |filePath|

	   	basename 		  = File.basename(filePath,File.extname(filePath))
			transaction_date = DateTime.strptime(basename[20..30], "%m-%d-%Y") rescue Date.today

		   puts "-----------------------------------------------------------"
		   puts "INVENTORY MOVEMENTS IMPORT STARTED"
		   puts "-----------------------------------------------------------"

		   #name_x = File.basename(filePath, ".csv").to_s   #=> "ruby"
		   #name_x = name_x.to(20).from(-8).to_date
		   columns  = [:account_id, :branch_id, :first_inventory, :quantity, :last_inventory, :transaction_code, :type_code, :date, :product_id]
		   values   = []

		   csv_file = CSV.open(filePath, 'r:ISO-8859-1', {headers: true, header_converters: :symbol})
		   csv_file.each_with_index do |row, i|

		   	#productObject = self.products.find_by_sku(row[:product_sku]) rescue nil

		   	#if productObject.present?

	   		lastInventoryQty = 0 #productObject.transactions.order('id DESC').first.last_inventory rescue 0.0
	   		newLastInventory = 0 #row[:transaction_code].to_s.upcase == 'REC' ? (row[:quantity].to_f + lastInventoryQty.to_f) : (row[:quantity].to_f - lastInventoryQty.to_f)
	   		values << [
	   			self.id,
	   			"#{self.id}-#{row[:branch_code]}",
	   			lastInventoryQty,
	   			row[:quantity],
	   			newLastInventory,
	   			row[:transaction_code].to_s.upcase,
	   			row[:type_code],
	   			transaction_date,
	   			"#{self.id}-#{row[:product_sku]}"	   			
	   		]	
		   	#end

		   end
		   Transaction.import columns, values, { validate: false }
		   puts Transaction.last.to_json
		   FileUtils.mv(filePath, self.archives_dir)

		end
	end

end
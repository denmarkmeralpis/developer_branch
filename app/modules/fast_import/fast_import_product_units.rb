module FastImport::FastImportProductUnits

	def fi_product_units(file_name = 'product_units_?????????????????.csv')
		temp_filePath ||= File.join(self.account_dir, 'uploads', file_name)
	   filePath2 = Dir.glob("#{temp_filePath}")

	   filePath2.each do |filePath|

		   puts "-----------------------------------------------------------"
		   puts "PRODUCT UNITS IMPORT STARTED"
		   puts "-----------------------------------------------------------"

	   	# parent_product_id:     parentProduct.id,
	    #   sku:                   row[:sku],
	    #   name:                  row[:name],
	    #   barcode:               (row[:barcode] rescue ''),
	    #   base_unit_name:        (row[:unit_name] rescue ''),
	    #   unit_content_quantity: row[:content_quantity],
	    #   cost:                  row[:cost],
	    #   retail_price:          (row[:retail_price] rescue 0.0),
	    #   status:                row[:status],
	    #   ordering_unit:         (row[:is_default].to_i.abs.to_s rescue ''),
	    #   product_category_id:   category.id

		   columns  = [:id, :account_id, :sku, :parent_product_id, :name, :barcode, :base_unit_name, :unit_content_quantity, :cost, :retail_price, :status, :ordering_unit, :product_category_id]
		   values   = []

		   csv_file = CSV.open(filePath, 'r:ISO-8859-1', {headers: true, header_converters: :symbol})
		   csv_file.each_with_index do |row, i|	   	
		     values << [
		     		"#{self.id}-#{row[:sku]}",
		     		self.id,
		     		row[:sku],
		     		"#{self.id}-#{row[:parent_sku]}",
		     		row[:name],
		     		row[:barcode],
		     		row[:unit_name],
		     		row[:content_quantity],
		     		row[:cost],
		     		row[:retail_price],
		     		row[:status],
		     		(row[:is_default].to_i.abs.to_s rescue 0),
		     		"#{self.id}-#{row[:category_code]}"
		     ]
		   end

		   Product.import columns, values, { validate: false, on_duplicate_key_update: [:parent_product_id, :name, :barcode, :base_unit_name, :unit_content_quantity, :cost, :retail_price, :status, :ordering_unit, :product_category_id] }
		   puts Product.last.to_json
		   FileUtils.mv(filePath, self.archives_dir)

	  end
	end

end
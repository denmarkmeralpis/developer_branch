module FastImport::FastImportBranches
	def fi_branches(file_name = 'branches_?????????????????.csv')
		temp_filePath ||= File.join(self.account_dir, 'uploads', file_name)
    	filePath2 = Dir.glob("#{temp_filePath}")

    	filePath2.each do |filePath|

	    	puts "-----------------------------------------------------------"
	    	puts "BRANCHES IMPORT STARTED"
	    	puts "-----------------------------------------------------------"

		   columns = [:id, :account_id, :code, :name, :status]
		   values  = []

		   csv_file = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })
		   csv_file.each_with_index { |row, i| values.push(["#{self.id}-#{row[:code]}", self.id, row[:code], row[:name], row[:status]]) }

		   Branch.import columns, values, { validate: false, on_duplicate_key_update: [:name] }
		   puts Branch.last.name

		   FileUtils.mv(filePath, self.archives_dir)
		end
	end
end
module FastImport::Base

  include FileUtils
 
  include FastImport::FastImportProductCategories
  include FastImport::FastImportCompanies
  include FastImport::FastImportBranches
  include FastImport::FastImportSuppliers
  include FastImport::FastImportProducts
  include FastImport::FastImportProductUnits
  include FastImport::FastImportSupplierProducts
  include FastImport::FastImportBranchProducts
  include FastImport::FastImportInventoryMovements
  include FastImport::FastImportSalesInventory


  def start_import
    start_time = Time.zone.now
    

     puts "======UNZIP STARTS============="
     file_name = File.join(self.account_dir, "*.zip")
     fileNameList = Dir.glob(file_name)
     puts fileNameList.to_s
     fileNameList.each do |fileName|
       Zip::ZipFile.open(fileName) { |zip_file|
          puts zip_file
          zip_file.each { |f|
            f_path=File.join(self.account_dir, 'uploads' ,f.name)
            FileUtils.mkdir_p(File.dirname(f_path))
            zip_file.extract(f, f_path) unless File.exist?(f_path)
          }
         }
         mac_os = File.join(self.account_dir,"__MACOSX")
         FileUtils.rm_rf(mac_os) 
         File.delete(fileName)
     end
     
     puts "======UNZIP ENDS============="


    self.fi_product_categories
    self.fi_companies
    self.fi_branches
    self.fi_suppliers
    self.fi_products
    self.fi_product_units
    self.fi_supplier_products
    self.fi_branch_products
    self.fi_inventory_movements
    self.fi_sales_and_inventories

    end_time   = Time.zone.now
    puts "------------------------------------------------------------"
    puts "Done import | TIME SPEND: #{end_time - start_time} seconds!"
    puts "------------------------------------------------------------"
    
  end

  def reset_db
    Company.delete_all
    Branch.delete_all
    Supplier.delete_all
    Product.delete_all
    SupplierProduct.delete_all
    BranchProduct.delete_all
    Transaction.delete_all
    ProductCategory.delete_all
    BranchSupplierConfiguration.delete_all
    Ownership.delete_all
    PurchaseOrder.delete_all
    PurchaseOrderLine.delete_all
    BuyerSupplier.destroy_all
    'Na reset na!!!!!!!'
  end

  def start_log( options = {} )
    directory = File.join(Rails.root, 'log', 'import', 'errors')
    FileUtils.mkdir_p( directory ) unless File.directory?( directory ) # create if not exist
    file_path = File.join( directory, "#{options[:file_name]}.error.log" )
    File.delete file_path if File.exist?( file_path )

    @error_logger =  Logger.new( file_path )
    @error_count  = 0
  end

  def log_error( message )
    puts "** ERROR ** #{message}"
    @error_logger.info message
    @error_count += 1
  end

  def stop_log( options = {} )
    @error_logger.close
    if @error_count == 0
      directory = File.join(Rails.root, 'log', 'import', 'errors')
      file_path = File.join( directory, "#{options[:file_name]}.error.log" )
      File.delete file_path if File.exist?( file_path )
    else
      # mail the log file to administrator
    end
  end

end
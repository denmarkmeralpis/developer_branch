module FastImport::FastImportSalesInventory

	def fi_sales_and_inventories(file_name = 'sales_and_inventories_?????????????????.csv')
		temp_filePath ||= File.join(self.account_dir, 'uploads', file_name)
	   filePath2 = Dir.glob("#{temp_filePath}")
	   
	   filePath2.each do |filePath|
		   puts "-----------------------------------------------------------"
		   puts "SALES AND INVENTORIES IMPORT STARTED"
		   puts "-----------------------------------------------------------"

		   columns = [:product_id, :branch_id, :first_inventory, :quantity, :last_inventory, :account_id, :transaction_code, :type_code, :date]
		   values  = []
		   
		   csv_file = CSV.open(filePath, 'r:ISO-8859-1', {headers: true, header_converters: :symbol})
		   csv_file.each_with_index do |row, i|	      

	      	  fInv = row[:quantity_sold].to_f + row[:end_of_day_quantity].to_f
		      lInv = row[:end_of_day_quantity].to_f + row[:incoming_quantity].to_f
		      transCode = 'REL'
		      typeCode  = 'POS'

		      #branch_id = self.branches.find_by_code(row[:branch_code].to_s).id rescue nil

		      values << [
		      	"#{self.id}-#{row[:sku]}",
		      	row[:branch_code],#branch_id,
		      	fInv,
		      	row[:quantity_sold],
		      	lInv,
		      	self.id,
		      	transCode,
		      	typeCode,
		      	(row[:date].to_date rescue Time.zone.now)
		      ]		      
		      
		   end

		   Transaction.import columns, values, { validate: false }
		   puts Transaction.last.to_json
		   FileUtils.mv(filePath, self.archives_dir)
		end
	end
	
end

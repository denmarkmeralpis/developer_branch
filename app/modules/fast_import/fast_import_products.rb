module FastImport::FastImportProducts
	
	def fi_products(file_name = 'products_?????????????????.csv')
		temp_filePath ||= File.join(self.account_dir, 'uploads', file_name)
	   filePath2 = Dir.glob("#{temp_filePath}")

	   filePath2.each do |filePath|
		   puts "-----------------------------------------------------------"
		   puts "PRODUCTS IMPORT STARTED"
		   puts "-----------------------------------------------------------"

		   columns  = [:id, :account_id, :sku, :stock_no, :barcode, :name, :cost, :retail_price, :base_unit_name, :unit_content_quantity, :discount_text, :ordering_unit, :status, :allow_decimal_quantities, :product_category_id]
		   values   = []

		   csv_file = CSV.open(filePath, 'r:ISO-8859-1', {headers: true, header_converters: :symbol})
		   csv_file.each_with_index do |row, i|
		     values.push [ 
		     	"#{self.id}-#{row[:sku]}", 
		     	self.id,
		     	row[:sku],
		     	row[:stock_no],
		     	row[:barcode],
		     	row[:name],
		     	row[:cost],
		     	row[:retail_price],	     	
		     	row[:unit_name],
		     	row[:unit_content_quantity],
		     	row[:discount],
		     	(row[:default_ordering_unit].to_i.abs.to_s rescue ''),
		     	row[:status],
		     	row[:allow_decimal_quantities],
		     	"#{self.id}-#{row[:category_code]}"	     
		     ]
		   end

		   Product.import columns, values, { validate: false, on_duplicate_key_update: [:stock_no, :barcode, :name, :cost, :retail_price, :base_unit_name, :unit_content_quantity, :discount_text, :ordering_unit, :status, :allow_decimal_quantities, :product_category_id] }
		   puts Product.last.to_json
		   FileUtils.mv(filePath, self.archives_dir)
	  end
	end

end
module FastImport::FastImportBranchProducts

	def fi_branch_products(file_name = 'branch_products_?????????????????.csv')
		temp_filePath ||= File.join(self.account_dir, 'uploads', file_name)
    	filePath2 = Dir.glob("#{temp_filePath}")

    	filePath2.each do |filePath|
	    	puts "-----------------------------------------------------------"
	    	puts "BRANCH PRODUCTS IMPORT STARTED"
	    	puts "-----------------------------------------------------------"

		   columns = [:id, :account_id, :branch_id, :product_id, :cost, :discount_text, :supplier_id]
		   values  = []

		   # branch_code,sku,cost,discount_text,supplier_code

		   csv_file = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })
		   csv_file.each_with_index do |row, i|
		   	values << [
		   		"#{self.id}-#{row[:branch_code]}-#{row[:sku]}-#{row[:supplier_code]}", 
		   		self.id, 
		   		"#{self.id}-#{row[:branch_code]}", 
		   		"#{self.id}-#{row[:sku]}", 
		   		row[:cost],
		   		row[:discount_text],
		   		"#{self.id}-#{row[:supplier_code]}"
		   	]
		   end

		   BranchProduct.import columns, values, { validate: false, on_duplicate_key_update: [:cost, :discount_text] }
		   puts BranchProduct.last.to_json	

		   FileUtils.mv(filePath, self.archives_dir)
		   
		end
	end

end
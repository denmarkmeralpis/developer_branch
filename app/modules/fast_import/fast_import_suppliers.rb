module FastImport::FastImportSuppliers
	
	def fi_suppliers(file_name = 'suppliers_?????????????????.csv')
		temp_filePath ||= File.join(self.account_dir, 'uploads', file_name)
	   filePath2 = Dir.glob("#{temp_filePath}")

	   filePath2.each do |filePath|

		   puts "-----------------------------------------------------------"
		   puts "SUPPLIERS IMPORT STARTED"
		   puts "-----------------------------------------------------------"

		   columns  = [:id, :account_id, :name, :code, :reference, :discount_text, :contact_person, :contact_telephone, :contact_fax, :address, :status]
		   values   = []

		   csv_file = CSV.open(filePath, 'r:ISO-8859-1', {headers: true, header_converters: :symbol})
		   csv_file.each_with_index do |row, i|
		     values.push [ "#{self.id}-#{row[:code]}", self.id, row[:name], row[:code], row[:reference], row[:discount_text], row[:contact_person], row[:telephone], row[:fax], row[:address], row[:status]
		     ]
		   end

		   Supplier.import columns, values, { validate: false, on_duplicate_key_update: [:name, :reference, :discount_text, :contact_person, :contact_telephone, :contact_fax, :address, :status] }
		   puts Supplier.last.to_json
		   FileUtils.mv(filePath, self.archives_dir)
		end
	end

end
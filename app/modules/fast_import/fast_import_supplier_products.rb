module FastImport::FastImportSupplierProducts

	def fi_supplier_products(file_name = 'alternate_supplier_products_?????????????????.csv')
		temp_filePath ||= File.join(self.account_dir, 'uploads', file_name)
	   filePath2 = Dir.glob("#{temp_filePath}")

	   filePath2.each do |filePath|
		   puts "-----------------------------------------------------------"
		   puts "SUPPLIER PRODUCTS IMPORT STARTED"
		   puts "-----------------------------------------------------------"

		   columns  = [:id, :account_id, :product_id, :supplier_id, :cost, :status, :last_discount_text]
		   values   = []

		   csv_file = CSV.open(filePath, 'r:ISO-8859-1', {headers: true, header_converters: :symbol})
		   csv_file.each_with_index do |row, i|
		     values.push [ 	     	
		     	"#{self.id}-#{row[:supplier_code]}-#{row[:sku]}",
		     	self.id,
		     	"#{self.id}-#{row[:sku]}",
		     	"#{self.id}-#{row[:supplier_code]}",	     	
		     	row[:cost],
		     	row[:status].to_s.upcase,
		     	row[:discount_text]
		     ]
		   end

		   SupplierProduct.import columns, values, { validate: false, on_duplicate_key_update: [:cost, :status, :last_discount_text] }
		   puts SupplierProduct.last.to_json
		   FileUtils.mv(filePath, self.archives_dir)
		end
	end

end
# noinspection ALL
module Import::ImportCompanies

  def start_import_companies(fileName = 'companies_??????????.csv')
    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Companies started."
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile  = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })

    timerStart = Time.now
    start_log file_name: fileName

    # Start of Import on Company CSV
    csvFile.each_with_index do |row, i|
      companyAttr = {
        code:       row[:code],
        name:       row[:name],
        reference:  row[:reference],
        status:     row[:status],
        address:    row[:address],
        telephone:  row[:telephone],
        remark:     row[:remark]
      }
      companyObje = self.companies.find_by_code(row[:code])
      companyObje.nil? ? companyObje = self.companies.create(companyAttr) : companyObje.update_attributes(companyAttr)
      #log_error "row #{i}: COMPANY CODE IS: #{row[:code]} #{companyObje.errors.full_messages.to_sentence}" unless companyObje.errors.blank?
    end
    # End of Import on Company CSV

    timerEnd = Time.now
    csvFile.close
    stop_log file_name: fileName

    FileUtils.mv(filePath, self.archives_dir)

    "Import Companies completed. Time spend #{timerEnd - timerStart} seconds"

  end

end
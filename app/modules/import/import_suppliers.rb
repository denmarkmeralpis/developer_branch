# noinspection ALL
module Import::ImportSuppliers
  
  def start_import_suppliers( file_path = nil)
    fileName = 'suppliers_??????????.csv'
    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Suppliers started."
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile  = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })

    start_log file_name: fileName
    timerStart = Time.now

    # Start of Supplier CSV Import
    csvFile.each_with_index do |row, i|
      supplierAttr =
        {
          code:              row[:code],
          name:              row[:name],
          reference:         row[:reference],
          contact_telephone: row[:telephone],
          contact_fax:       row[:fax],
          address:           row[:address],
          contact_person:    row[:contact_person],
          discount_text:     row[:discount_text],
          status:            row[:status]
        }

      supplierObje = self.suppliers.find_by_code(row[:code])
      supplierObje.nil? ? supplierObje = self.suppliers.create(supplierAttr) : supplierObje.update_attributes(supplierAttr)
      log_error "row #{i}: SUPPLIER CODE IS: #{row[:code]} #{supplierObje.errors.full_messages.to_sentence}" unless supplierObje.errors.blank?
    end
    # End of Supplier CSV Import

    timerEnd = Time.now
    csvFile.close
    stop_log file_name: fileName
    FileUtils.mv(filePath, self.archives_dir)
    "Import supplier completed. Time spend #{timerEnd - timerStart} seconds"

  end

end
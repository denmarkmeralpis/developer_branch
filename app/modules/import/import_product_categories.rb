module Import::ImportProductCategories

  def start_import_product_categories(fileName = 'product_categories_??????????.csv')
    
    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Product Category started."
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile  = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })

    start_log file_name: fileName

    timerStart = Time.now

    # Start Product Category Import
    csvFile.each_with_index do |row, i|
      product_category_attr = { code: row[:code], name: row[:name] }
      product_category = self.product_categories.find(:first, conditions: { code: row[:code], name: [:name] })
      product_category.nil? ? product_category = self.product_categories.create(product_category_attr) : product_category.update_attributes(product_category_attr)
      log_error "row #{i}: #{row[:code]}: #{product_category.errors.full_messages.to_sentence}" unless product_category.errors.blank?
    end
    # End of Product Cateogry Import

    timerEnd = Time.now
    csvFile.close
    stop_log file_name: fileName
    FileUtils.mv(filePath, self.archives_dir)
    "Import products category Completed. Time spend #{timerEnd - timerStart} seconds"

  end

end
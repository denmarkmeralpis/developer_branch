# noinspection ALL
module Import::ImportBranches

  def start_import_branches(fileName = 'branches_??????????.csv')

    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Branch started."
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile   = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })

    start_log file_name: fileName
    timeStart = Time.now

    # Start of Branch CSV Import
    csvFile.each_with_index do |row, i|
      company = self.companies.find_by_code(row[:company_code])
      if company
        branchAttr = {
          code: row[:code],
          name: row[:name],
          reference: row[:reference],
          company_id: company.id,
          status: row[:status]
        }
      else
        puts "#{i} company code #{row[:code]} not found in company table!"
      end

      branchObje = self.branches.find_by_code(row[:code])
      branchObje.nil? ? branchObje = self.branches.create(branchAttr) : branchObje.update_attributes(branchAttr)
      log_error "row #{i}: BRANCH CODE: #{row[:code]}, #{branchObje.errors.full_messages.to_sentence}" unless branchObje.errors.blank?
    end
    # End of Branch CSV Import

    timeEnd   = Time.now
    stop_log file_name: fileName
    FileUtils.mv(filePath, self.archives_dir)
    "Import Branch completed. Time spend #{timeEnd - timeStart} seconds"

  end

end
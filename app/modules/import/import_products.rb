# noinspection ALL
module Import::ImportProducts

  def start_import_products(fileName = 'products_??????????.csv')
    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Products started."
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile  = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })

    start_log file_name: fileName
    timerStart = Time.now

    # Start of Product Import
    csvFile.each_with_index do |row, i|

      category_id = self.product_categories.find_or_create_by_code(row[:category_code])
      supplier_obj = self.suppliers.find_by_code(row[:supplier_code])

      productsAttr = {      
        sku:                      row[:sku],
        stock_no:                 row[:stock_no],
        barcode:                  row[:barcode],
        name:                     row[:name],
        cost:                     row[:cost],
        retail_price:             row[:retail_price],
        supplier_code:            row[:supplier_code],
        base_unit_name:           row[:unit_name],
        unit_content_quantity:    row[:unit_content_quantity],
        discount_text:            row[:discount],
        ordering_unit:            (row[:default_ordering_unit].to_i.abs.to_s rescue ''),
        status:                   row[:status].to_s.upcase,
        allow_decimal_quantities: row[:allow_decimal],
        product_category_id:      category_id.id
      }      

      productsObje = self.products.find_by_sku(row[:sku])
      productsObje.nil? ? productsObje = self.products.create(productsAttr) : productsObje.update_attributes(productsAttr)

      supplierProductAttr = {
        account_id:         self.id,
        supplier_id:        supplier_obj.id,
        product_id:         productsObje.id,
        cost:               row[:cost],
        retail_price:       row[:retail_price],
        last_discount_text: row[:discount],
        status:             row[:status]
      }

      SupplierProduct.create(supplierProductAttr)
      
      log_error "row #{i}: #{row[:sku]} -- SUPPLIER CODE IS: #{row[:supplier_code]} #{productsObje.errors.full_messages.to_sentence}" unless productsObje.errors.blank?
    end
    # End of Product Import

    timerEnd = Time.now
    csvFile.close
    stop_log file_name: fileName
    FileUtils.mv(filePath, self.archives_dir)
    "Import Products Completed. Time spend #{timerEnd - timerStart} seconds."

  end

end
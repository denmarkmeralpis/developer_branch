# noinspection ALL
module Import::ImportSalesInv

  def start_import_sales_inv
    fileName = 'sales_and_inventory_??????????.csv'
    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Sales Inventory started."
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile  = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })

    timerStart = Time.now
    start_log file_name: fileName

    # Start of Import on Inventory Movements CSV for statistic table
    csvFile.each_with_index do |row, i|

      productObject = self.products.find_or_create_by_sku(row[:sku])

      fInv = row[:quantity_sold].to_f + row[:end_of_day_quantity].to_f
      lInv = row[:end_of_day_quantity].to_f + row[:incoming_quantity].to_f
      transCode = 'REL'
      typeCode = 'POS'

      branch_id = self.branches.find_by_code(row[:site_code].to_s).id rescue nil

      statisticAttr = {
          branch_id:        branch_id,
          first_inventory:  fInv,
          quantity:         row[:quantity_sold],
          last_inventory:   lInv,
          account_id:       self.id,
          transaction_code: transCode,
          type_code:        typeCode,
          date:             (row[:date].to_date rescue Time.now)
      }

      statisticObje = productObject.transactions.create(statisticAttr)
      log_error "row #{i}: #{row[:code]} #{statisticObje.errors.full_messages.to_sentence}" unless statisticObje.errors.blank?

    end
    # End of Import on Inventory Movements CSV for statistic table

    timerEnd = Time.now
    csvFile.close
    stop_log file_name: fileName
    FileUtils.mv(filePath, self.archives_dir)
    "Import Sales Inventory completed. Time spend #{timerEnd - timerStart} seconds"

  end

end
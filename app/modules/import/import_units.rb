
module Import::ImportUnits

  def start_import_units(fileName = 'product_units_??????????.csv')
    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Units started."
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile  = CSV.open(filePath, 'r', {headers: true, header_converters: :symbol, encoding: 'bom|utf-8'})

    start_log file_name: fileName
    timerStart = Time.now

    # Start of Import for Units
    csvFile.each_with_index do |row, i|
      category = self.product_categories.find_or_create_by_code(row[:category_code])
      parentProduct = self.products.first conditions: {sku: row[:parent_sku]} # search for parent product
      if parentProduct
        unitAttr = {
          parent_product_id:     parentProduct.id,
          sku:                   row[:sku],
          name:                  row[:name],
          barcode:               (row[:barcode] rescue ''),
          base_unit_name:        (row[:unit_name] rescue ''),
          unit_content_quantity: row[:content_quantity],
          cost:                  row[:cost],
          retail_price:          (row[:retail_price] rescue 0.0),
          status:                row[:status],
          ordering_unit:         (row[:is_default].to_i.abs.to_s rescue ''),
          product_category_id:   category.id
        }

        product = self.products.find_by_parent_product_id_and_sku(parentProduct.id, row[:sku])
        product.nil? ? product = self.products.create(unitAttr) : product.update_attributes(unitAttr)

        log_error "row #{i}: #{product.errors.full_messages.to_sentence}" unless product.errors.blank?
      else
        puts "Not found #{row[:parent_sku]}"
      end
    end
    # End of Import

    timerEnd = Time.now
    csvFile.close
    stop_log file_name: fileName
    FileUtils.mv(filePath, self.archives_dir)
    "Import Units completed. Time spend #{timerEnd - timerStart} seconds."

  end

end
# noinspection ALL
module Import::ImportBranchProducts

  def start_import_branch_products(fileName = 'branch_products_??????????.csv') 

    temp_filePath ||=  File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Branch Product started."
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile  = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })

    start_log file_name: fileName

    timerStart = Time.now

    # Start Branch Product Import
    csvFile.each_with_index do |row, i|
      branch   = self.branches.find_by_code( row[:branch_code] )
      product  = self.products.find_by_sku( row[:sku] )
      supplier = self.suppliers.find_by_code( row[:supplier_code] )

      branchId   = branch.id   if branch
      productId  = product.id  if product
      supplierId = supplier.id if supplier

      bsc_attr = {
        account_id: self.id,
        branch_id: branchId,
        supplier_id: supplierId,
        frequency_type: :weekly,
        frequency_week: nil,
        frequency_day: :monday
      }

      bsc = BranchSupplierConfiguration.where(bsc_attr)

      if bsc.blank?        
        newBsc = BranchSupplierConfiguration.create(bsc_attr)        
        newBsc.reload.calculate_next_po_date        
      end
      
      branchProduct = self.branch_products.find_by_product_id_and_branch_id(productId, branchId)
      branchProductAttr = { branch_id: branchId, product_id: productId, supplier_id: supplierId, status: row[:status].to_s.upcase }
      branchProduct.nil? ? branchProduct = self.branch_products.create(branchProductAttr) : branchProduct.update_attributes(branchProductAttr)

      log_error "row #{i}: #{row[:branch_code]}: #{row[:sku]} #{branchProduct.errors.full_messages.to_sentence}" unless branchProduct.errors.blank?
    end
    # End of Branch Product Import

    timerEnd = Time.now
    csvFile.close
    stop_log file_name: fileName
    FileUtils.mv(filePath, self.archives_dir)
    "Import branch products Completed. Time spend #{timerEnd - timerStart} seconds"

  end

end

module Import::Base

  include FileUtils
  include Import::ImportCompanies
  include Import::ImportBranches
  include Import::ImportSuppliers
  include Import::ImportProducts
  include Import::ImportUnits
  include Import::ImportBranchProducts
  include Import::ImportSupplierProducts
  include Import::ImportInventoryMovements
  include Import::ImportSalesInv
  include Import::ImportProductCategories

  #unzipping
  require 'rubygems'
  require 'zip/zip'

  def import_test_data#(args)

    puts "======UNZIP STARTS============="
    #unzip file
    fileName = 'uploads.zip'
    file = File.join(self.account_dir,fileName)
    Zip::ZipFile.open(file) { |zip_file|
       puts zip_file
       zip_file.each { |f|
         f_path=File.join(self.account_dir, f.name)
         FileUtils.mkdir_p(File.dirname(f_path))
         zip_file.extract(f, f_path) unless File.exist?(f_path)
       }
      }
      mac_os = File.join(self.account_dir,"__MACOSX")
      FileUtils.rm_rf(mac_os) 
      File.delete(file)

    puts "======UNZIP ENDS============="

    start_time = Time.zone.now

    self.start_import_product_categories
    self.start_import_companies # File.join(folder, 'companies.csv' )

    self.start_import_branches # File.join(folder, 'branches.csv' )
    self.start_import_suppliers # File.join(folder, 'suppliers.csv' )

    self.start_import_products # File.join(folder, 'products.csv' )
    self.start_import_units # File.join(folder, 'units.csv' )

    self.start_import_supplier_products # File.join(folder, 'supplier_products.csv' )
    self.start_import_branch_products # File.join(folder, 'branch_products.csv' )

    self.start_import_inventory_movements
    self.start_import_sales_inv


    end_time = Time.zone.now
    puts '#############################################################'
    puts "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
    puts "Done importing. Time spend #{end_time - start_time} seconds."
    puts "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
    puts '#############################################################'
  end

  def reset_db
    Company.delete_all
    Branch.delete_all
    Supplier.delete_all
    Product.delete_all
    SupplierProduct.delete_all
    BranchProduct.delete_all
    Transaction.delete_all
    ProductCategory.delete_all
    BranchSupplierConfiguration.delete_all
    Ownership.delete_all
    PurchaseOrder.delete_all
    PurchaseOrderLine.delete_all
    BuyerSupplier.destroy_all
    'Na reset na!!!!!!!'
  end

  def start_log( options = {} )
    directory = File.join(Rails.root, 'log', 'import', 'errors')
    FileUtils.mkdir_p( directory ) unless File.directory?( directory ) # create if not exist
    file_path = File.join( directory, "#{options[:file_name]}.error.log" )
    File.delete file_path if File.exist?( file_path )

    @error_logger =  Logger.new( file_path )
    @error_count  = 0
  end

  def log_error( message )
    puts "** ERROR ** #{message}"
    @error_logger.info message
    @error_count += 1
  end

  def stop_log( options = {} )
    @error_logger.close
    if @error_count == 0
      directory = File.join(Rails.root, 'log', 'import', 'errors')
      file_path = File.join( directory, "#{options[:file_name]}.error.log" )
      File.delete file_path if File.exist?( file_path )
    else
      # mail the log file to administrator
    end
  end

end
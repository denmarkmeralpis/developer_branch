# noinspection ALL
module Import::ImportSupplierProducts

  def start_import_supplier_products(fileName = 'supplier_products_??????????.csv')

    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Supplier Products started."
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile  = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })

    timerStart = Time.now
    start_log file_name: fileName

    # Start Supplier Product Import CSV
    csvFile.each_with_index do |row, i|
      # Locate Supplier
      supplierId  = nil
      supplierObj = self.suppliers.find_by_code(row[:supplier_code])
      supplierId  = supplierObj.id if supplierObj

      # Locate SKU in Product
      productObj = self.products.find_or_create_by_sku(row[:sku].to_s)
      productId  = productObj.id


      log_error("PRODUCT SKU NOT FOUND: row: #{i}, sku: #{row[:sku]}") if productObj.nil?

      supplierProductObjec = self.supplier_products.find_by_supplier_id_and_product_id(supplierId, productId)
      supplierProrductAttr = {
        account_id:   self.id,
        supplier_id:  supplierId,
        product_id:   productId,
        cost:         row[:cost],
        status:       row[:status].to_s.upcase
      }

      supplierProductObjec.nil? ? supplierProductObjec = SupplierProduct.create(supplierProrductAttr) : supplierProductObjec.update_attributes(supplierProrductAttr)
      log_error "row #{i}: #{row[:supplier_code]}:#{row[:sku]} #{supplierProductObjec.errors.full_messages.to_sentence}" unless supplierProductObjec.errors.blank?
    end
    # End Supplier Product Import CSV
    timerEnd = Time.now
    csvFile.close
    stop_log file_name: fileName
    FileUtils.mv(filePath, self.archives_dir)
    "Import supplier products completed. TIme spend #{timerEnd - timerStart} seconds"

  end

end
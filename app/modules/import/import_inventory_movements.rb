# noinspection ALL
module Import::ImportInventoryMovements

  def start_import_inventory_movements
    fileName = 'inventory_movements_??????????.csv'
    temp_filePath ||= File.join(self.account_dir, 'uploads', fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Inventory Movement started."
    else
      puts "Unable to find #{fileName}"
      return
    end

    csvFile  = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })

    timerStart = Time.now
    start_log file_name: fileName

    # Start of Import on Inventory Movements CSV for statistic table
    csvFile.each_with_index do |row, i|

      productObject = Product.find :first, conditions: { account_id: self.id, sku: row[:product_sku] }
      if productObject.nil?
        productObject = self.products.create(:sku => row[:product_sku])
      end
      
      lastInventoryQty = productObject.transactions.order('id DESC').first.last_inventory rescue 0.0

      if row[:transaction_code].to_s.upcase == 'REC'
        newLastInventory = row[:quantity].to_f + lastInventoryQty.to_f
      else
        newLastInventory = row[:quantity].to_f - lastInventoryQty.to_f
      end

      # branch_id = self.branches.find_by_code(row[:branch_code]).id rescue nil

      branch_id = self.branches.find_by_reference(row[:branch_code]).id rescue nil

      statisticAttr = {
        branch_id:        branch_id,
        first_inventory:  lastInventoryQty,
        quantity:         row[:quantity],
        last_inventory:   newLastInventory,
        account_id:       self.id,
        transaction_code: row[:transaction_code],
        type_code:        row[:type_code],
        date:             Time.zone.today
      }


      statisticObje = productObject.transactions.create(statisticAttr)
      log_error "row #{i}: #{row[:code]} #{statisticObje.errors.full_messages.to_sentence}" unless statisticObje.errors.blank?

    end
    # End of Import on Inventory Movements CSV for statistic table

    timerEnd = Time.now
    csvFile.close
    stop_log file_name: fileName
    FileUtils.mv(filePath, self.archives_dir)
    "Import Transaction completed. Time spend #{timerEnd - timerStart} seconds"

  end

end
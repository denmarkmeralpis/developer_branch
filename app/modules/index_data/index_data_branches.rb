module IndexData::IndexDataBranches

	def start_indexing_on_branches
		self.branches
	end

	def start_indexing_on_branch_products
		self.branches.each do |branch|
			branch.br_suppliers.select("DISTINCT supplier_id")
		end
	end

	def start_index_branch_supplier_configuration
		self.branches.each do |branch|
			branch.suppliers
		end
	end

end
module IndexData::IndexDataSuppliers

	def start_indexing_on_suppliers
		self.suppliers
	end

	def start_index_on_supplier_products
		self.suppliers.each do |supplier|
			supplier.products
		end
	end

	def start_index_on_user_suppliers
		self.users.each do |user|
			user.suppliers
		end
	end

end
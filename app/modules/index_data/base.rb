module IndexData::Base

	include IndexData::IndexDataBranches	
	include IndexData::IndexDataSuppliers
	
	def start_indexing		
		# Branch Table
		self.start_indexing_on_branches		

		# Branch Products Table
		self.start_indexing_on_branch_products

		# Supplier Table
		self.start_indexing_on_suppliers

		# Supplier Products Table
		self.start_index_on_supplier_products

		# Buyer Supplier Table
		self.start_index_on_user_suppliers

		# Branch Supplier Configuration Table
		self.start_index_branch_supplier_configuration

		# last index
		self.update_attributes(last_data_index: Time.zone.now, index_status: :done)
	end	

end
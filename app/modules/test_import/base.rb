module TestImport::Base

  include FileUtils
  include TestImport::TestImportUserImport
  include TestImport::TestImportUserAssignment
  include TestImport::TestImportBranchesAssignment

  def start_test_import
  	self.user_import
  	self.user_assignment_import
  	self.branches_assignment_import
  end

  def start_log( options = {} )
    directory = File.join(Rails.root, 'log', 'assignment_import', 'errors')
    FileUtils.mkdir_p( directory ) unless File.directory?( directory )
    file_path = File.join( directory, "#{options[:file_name]}.error.log" )
    File.delete file_path if File.exist?( file_path )

    @error_logger =  Logger.new( file_path )
    @error_count  = 0
  end

  def log_error( message )
    puts "** ERROR ** #{message}"
    @error_logger.info message
    @error_count += 1
  end

  def stop_log( options = {} )
    @error_logger.close
    if @error_count == 0
      directory = File.join(Rails.root, 'log', 'assignment_import', 'errors')
      file_path = File.join( directory, "#{options[:file_name]}.error.log" )
      File.delete file_path if File.exist?( file_path )
    else
      # mail the log file to administrator
    end
  end

end
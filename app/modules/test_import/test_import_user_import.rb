module TestImport::TestImportUserImport

  def user_import(fileName = 'users.csv')

    temp_filePath ||= File.join(self.account_dir, fileName)
    filePath = Dir.glob("#{temp_filePath}").first

    if File.exists?(filePath)
      puts "Import Users"
    else
      return
    end

    csvFile = CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })
    timeStart = Time.now
     
    csvFile.each_with_index do |row, i|

      parent_id = self.users.first.id
    
      if (row[:user_type] == 'buyer') 
        supervisor_id = self.users.find_by_email(row[:supervisor_email]).id 
      else
        supervisor_id = nil
      end

      userAttr = {
        first_name: row[:first_name],
        last_name: row[:last_name],
        role: row[:user_type],
        parent_id: parent_id,
        supervisor_id: supervisor_id,
        account_id: self.id,
        email: row[:email],
        password: row[:password]
      }
      
      user = self.users.new(userAttr)

     if !user.save
       puts "______________"
       puts user.errors.full_messages.to_sentence
       puts "_______________"
     else
      user.roles.create(:name => 'rpo' ) if (row[:can_confirm_po] == 'YES')
      user.roles.create(:name => 'apo' ) if (row[:can_approve_void_po] == 'YES')
      user.roles.create(:name => 'cpo' ) if (row[:can_copy_po] == 'YES')  
      user.roles.create(:name => 'epo' ) if (row[:can_edit_price_in_po] == 'YES')
      user.roles.create(:name => 'ssu' ) if (row[:can_suspend_suppliers] == 'YES')
      user.roles.create(:name => 'spr' ) if (row[:can_suspend_products] == 'YES')
     end      

    end

      timeEnd   = Time.now
      # FileUtils.mv(filePath, self.archives_dir)
      csvFile.close
      "Import Users completed. Time spend #{timeEnd - timeStart} seconds"

  end

end


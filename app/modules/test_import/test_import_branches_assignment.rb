module TestImport::TestImportBranchesAssignment

  	def branches_assignment_import(fileName = 'branch-supplier.csv')	

   	temp_filePath ||= File.join(self.account_dir, fileName)
   	filePath = Dir.glob("#{temp_filePath}").first
   	
   	if File.exists?(filePath)
   	  puts "Import Users Assignment"
   	else
   		puts "Import CSV Not found"
   	  return
   	end	
   	
   	csvFile 	= CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })
   	timeStart 	= Time.now    
   	start_log file_name: fileName    
   	
      csvFile.each_with_index do |row, i|         

         supplier_id    = row[:supplier_id].to_s          
         frequency_type = row[:frequency_type]
         frequency_week = row[:frequency_week]
         frequency_day  = row[:frequency_day]

         if row[:all_branches].to_s.upcase == "YES"            
            x_branches = row[:excluded_from_all].to_s.downcase rescue ""
            x_branches = x_branches.split(",")
            x_branches = [""] unless x_branches.present?

            branches = self.branches.where("lower(name) NOT IN (?)", x_branches)            
         else
            i_branches = row[:branch_name].to_s.downcase rescue ""
            i_branches = i_branches.split(",")
            i_branches = [""] unless i_branches.present?

            branches = self.branches.where("lower(name) IN (?)", i_branches)            
         end

         # fetched branches
         branches.each do |branch|
            bsc_attr = {
               account_id: self.id,
               supplier_id: supplier_id,
               frequency_type: frequency_type,
               frequency_week: frequency_week,
               frequency_day: frequency_day
            }
            # add new row
            bsc = branch.branch_supplier_configurations.new(bsc_attr)
            # save the added row
            if bsc.save
               # after save, calculate the next po date
               bsc.calculate_next_po_date
            else
               # on error, put it in the log file
               log_error "row #{i}: BRANCH_ID: #{branch.id} | SUPPLIER ID: #{supplier_id} #{bsc.errors.full_messages.to_sentence}" unless bsc.errors.blank?
            end
         end

   	end

	   timeEnd   = Time.now
	   stop_log file_name: fileName
	   #  FileUtils.mv(filePath, self.archives_dir)
	   csvFile.close
	   puts "Import Users - Supplier ASSIGNMENT completed. Time spend #{timeEnd - timeStart} seconds"
  end

end


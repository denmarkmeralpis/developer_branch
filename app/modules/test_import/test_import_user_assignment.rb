module TestImport::TestImportUserAssignment

  	def user_assignment_import(fileName = 'user-supplier.csv')	

   	temp_filePath ||= File.join(self.account_dir, fileName)
   	filePath = Dir.glob("#{temp_filePath}").first	

   	if File.exists?(filePath)
   	  puts "Import Users Assignment"
   	else
   		puts "Import CSV Not found"
   	  return
   	end	
   	
   	csvFile 	= CSV.open(filePath, 'r:ISO-8859-1', { headers: true, header_converters: :symbol })
   	timeStart 	= Time.now    
   	start_log file_name: fileName    

      ownership_columns = [:account_id, :supplier_id, :user_id, :product_category_id]
      ownership_data    = []

   	csvFile.each_with_index do |row, i|
   		user = self.users.find_by_email(row[:user_email])
   		if user.present?
            supplier = self.suppliers.find(row[:supplier_id]) rescue nil
            if supplier.present?
               user.supervisor.buyer_suppliers.create(supplier_id: row[:supplier_id]) if user.role == 'buyer'            
               buyer_supplier = user.buyer_suppliers.new(supplier_id: row[:supplier_id])               
               if buyer_supplier.save                                 
                  supplier_categories = supplier.products.select("DISTINCT product_category_id").pluck(:product_category_id)                                 
                  supplier_categories.each { |sc| ownership_data.push([self.id, supplier.id, user.id, sc]) }
               else
                  log_error "row #{i}: #{buyer_supplier.errors.full_messages.to_sentence}" unless buyer_supplier.errors.blank?
               end            
            end            
   		end
   	end

      Ownership.import ownership_columns, ownership_data, { validate: true }

	   timeEnd   = Time.now
	   stop_log file_name: fileName
	   # FileUtils.mv(filePath, self.archives_dir)
	   csvFile.close
	   puts "Import Users - Supplier ASSIGNMENT completed. Time spend #{timeEnd - timeStart} seconds"
  end

end


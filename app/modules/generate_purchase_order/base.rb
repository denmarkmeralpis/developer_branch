module GeneratePurchaseOrder::Base

  def gen_po
    supplier_config = self.branch_supplier_configurations.get_schedule
    fast_process_purchase_order(supplier_config, nil)
  end

  def gen_po_demo
    supplier_config = BranchSupplierConfiguration.joins(:supplier).where("suppliers.status = 'A' AND branch_supplier_configurations.next_po_date BETWEEN ? AND ?", Time.now, params[:end_date]).includes(:branch).includes(:supplier)
    fast_process_purchase_order(supplier_config, nil)
  end

  def manual_po_per_branch(branch_id, user_id, email, supplier_ids)
    supplier_config = self.branch_supplier_configurations.where(branch_id: branch_id, supplier_id: supplier_ids)
    fast_process_purchase_order(supplier_config, user_id)

    MerchantMailer.purchase_order(email).deliver
  end

  def reset_po
    PurchaseOrder.destroy_all
    PurchaseOrderLine.destroy_all
  end

  def generate_test_po(time_travel)
    Delorean.time_travel_to(time_travel) do
      gen_po
    end
  end

  private

  def fast_process_purchase_order(supplier_config, user_id)
    batch_po = []
    batch_po_columns = [:account_id, :assigned_to_user_id, :supplier_id, :created_by_id, :branch_id, :product_category_id, :cancellation_date, :due_date, :reference, :status,
                        :total_po_cost, :total_item_discount, :grand_total, :gross_amount, :overall_discount, :total_units, :total_quantity]
    batch_po_lines = []
    batch_po_lines_columns = [:account_id, :product_id, :stock_keeping_days, :average, :average_daily_sales, :adjusted_average_daily_sales, :onhand_quantity, :suggested_quantity,
                              :quantity_to_purchase, :quantity_per_unit, :units_to_purchase, :purchase_price, :subtotal, :discount_text, :purchase_order_id, :ordering_unit]

    puts 'Start generating purchase orders'
    start_time = Time.now
    puts start_time
    # first loop
    supplier_config.each do |branch_supplier_config|
      # Configuration Details
      supplier      = branch_supplier_config.supplier
      branch_id     = branch_supplier_config.branch_id
      days_to_stock = branch_supplier_config.days_to_stock

      # Get Discount for supplier
      overall_discount = supplier.discount_text

      po_products = supplier.branch_products.active_branch_products(branch_id)

      if po_products.present?
        categories  = po_products.select('products.product_category_id').map(&:product_category_id).uniq

        if user_id.nil?
          ownerships = self.ownerships.where(supplier_id: supplier.id, product_category_id: categories)
        else
          ownerships = self.ownerships.where(supplier_id: supplier.id, product_category_id: categories, user_id: user_id)
        end

        # second loop
        ownerships.each do |ownership|
          reference = DateTime.now.strftime('%Q').to_s.to_i
          po = [self.id, ownership.user_id, supplier.id, user_id, branch_id, ownership.product_category_id, DateTime.now + 15.days, DateTime.now + 2.days, reference, 'P']

          total_po_cost       = 0.0
          total_item_discount = 0.0
          total_units = 0
          total_quantity = 0

          po_content = po_products.where(products:{product_category_id: ownership.product_category_id}).map(&:product)

          po_content.each do |po_product|

            if po_product.ordering_unit == 1
              ordering_product = po_product 
            else
              ordering_product = po_products.where(products:{parent_product_id: po_product.id, ordering_unit: 1}).map(&:product).first 
            end

            ordering_product = po_product if ordering_product.nil?
            ordering_unit = ordering_product.base_unit_name 

            qpu = ordering_product.unit_content_quantity rescue po_product.unit_content_quantity

            #############
            last_po = po_product.purchase_order_lines.order('id DESC').first

            ### Get Last PO Date
            last_po_date = last_po.created_at rescue nil

            ### Get Transactions
            if !last_po_date.nil?
              transaction = po_product.transactions.where('type_code = ? AND created_at > ? AND quantity <> 0 AND last_inventory > 0', 'POS', last_po_date).order('id DESC')
            else
              transaction = po_product.transactions.where('type_code = ? AND quantity <> 0 AND last_inventory > 0', 'POS').order('id DESC') 
            end

            average       = BigDecimal.new(transaction.average(:quantity).to_s)
            on_hand_qty   = transaction.last.last_inventory rescue 0 

            # if onhand_qty is less than 1, onhand should not be subtracted to the suggested_qty
            on_hand_qty = 0 if on_hand_qty < 1
            last_average  = BigDecimal.new(last_po.average.to_s) rescue average

            new_average   = (last_average + average) / 2
            suggested_qty = (new_average * days_to_stock) - on_hand_qty
            ###################

            qtp = suggested_qty 
            utp = (qtp / qpu).round
            utp = 1 if utp == 0
            pp  = BigDecimal.new(ordering_product.cost.to_s)
            st  = (utp * pp)

            old_st = st

            discount_txt = ordering_product.discount_text rescue '0'
            discount_txt = '0' if discount_txt.nil?

            # Discount Computation
            discount_txt.split(",").each do |discount|
              if discount.last == "%"
                st -= st * (BigDecimal.new(discount) / 100)
              elsif discount.last != "%"
                st -= BigDecimal.new(discount)
              end
            end            

            if suggested_qty > 0            
              total_item_discount += old_st - st

              # For po table
              total_po_cost  += old_st
              total_units    += utp
              total_quantity += qtp

              batch_po_lines << [self.id, po_product.id, days_to_stock, new_average, new_average, new_average, on_hand_qty, suggested_qty, qtp, qpu, utp, pp, st, discount_txt, reference, ordering_unit]            
            end
          end

          net_total = (total_po_cost - total_item_discount)
          final_net_total = net_total;


          overall_discount.split(',').each do |discount|
            if discount.last == '%'
              final_net_total -= net_total * (discount.to_f / 100)
            else
              final_net_total -= discount.to_f
            end
          end

          if net_total > 0 || user_id.present?
            po << total_po_cost # GTC
            po << total_item_discount # TID
            po << final_net_total # NA
            po << net_total # GT
            po << overall_discount # OD
            po << total_units
            po << total_quantity

            batch_po << po
          end                    
        end #end of second loop
      end
      branch_supplier_config.calculate_next_po_date
    end #end of first loop
    PurchaseOrder.import batch_po_columns, batch_po
    PurchaseOrderLine.import batch_po_lines_columns, batch_po_lines

    end_time = Time.now
    puts "Finished generating Purchase Orders"
    puts end_time
    elapsed = (end_time - start_time).to_i
    puts "Finished in #{elapsed / 60} minutes"
    puts "Finished in #{elapsed % 60} seconds"
    puts "Finished in #{(end_time - start_time) * 1000} milliseconds"
  end

  # def process_purchase_order(supplier_config, user_id)
  #   # First Loop
  #   supplier_config.each do |branch_supplier_config|

  #     # Configuration Details
  #     supplier      = branch_supplier_config.supplier
  #     branch_id     = branch_supplier_config.branch_id
  #     days_to_stock = branch_supplier_config.days_to_stock

  #     # Get Discount for supplier
  #     overall_discount = supplier.discount_text

  #     # Get Products by Category
  #     po_products = supplier.branch_products.active_branch_products(branch_id)
  #     categories  = po_products.group('products.product_category_id').count

  #     ## Second Loop
  #     categories.each do |category_id, po_line_number|

  #       ## check if ownership
  #       if user_id.nil?
  #         ownerships = self.ownerships.where(supplier_id: supplier.id, product_category_id: category_id)
  #       else
  #         ownerships = self.ownerships.where(supplier_id: supplier.id, product_category_id: category_id, user_id: user_id)
  #       end

  #       #### Fourth Loop ownership
  #       ownerships.each do |ownership|

  #         ## Generate PO
  #         new_po_attr = {
  #             assigned_to_user_id: ownership.user_id,
  #             supplier_id:         supplier.id,
  #             created_by_id:       user_id,
  #             branch_id:           branch_id,
  #             product_category_id: category_id,
  #             cancellation_date:   DateTime.now + 15.days,
  #             reference:           DateTime.now.strftime('%Q').to_s,
  #             status:              :Pending
  #         }

  #         total_po_cost       = 0.0
  #         total_item_discount = 0.0

  #         new_po = self.purchase_orders.create(new_po_attr)
  #         ## End of Generate PO

  #         ## Get products of current category
  #         po_content = po_products.where("products.product_category_id = ?", category_id)

  #         ### Third Loop for PO Lines
  #         po_content.each do |product|

  #           ### Create PO Lines
  #           po_product = self.products.find(product.product_id)

  #           ### Get default ordering unit
  #           qpu = po_product.find_by_ordering_unit(1).unit_content_quantity rescue 1

  #           ### Get Last PO
  #           last_po = po_product.purchase_order_lines.order('id DESC').first

  #           ### Get Last PO Date
  #           last_po_date = last_po.created_at rescue nil

  #           ### Get Transactions
  #           if !last_po_date.nil?
  #             transaction = po_product.transactions.where('type_code = ? AND created_at > ?', 'POS', last_po_date).order('id DESC')
  #           else
  #             transaction = po_product.transactions.where('type_code = ?', 'POS').order('id DESC')
  #           end

  #           transaction = po_product.transactions.where('type_code = ?', 'POS').order('id DESC')

  #           average       = BigDecimal.new(transaction.average(:quantity).to_s)
  #           on_hand_qty   = transaction.last.last_inventory

  #           # if onhand_qty is less than 1, onhand should not be subtracted to the suggested_qty
  #           on_hand_qty = 0 if on_hand_qty < 1
  #           last_average  = BigDecimal.new(last_po.average.to_s) rescue average

  #           new_average   = (last_average + average) / 2
  #           suggested_qty = (new_average * days_to_stock) - on_hand_qty


  #           qtp = (suggested_qty * po_product.unit_content_quantity).round
  #           utp = qtp / qpu
  #           pp  = BigDecimal.new(po_product.cost.to_s)
  #           st  = utp * pp

  #           old_st = st

  #           discount_txt = po_product.discount_text rescue '0'
  #           discount_txt = '0' if discount_txt.nil?

  #           # Discount Computation
  #           discount_txt.split(",").each do |discount|
  #             if discount.last == "%"
  #               st -= st * (BigDecimal.new(discount) / 100)
  #             elsif discount.last != "%"
  #               st -= BigDecimal.new(discount)
  #             end
  #           end

  #           total_item_discount += old_st - st

  #           # For po table
  #           total_po_cost += old_st

  #           if suggested_qty > 0
  #             po_lines_attr = {
  #                 account_id:           self.id,
  #                 product_id:           po_product.id,
  #                 stock_keeping_days:   days_to_stock,
  #                 average:              new_average,
  #                 average_daily_sales:  new_average,
  #                 adjusted_average_daily_sales: new_average,
  #                 onhand_quantity:      on_hand_qty,
  #                 suggested_quantity:   suggested_qty,
  #                 quantity_to_purchase: qtp,
  #                 quantity_per_unit:    qpu,
  #                 units_to_purchase:    utp,
  #                 purchase_price:       pp,
  #                 subtotal:             st,
  #                 discount_text:        discount_txt
  #             }
  #             new_po.purchase_order_lines.create(po_lines_attr)
  #           end

  #         end
  #         ### End of Third Loop

  #         # destroys PO id there is no PO lines
  #         if new_po.purchase_order_lines.empty?
  #           new_po.destroy
  #         else

  #           net_total = (total_po_cost - total_item_discount)
  #           final_net_total = net_total;

  #           overall_discount.split(',').each do |discount|
  #             if discount.last == '%'
  #               final_net_total -= net_total * (discount.to_f / 100)
  #             else
  #               final_net_total -= discount.to_f
  #             end
  #           end
            
  #           updated_attr = {
  #               total_po_cost:       total_po_cost, # GTC
  #               total_item_discount: total_item_discount, # TID
  #               grand_total:         final_net_total, # NA
  #               gross_amount:        net_total, # GT
  #               overall_discount:    overall_discount # OD
  #           }
  #           new_po.update_attributes(updated_attr)
  #         end

  #       end
  #       #### End of Fourth Loop

  #     end
  #     ## End of Second Loop
  #     branch_supplier_config.calculate_next_po_date
  #   end
  #   # End of First Loop
  # end

end
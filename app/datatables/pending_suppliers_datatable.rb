class PendingSuppliersDatatable

  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, type, branch_or_user)
    @view      = view
    @type      = type
    @assigned  = branch_or_user
    @object    = type == :user ? branch_or_user.account : branch_or_user    
  end

  def as_json(options = {})
    {
        sEcho:                params[:sEcho].to_i,
        iTotalRecords:        suppliers.count,
        iTotalDisplayRecords: suppliers.count,
        aaData:               data
    }
  end

  private

  def data 
    no = 0
    if @type == :supplier
      suppliers.map do |s|
        [
          no += 1,
          s.supplier.name,
          no == 1 ? javascripts_user(s) : action_user(s)
        ]
      end
    elsif @type == :user
      suppliers.map do |s|
        [
          no += 1,
          s.supplier.name,
          no == 1 ? javascripts_user(s) : action_user(s)
        ]
      end
    else      
      suppliers.map do |s|
        [
          no += 1,
          s.supplier.name,
          s.supplier.created_at.strftime('%m-%d-%Y %r'),
          s.supplier.updated_at.strftime('%m-%d-%Y %r'),          
          no == 1 ? javascrpts_owner(s) : action_owner(s)
        ]
      end
    end
  end

  def action_owner(s)
    o_action =  "<div class='span assign-btn-b btn btn-primary btn-xs' data-supplier-id='#{s.supplier.id}' id='assign-btn-#{s.supplier.id}' data-loading-text='Loading...' data-target='.show-schedule-modal' data-toggle='modal' view='N'>"
    o_action += "   Assign"
    o_action += "</div>"
    o_action.html_safe     
  end

  def action_user(s)
    u_action =  "<div class='span assign-btn-u btn btn-primary btn-xs' id='assign-action-#{s.supplier.id}' data-supplier-id='#{s.supplier.id}' data-loading-text='Loading...'>"
    u_action += "   Assign"
    u_action += "</div>"
    u_action.html_safe
  end

  def javascrpts_owner(s)
    o_js =  "<div class='span assign-btn-b btn btn-primary btn-xs' data-supplier-id='#{s.supplier.id}' id='assign-btn-#{s.supplier.id}' data-loading-text='Loading...' data-target='.show-schedule-modal' data-toggle='modal' view='N'>Assign</div>"
    o_js += "<script type='text/javascript' src='/assets/assigning-distributors.js'></script>".html_safe    
  end

  def javascripts_user(s)
    u_js =  "<div class='span assign-btn-u btn btn-primary btn-xs' id='assign-action-#{s.supplier.id}' data-supplier-id='#{s.supplier.id}' data-loading-text='Loading...'>"
    u_js += "   Assign"
    u_js += "</div>"
    u_js += "<script type='text/javascript' src='/assets/assigning-distributors.js'></script>"
    u_js.html_safe
  end

  def suppliers
    @suppliers ||= fetch_suppliers
  end

  def fetch_suppliers
    assigned_id = @assigned.suppliers.pluck(:id)

    if assigned_id.blank?
      distributors = @object.branch_products
          .select("DISTINCT supplier_id")
          .joins(:supplier)
          .uniq
          .order("#{sort_column} #{sort_direction}")
          .page(page)
          .per_page(per_page)      
    else      
      distributors = @object.branch_products
          .select("DISTINCT supplier_id")
          .where("supplier_id NOT IN (?)", assigned_id)
          .joins(:supplier)
          .uniq
          .order("#{sort_column} #{sort_direction}")
          .page(page)
          .per_page(per_page)            
    end

    if params[:sSearch].present?
      distributors = distributors.where("suppliers.name like :search", search: "%#{params[:sSearch]}%")
    end

    distributors
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[suppliers.name suppliers.contact_email suppliers.address suppliers.discount_text suppliers.is_principal suppliers.status]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end








class ProductsDatatable

  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, supplier, merchant)
    @merchant = merchant
    @supplier = supplier
    @view     = view
  end

  def as_json(options = {})
    {
      sEcho:                params[:sEcho].to_i,
      iTotalRecords:        @supplier.supplier_products.count,
      iTotalDisplayRecords: products.count,
      aaData:               data
    }
  end

  private

  def data
    merchant_roles = @merchant.roles.map(&:name)
    products.map do |sp|
      if merchant_roles.include?('spr')
        if sp.status == 'A'
          css = 'btn-warning'
          string_status = 'Deactivate'
        else
          css = 'btn-success'
          string_status = 'Activate'
        end
        link = link_to(string_status, activate_product_supplier_path(@supplier.id, product_id: sp.product.id), class: "btn btn-xs #{css}")
      end
      
      [
        sp.product.sku,
        sp.product.name,
        sp.product.barcode,
        sp.product.base_unit_name,
        sp.product.stock_no,
        sp.cost,
        link
      ]
    end
  end

  def products
    @products ||= fetch_products
  end

  def fetch_products
    products = @supplier.supplier_products.includes(:product)
    products = products.joins(:product).order("#{sort_column} #{sort_direction}")
    products = products.page(page).per_page(per_page)
    products = products.where("products.name like :search", search: "%#{params[:sSearch]}%") unless params[:sSearch].blank?
    products
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[products.sku products.name products.barcode products.unit products.stock_no cost]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
class AssignedSuppliersDatatable

  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, supplier, type, user)
    @supplier = supplier
    @view     = view
    @type     = type
    @user     = user
  end

  def as_json(options = {})
    {
        sEcho:                params[:sEcho].to_i,
        iTotalRecords:        @supplier.count,
        iTotalDisplayRecords: suppliers.count,
        aaData:               data
    }
  end

  private

  def user_ac(supplier)
    u_ac =  "<div class='span unassign-btn-u btn btn-warning btn-xs' id='unassign-btn-#{supplier.id}' data-supplier-id='#{supplier.id}' data-loading-text='Loading...'>"
    u_ac += "   Unassign"
    u_ac += "</div>"    
  end

  def user_js(supplier)
    u_js =  user_ac(supplier)
    u_js += "<script type='text/javascript' src='/assets/assigning-distributors.js'></script>"
    u_js.html_safe
  end

  def supplier_ac(supplier)
    s_ac =  "<div class='span unassign-btn btn btn-warning btn-xs' id='unassign-btn-#{supplier.id}' data-loading-text='Loading...' data-supplier-id='#{supplier.id}'>"             
    s_ac += "   Unassign"
    s_ac += "</div>"
  end

  def supplier_js(supplier)
    s_js =  supplier_ac(supplier)
    s_js += "<script type='text/javascript' src='/assets/assigning-distributors.js'></script>"
  end

  def data
    no = 0
    if @type == "supplier"
      suppliers.map do |supplier|
        [
          no += 1,
          supplier.name,
          no == 1 ? supplier_js(supplier) : supplier_ac(supplier).html_safe
        ]
      end
    elsif @type == "user"
      suppliers.map do |supplier|
        [
          no += 1,
          supplier.name,
          no == 1 ? user_js(supplier) : user_ac(supplier).html_safe
        ]
      end
    else
      suppliers.map do |supplier|

        # ROLE TYPE (User Priv)
        if @user.role == 'owner'
          sched = supplier.branch_supplier_configurations.find_by_branch_id(params[:id])
          action = "<div class='span unassign-btn-b btn btn-warning btn-xs' id='unassign-btn-#{supplier.id}' data-loading-text='Loading...' data-supplier-id='#{supplier.id}'>Unassign</div>"
        else
          sched = supplier.branch_supplier_configurations.find_by_branch_id(params[:id])
          action = ""
        end

        # HTML for SCHEDULES
        if sched.frequency_type == 'weekly'
            schedule = "<div class='well-sm3 bg-primary span sched-btn btn' data-supplier-id='#{supplier.id}' data-target='.show-schedule-modal' data-toggle='modal' frequency_day='#{sched.frequency_day}' frequency_type='#{sched.frequency_type}' frequency_week='#{sched.frequency_week}' sched_id='#{sched.id}'>
            <b>TYPE:</b> #{sched.frequency_type.titleize.capitalize} <br>
            <b>DAY:</b>  #{sched.frequency_day.capitalize} <br>
            </div>"
        else
          schedule = "<div class='well-sm3 bg-primary span sched-btn btn' data-supplier-id='#{supplier.id}' data-target='.show-schedule-modal' data-toggle='modal' frequency_day='#{sched.frequency_day}' frequency_type='#{sched.frequency_type}' frequency_week='#{sched.frequency_week}' sched_id='#{sched.id}'>
          <b>TYPE:</b> #{sched.frequency_type.titleize.capitalize} <br>
          <b>WEEK:</b> #{sched.frequency_week.split("_").to_sentence.capitalize} <br>
          <b>DAY:</b>  #{sched.frequency_day.capitalize} <br>
          </div>"
        end

        [
          no += 1,
          supplier.name,
          supplier.updated_at.strftime('%m-%d-%Y %r'),
          if no == 1 && @user.role != 'owner'
            ( schedule +  "<script type='text/javascript' src='/assets/assigning-distributors.js'></script>").html_safe 
          else
            schedule.html_safe
          end,
          if no == 1
            ( action +  "<script type='text/javascript' src='/assets/assigning-distributors.js'></script>").html_safe 
          else
            action.html_safe
          end
        ]
      end
    end
  end


  def suppliers
    @suppliers ||= fetch_suppliers
  end

  def fetch_suppliers
    suppliers = @supplier.order("#{sort_column} #{sort_direction}")
    suppliers = suppliers.page(page).per_page(per_page)
    if params[:sSearch].present?
      suppliers = suppliers.where("name like :search", search: "%#{params[:sSearch]}%")
    end
    suppliers.uniq
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[name contact_email address discount_text is_principal status]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end








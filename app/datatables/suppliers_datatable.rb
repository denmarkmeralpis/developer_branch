class SuppliersDatatable

  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, account, merchant)
    @account  = account
    @merchant = merchant
    @view     = view
    @role     = merchant.role
  end

  def as_json(options = {})
    {
        sEcho:                params[:sEcho].to_i,
        iTotalRecords:        suppliers.count,
        iTotalDisplayRecords: suppliers.count,
        aaData:               data
    }
  end

  private

  def data
    merchant_abilities  = @merchant.roles.map(&:name)
    merchant_role       = @merchant.role
    suppliers.map do |supplier|
      
      if merchant_abilities.include?("ssu")
        if supplier.status == 'A'
          css = 'btn-warning'
          string_status = 'Deactivate'
        else
          css = 'btn-success'
          string_status = 'Activate'
        end
      end
                
      [
        link_to(supplier.name, "/suppliers/#{supplier.id}/products"), #supplier.name, 
        supplier.contact_email,
        supplier.address,
        supplier.discount_text,
        (supplier.is_principal ? 'Principal Supplier' : 'Distributor'),
        [
          link_to('Delete', supplier, class: 'btn btn-xs btn-danger', data: {confirm: "Delete #{supplier.name}?"}, method: :delete),
          link_to(string_status, activate_supplier_path(supplier), class: "btn btn-xs #{css}", action: :activate)
        ]
      ]
    end
  end

  def suppliers
    @suppliers ||= fetch_suppliers
  end

  def fetch_suppliers    
    if @merchant.role == 'owner'
      _suppliers = @account.suppliers.notdeleted.order("#{sort_column} #{sort_direction}").page(page).per_page(per_page)  
    else
      _suppliers = @merchant.suppliers.notdeleted.order("#{sort_column} #{sort_direction}").page(page).per_page(per_page)  
    end

    if params[:sSearch].present?
      _suppliers = _suppliers.where("name like :search", search: "%#{params[:sSearch]}%")
    end

    _suppliers
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[name contact_email address discount_text is_principal status]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
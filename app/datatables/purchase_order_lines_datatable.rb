class PurchaseOrderLinesDatatable

  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, :number_with_precision, :number_with_delimiter, to: :@view

  def initialize(view, purchase_order_line, edit_price, merchant)
    @my_purchase_order_lines  = purchase_order_line
    @merchant   = merchant
    @view       = view
    @edit_price = edit_price
  end

  def as_json(options = {})
    {
      sEcho:                params[:sEcho].to_i,
      iTotalRecords:        @my_purchase_order_lines.count,
      iTotalDisplayRecords: purchase_order_lines.count,
      aaData:               data
    }
  end

  private

  def data
    no = 0
    purchase_order_lines.map do |line|
      [
          "<ul class='order-lines'><li><label>#{no += 1}</label></li></ul>",
          "<ul class='order-lines'>
          <li>
            <label class='product-name-#{line.id}'>#{line.product.name}</label>
          </li>
          <li> #{line.product.sku} / #{line.product.barcode}</li>
          <li><br></li>
          <li> <strong>SRP:</strong> <span id='srp-#{line.id}'>P#{line.product.retail_price}</span> </li>
          <li>
            <br>
            <!-- other details -->
            <div class='hidden' style='display: none;'>
              <span id='last-received-date-#{line.id}'>
                #{line.product.branch_product.last_received_date rescue '--'}
              </span>
              <span id='last-received-qty-#{line.id}'>
                #{line.product.branch_product.last_received_quantity rescue '--'}
              </span>              
            </div>
          </li>
          <li>
            <center><span class='btn btn-xs btn-primary graph-btn' data-line-id='#{line.id}' id='graph-btn-#{line.id}' >Graph</span></center>            
          </li>
          <li style='margin-top: 2px;'></li>
          <li>
            <span class='btn btn-xs btn-success order-to-supplier-btn' data-line-id='#{line.id}' data-loading-text='Loading...' id='order-to-supplier-btn-#{line.id}' style='visibility: hidden; width: 115px;'>Order from supplier</span>
          </li>
        </ul>".html_safe,
          "<ul class='order-lines'>
          <li>
            <label class='ads-#{line.id}' data-value='#{line.adjusted_average_daily_sales}'>#{line.adjusted_average_daily_sales}</label>
          <li>average daily sales</li>
          <li class='spacer'></li>
          <li>
            <input class='dts dts-#{line.id} input-#{line.id} input-po' type='text' value='#{line.stock_keeping_days}'>
          </li>
          <li>days to stock</li>
          <li class='spacer'></li>
          <li>
            <input class='input-#{line.id} input-po ohq ohq-#{line.id}' type='text' value='#{line.onhand_quantity}'>
          </li>
          <li>onhand quantity</li>
          <li class='spacer'></li>
          <li>
            <label class='sq-#{line.id}' data-value='#{line.suggested_quantity.round}'>#{line.suggested_quantity.round}</label>
          </li>
          <li>suggested quantity</li>
        </ul>".html_safe,
          get_order_details(line),
          if no == 1
            (get_cost(line) + "
              <script type='text/javascript' src='/assets/purchase_order_lines/modal-graphs.js'></script>              
              <script type='text/javascript' src='/assets/purchase_order_lines/po-lines.prog.js'></script>
              <script type='text/javascript' src='/assets/purchase_order_lines/modal-order-to-supplier.js'></script>"
            ).html_safe
          else
            get_cost(line).html_safe
          end,
        get_last_html(line).html_safe
      ]
    end

  end

  def get_last_html(line)
    "<div class='order-lines'>
      <li>
        <span class='btn btn-warning btn-xs col-xs-12 edit-action-btn' data-line-id='#{line.id}' id='edit-btn-#{line.id}' style='display: block;'>edit</span>
        <span class='btn btn-success btn-xs col-xs-12 save-action-btn' data-line-id='#{line.id}' id='save-btn-#{line.id}' style='display: none;'>save</span>
      </li>
      <li>
        <br>
        <br>
        <span class='btn btn-danger btn-xs cancel-action-btn col-xs-12' data-line-id='#{line.id}' id='cancel-btn-#{line.id}' style='display: none;'>cancel</span>
      </li>
      <li>
        <span class='btn btn-primary btn-xs del-po-line col-xs-12' id='delete-btn-#{line.id}' data-line-id='#{line.id}' style='visibility: hidden; margin-top: 2px;'>delete</span> 
      </li> 
    </div>"
  end

  def get_cost(line)
    front_html = "<ul class='order-lines'>
                    <li>"
    if @edit_price == true
      middle_html = "<input class='input-po pp pp-#{line.id} input-#{line.id}' type='text' value='#{line.purchase_price}'>"
    else
      middle_html = "<input class='pp pp-#{line.id}' disabled style='background-color: transparent; border: none; font-weight: bold; text-align: center; ' type='text' value='#{line.purchase_price}'>"
    end

    discount_txt = line.discount_text == '' || line.discount_text.nil? ? 0 : line.discount_text 

    last_html = "</li>
            <li>purchase price</li>
            <li class='spacer'></li>
            <li>              
              <input class='d d-#{line.id} input-#{line.id} input-po' type='text' value='#{discount_txt}'>
            </li>
            <li>discount(s)</li>
            <li class='spacer'></li>
            <li>
              <span class='input-po subtotal subtotal-#{line.id}'>
                P
                #{number_with_delimiter(number_with_precision(line.subtotal, precision: 2), delimiter: ',')}
              </span>
            </li>
            <li>subtotal</li>
            </ul>"
    (front_html + middle_html + last_html)
  end

  def get_order_details(line)
    if line.product.base_unit_name == line.ordering_unit
      front_html = "<ul class='order-lines'>
            <li>
              <input class='input-#{line.id} input-po qtp qtp-#{line.id}' type='text' value='#{line.quantity_to_purchase.round}'>
            </li>
              <li>quantity to purchase</li>
              <li class='spacer'></li>
              <li>
                <select class='input-#{line.id} input-po qpu qpu-#{line.id}'>
                  <option cost='#{line.product.cost}' selected value='#{line.product.unit_content_quantity.to_f}' unit_name='#{line.product.base_unit_name}'>
                    #{line.product.unit_content_quantity} / #{line.product.base_unit_name}"
    else
      front_html = "<ul class='order-lines'>
            <li>
              <input class='input-#{line.id} input-po qtp qtp-#{line.id}' type='text' value='#{line.quantity_to_purchase.round}'>
            </li>
              <li>quantity to purchase</li>
              <li class='spacer'></li>
              <li>
                <select class='input-#{line.id} input-po qpu qpu-#{line.id}'>
                  <option cost='#{line.product.cost}' value='#{line.product.unit_content_quantity.to_f}' unit_name='#{line.product.base_unit_name}'>
                    #{line.product.unit_content_quantity} / #{line.product.base_unit_name}"
    end
    append_html = ''
    line.product.product_units.each do |u|
      if u.base_unit_name == line.ordering_unit
        append_html << "<option selected value='#{u.unit_content_quantity.to_f}' cost='#{u.cost}' unit_name='#{u.base_unit_name}'>
              #{u.unit_content_quantity} / #{u.base_unit_name}
            </option>"
      else
         append_html << "<option value='#{u.unit_content_quantity.to_f}' cost='#{u.cost}' unit_name='#{u.base_unit_name}'>
              #{u.unit_content_quantity} / #{u.base_unit_name}
            </option>"
      end
    end
    front_html = front_html + append_html
    (front_html +
        "</select>
            </li>
            <li>quantity per unit</li>
            <li class='spacer'></li>
            <li>
              <input class='input-#{line.id} input-po utp utp-#{line.id}' placeholder='e.g 100' type='text' value='#{line.units_to_purchase.round}'>
            </li>
            <li>units to purchase</li>").html_safe
  end

  def purchase_order_lines
    @purchase_order_lines ||= fetch_purchase_order_lines
  end

  def fetch_purchase_order_lines
    purchase_order_lines = @my_purchase_order_lines.order("#{sort_column} #{sort_direction}").page(page).per_page(per_page)
    if params[:sSearch].present?
      purchase_order_lines = purchase_order_lines.where("(products.name like :search) 
      OR (products.sku like :search)
      OR (products.barcode like :search)", search: "%#{params[:sSearch]}%")
    end        
    purchase_order_lines
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[purchase_order_lines.id]
    columns[params[:iSortCol_1].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "asc" : "desc"
  end
end
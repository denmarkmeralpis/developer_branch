class PurchaseOrdersDatatable

  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, purchase_order, merchant)
    @my_purchase_orders  = purchase_order
    @merchant   = merchant
    @view       = view
  end

  def as_json(options = {})
    {
      sEcho:                params[:sEcho].to_i,
      iTotalRecords:        @my_purchase_orders.count,
      iTotalDisplayRecords: purchase_orders.count,
      aaData:               data
    }
  end

  private

  def data
    no = 0
    purchase_orders.map do |purchase_order|
      [
        no += 1,
        purchase_order.reference,
        link_to(purchase_order.supplier.name, purchase_order_lines_purchase_order_path(purchase_order.id)),
        purchase_order.branch.name,
        purchase_order.product_category.name,
        purchase_order.status.gsub(/[PCVA]/, 'P' => 'Pending', 'C' => 'Confirmed', 'V' => 'Voided', 'A' => 'Approved'),
        purchase_order.created_at.strftime('%m-%d-%Y %r'),
        if no == 1
          ("<div class='span btn btn-info btn-xs copy-po' data-target='#copy-po-modal' data-supplier-id='#{purchase_order.supplier_id}' data-branch-id='#{purchase_order.branch_id}' data-po-id='#{purchase_order.id}'>Duplicate</div>" +
              "<script type='text/javascript' src='/assets/copy-po.js'></script>").html_safe
        else
          "<div class='span btn btn-info btn-xs copy-po' data-target='#copy-po-modal' data-supplier-id='#{purchase_order.supplier_id}' data-branch-id='#{purchase_order.branch_id}' data-po-id='#{purchase_order.id}'>Duplicate</div>".html_safe
        end
      ]
    end
  end

  def purchase_orders
    @purchase_orders ||= fetch_purchase_orders
  end

  def fetch_purchase_orders
    purchase_orders_list = @my_purchase_orders.order("#{sort_column} #{sort_direction}").page(page).per_page(per_page)
    purchase_orders_list = purchase_orders_list.where("(suppliers.name like :search)
      OR (purchase_orders.reference like :search)
      OR (product_categories.name like :search)
      OR (branches.name like :search)", search: "%#{params[:sSearch]}%") if params[:sSearch].present?
    purchase_orders_list.uniq
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[suppliers.name]
    columns[params[:iSortCol_2].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
class UsersDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, account, merchant)
    @merchant = merchant
    @account = account
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: @account.users.nodeleted(@merchant).count,
      iTotalDisplayRecords: users.count,
      aaData: data
    }
  end

  private

  def data
    users.map do |user|
      if user.status == 'A'
        css = 'btn-warning'
        status_string = 'Deactivate'
      else 
        css = 'btn-success'
        status_string = 'Activate'
      end
      [
        user.first_name,
        user.last_name,
        user.email,
        user.created_at.strftime('%m-%d-%Y %I:%M %p'),
        user.role,
        [
          link_to('Edit', edit_user_path(user), class: 'btn btn-xs btn-info'),
          link_to('Delete', user, class: 'btn btn-xs btn-danger', data: {confirm: "Delete #{user.first_name}?"}, method: :delete),
          link_to(status_string, activate_user_path(user), class: "btn btn-xs #{css}")
        ]
      ]
    end
  end

  def users
    @users ||= fetch_users
  end

  def fetch_users
    users = @account.users.nodeleted(@merchant)
    users = users.order("#{sort_column} #{sort_direction}")
    users = users.page(page).per_page(per_page)
    users = users.where("(first_name like :search) 
      OR (last_name like :search)
      OR (role like :search)
      OR (email like :search)", search: "%#{params[:sSearch]}%") if params[:sSearch].present?
    users = users.uniq
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[first_name last_name email created_at role status]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end

end
class SuppliersDatatable2

  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, supplier, merchant, show_all)
    @my_supplier  = supplier
    @merchant   = merchant
    @view       = view
    @show_all = show_all
  end

  def as_json(options = {})
    {
      sEcho:                params[:sEcho].to_i,
      iTotalRecords:        @my_supplier.count,
      iTotalDisplayRecords: suppliers.count,
      aaData:               data
    }
  end

  private

  def data
    suppliers.map do |sup|

      link = link_to(sup.name, "/suppliers/#{sup.id}/products")

      if sup.purchase_orders.where("created_at >= ?", Time.zone.now.beginning_of_day).count <= 0
        progress_bar = "<div class= 'progress'><div class='progress-bar progress-bar-no-po'></div></div>"
      else
        pos = sup.purchase_orders.where("created_at >= ?", Time.zone.now.beginning_of_day)
        if @merchant.role == 'buyer'
          group_count = pos.where("assigned_to_user_id = ?", @merchant.id).group(:status).count 
        elsif @merchant.role == 'supervisor'
          buyers = @merchant.buyers.map(&:id)
          group_count = pos.where("assigned_to_user_id IN (?)", buyers).group(:status).count
        else
          group_count = pos.group(:status).count 
        end

        _pending_count = 0
        _approved_count = 0
        _confirmed_count = 0

        group_count.each do |status, count|
          _pending_count = count.to_f if status == 'P'
          _approved_count = count.to_f if status == 'A'
          _confirmed_count = count.to_f if status == 'C'
        end 

        _combined = _pending_count + _approved_count + _confirmed_count

        if _combined == 0
           progress_bar = "<div class= 'progress'><div class='progress-bar progress-bar-no-po'></div></div>"
        else

          pending_percentage = (_pending_count / _combined) * 100 
          approved_percentage = (_approved_count / _combined) * 100
          confirmed_percentage = (_confirmed_count / _combined) * 100
          
          pending_percentage == 0 ? pending = '' : pending = "<div class='progress-bar progress-bar-warning' style='width: #{pending_percentage}%'> <span class='glyphicon glyphicon-time'> #{_pending_count.to_i} </span></div>"
          approved_percentage == 0 ? approve = '' : approve = "<div class='progress-bar progress-bar-success' style='width: #{approved_percentage}%'> <span class='glyphicon glyphicon-ok'> #{_approved_count.to_i} </span></div>"
          confirmed_percentage == 0 ? confirm = '' : confirm = "<div class='progress-bar progress-bar-primary' style='width: #{confirmed_percentage}%'> <span class='glyphicon glyphicon-eye-open'> #{_confirmed_count.to_i}</span> </div>"

          progress_bar = "<div class= 'progress'>" + pending + confirm + approve + "</div>"
        end
        #progress_bar = "<div class= 'progress'><div class='progress-bar progress-bar-success' style='width: #{approved_percentage}%'>#{_approved_count.to_i}</div><div class='progress-bar progress-bar-warning' style='width: #{pending_percentage}%'>#{_pending_count.to_i}</div></div>"
      end

      [
          link,
          progress_bar.html_safe
      ]

    end
  end  

  def suppliers
    @suppliers ||= fetch_suppliers
  end

  def fetch_suppliers
    if @show_all
      suppliers = @my_supplier.order("#{sort_column} #{sort_direction}").page(page).per_page(per_page)
    else
      suppliers = @my_supplier.joins(:purchase_orders).order("#{sort_column} #{sort_direction}").page(page).per_page(per_page)
    end
    
    suppliers = suppliers.where("suppliers.name like :search", search: "%#{params[:sSearch]}%") if params[:sSearch].present?
    suppliers.uniq
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[name name]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
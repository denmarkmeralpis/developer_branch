class PurchaseOrderLinesDatatable2

  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, :number_with_precision, :number_with_delimiter, to: :@view

  def initialize(view, purchase_order_line, edit_price, merchant)
    @my_purchase_order_lines  = purchase_order_line
    @merchant   = merchant
    @view       = view
    @edit_price = edit_price
  end

  def as_json(options = {})
    {
        sEcho:                params[:sEcho].to_i,
        iTotalRecords:        @my_purchase_order_lines.count,
        iTotalDisplayRecords: purchase_order_lines.count,
        aaData:               data
    }
  end

  private

  def data
    no = 0
    purchase_order_lines.map do |line|
      [
          "<ul class='order-lines'><li><label>#{no += 1}</label></li></ul>",
          "<ul class='order-lines'><label class='product-name-#{line.id}'>#{line.product.name}</label></ul>".html_safe,
          get_order_details(line),
          "<ul class='order-lines'><label class='product-name-#{line.id}'>#{line.purchase_price}</label></ul>".html_safe,
          "<ul class='order-lines'><label class='product-name-#{line.id}'>#{line.discount_text}</label></ul>".html_safe,
          "<ul class='order-lines'><label class='product-name-#{line.id}'><span class='input-po subtotal subtotal-#{line.id}'>P#{number_with_delimiter(number_with_precision(line.subtotal, precision: 2), delimiter: ',')}</span></label></ul>".html_safe          
      ]
    end

  end

  def get_order_details(line)
     if line.ordering_unit == line.product.base_unit_name
        unit = line.product
      else
        line.product.product_units.each do |x|
          if x.base_unit_name == line.ordering_unit
            unit = x
          end
        end
      end
     #x = line.product.product_units.find_by_unit_content_quantity(line.quantity_per_unit.to_f).base_unit_name rescue "PC."
     details = "<ul class='order-lines'><li><b>#{line.units_to_purchase.round} #{line.ordering_unit} </b> </li></ul>".html_safe
     (details)
  end

  def get_cost(line)
    front_html = "<ul class='order-lines'>
                    <li>"
    if @edit_price == true
      middle_html = "<input class='input-po pp pp-#{line.id} input-#{line.id}' type='text' value='#{line.purchase_price}'>"
    else
      middle_html = "<input class='pp pp-#{line.id}' disabled style='background-color: transparent; border: none; font-weight: bold; text-align: center; ' type='text' value='#{line.purchase_price}'>"
    end
    last_html = "</li>
            <li>purchase price</li>
            <li class='spacer'></li>
            <li>
              <input class='d d-#{line.id} input-#{line.id} input-po' type='text' value='#{line.discount_text}'>
            </li>
            <li>discount(s)</li>
            <li class='spacer'></li>
            <li>
              <span class='input-po subtotal subtotal-#{line.id}'>P#{number_with_delimiter(number_with_precision(line.subtotal, precision: 2), delimiter: ',')}</span>
            </li>
            <li>subtotal</li>
            </ul>"
    (front_html + middle_html + last_html)
  end

  def purchase_order_lines
    @purchase_order_lines ||= fetch_purchase_order_lines
  end

  def fetch_purchase_order_lines
    purchase_order_lines = @my_purchase_order_lines.order("#{sort_column} #{sort_direction}").page(page).per_page(per_page)
    purchase_order_lines = purchase_order_lines.where("(products.name like :search) 
      OR (products.sku like :search)
      OR (products.barcode like :search)", search: "%#{params[:sSearch]}%") if params[:sSearch].present?
    purchase_order_lines.uniq
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[purchase_order_lines.id]
    columns[params[:iSortCol_1].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "asc" : "desc"
  end
end
class ProductCategory < ActiveRecord::Base
  attr_accessible :code, :name
  has_many :products
  belongs_to :account

  has_many  :ownerships
  has_many  :users, through: :ownerships

  has_many  :purchase_orders


end

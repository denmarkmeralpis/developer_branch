class Distributor < Supplier
  belongs_to :supplier, class_name: Supplier, foreign_key: :principal_id
end
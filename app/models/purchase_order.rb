class PurchaseOrder < ActiveRecord::Base
  attr_accessible :account_id, :branch_id, :buyer_id, :supplier_id,
                  :computation_end_date, :computation_options, :computation_start_date,
                  :date, :manager_id, :status, :overall_discount, :total_po_cost,
                  :mother_ref_no, :reference, :order_status, :due_date, :cancellation_date, :created_by_id,
                  :total_units, :total_quantity, :gross_amount, :total_item_discount,
                  :grand_total, :product_category_id, :cancellation_date,
                  :action_state, :reviewed_by_id, :voided_by_id, :void_reason, :approved_by_id, :assigned_to_user_id,
                  :current_viewer_id, :edit_expiration, :supplier_support_discount, :supplier_terms

  # validates :status, inclusion: { in: %w(Approved Voided Reviewed New) }
  validates_uniqueness_of :reference

  scope :unapprove_po, where(:status => 'S')
  scope :approved_po, where(:status => 'A')
  scope :voided_po, where(:status => 'V')
  scope :for_today, lambda{|s| where('(updated_at between ? and ? or status = ?) and supplier_id IN (?)', Date.today.beginning_of_day, Date.tomorrow.end_of_day, 'S', s)}

  belongs_to :account
  belongs_to :branch
  belongs_to :supplier
  belongs_to :product_category
  belongs_to :user  

  has_many :purchase_order_lines, :foreign_key => :purchase_order_id, :primary_key => :reference, dependent: :destroy
  has_one  :po_number

  belongs_to :created_by,  :class_name => User, :foreign_key => :created_by_id
  belongs_to :reviewed_by, :class_name => User, :foreign_key => :reviewed_by_id
  belongs_to :approved_by, :class_name => User, :foreign_key => :approved_by_id
  belongs_to :voided_by,   :class_name => User, :foreign_key => :voided_by_id

  accepts_nested_attributes_for :purchase_order_lines
  attr_accessible :purchase_order_lines_attributes

  def self.branch_pending_purchase_orders(buyer_ids, branch_id)
    purchase_orders = buyer_ids.present? ? where(assigned_to_user_id: buyer_ids, branch_id: branch_id) : where(branch_id: branch_id)
    purchase_orders
  end

  def self.matching_purchase_order(status, category_id, supplier_id)     
    purchase_order = where("purchase_orders.status = ?", status)
    purchase_order = purchase_order.where("purchase_orders.product_category_id = ?", category_id)
    purchase_order = purchase_order.where("purchase_orders.supplier_id = ?", supplier_id)
    purchase_order
    # where("purchase_orders.product_category_id = ? AND purchase_orders.status = ? AND purchase_orders.supplier_id = ?", category_id, status, supplier_id)      
    # where(product_category_id: category_id, status: status, supplier_id: supplier_id)
  end

  def self.matching_purchase_order_via_category
    where("purchase_orders.product_category_id = ? AND purchase_order_lines.product_id = ?")
  end
end

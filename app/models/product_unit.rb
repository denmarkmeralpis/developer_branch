class ProductUnit < Product
  belongs_to :product, foreign_key: :parent_product_id
  belongs_to :account
end
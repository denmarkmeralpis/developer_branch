class BuyerSupplier < ActiveRecord::Base
  attr_accessible :buyer_id, :supplier_id

  belongs_to :user
  belongs_to :supplier


  validates :user_id, :uniqueness => {:scope => :supplier_id}


end

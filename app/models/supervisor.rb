class Supervisor < User
  has_many  :buyers, :class_name => User, :foreign_key => :supervisor_id
  has_many  :purchase_orders , through: :buyers
end
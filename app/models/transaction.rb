class Transaction < ActiveRecord::Base
  attr_accessible :branch_id, :first_inventory, :quantity, :last_inventory, :account_id, :date, :transaction_code, :type_code

  # For optimization only

  belongs_to :account
  belongs_to :purchase_order_line
  belongs_to :product
  belongs_to :branch


  def self.statistical_data(code, from, to)
    where(type_code: code, date: from...to).group(:date)
  end

end

class Company < ActiveRecord::Base
  attr_accessible :account_id, :code, :name, :status, :telephone, :address, :remark, :reference
  belongs_to :account
  has_many   :branches
end

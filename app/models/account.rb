require 'csv'
require 'pathname'

include GeneratePurchaseOrder::Base
include FastImport::Base
include TestImport::Base
include IndexData::Base

class Account < ActiveRecord::Base

  validates_uniqueness_of :subdomain  

  attr_accessible :name, :subdomain, :last_data_index, :index_status
  after_create    :assign_directory
  after_create    :create_API_key

  attr_accessible :users_attributes

  has_many :product_categories
  has_many :active_branches, class_name: Branch, conditions: { status: 'A' }
  has_many :branches
  has_many :branch_supplier_configurations
  has_many :companies
  has_many :users, dependent: :destroy
  has_many :products
  has_many :product_units, class_name:  Product
  has_many :suppliers
  has_many :sales
  has_many :purchase_orders
  has_many :purchase_order_lines
  has_many :loading_logs
  has_many :branch_products
  has_many :transactions
  has_many :supplier_products
  has_one  :account_configuration
  has_many :ownerships
  has_one :api_key


  accepts_nested_attributes_for :users

  def account_dir
    #Rails.root.join('file-server').join("#{id}")
    Rails.root.join('test').join('test_data')
  end

  def archives_dir
    account_dir.join('archives')
  end

  def uploads_dir
    account_dir.join('uploads')
  end

  def errors_dir
    account_dir.join('errors')
  end

  # create directory after user signed up
  # for test purposes only

  def assign_directory
    directoryUpload = File.join(Rails.root, 'file-server', "#{self.id}", 'uploads')
    directoryArchive = File.join(Rails.root, 'file-server', "#{self.id}", 'archive')
    FileUtils.mkdir_p(directoryUpload)
    FileUtils.mkdir_p(directoryArchive)
  end

  def create_API_key
    APIKey.create(account_id: self.id)
  end

end

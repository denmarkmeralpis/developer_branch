class BranchProduct < ActiveRecord::Base
  attr_accessible :account_id, :branch_id, :product_id, :supplier_id, :status
  attr_accessible :incoming_quantity, :last_arrival_quantity, :last_arrival_date

  belongs_to :account

  belongs_to :branch
  belongs_to :product

  belongs_to :supplier
  belongs_to :user

  def self.active_branch_products(branch_id)
    joins(:product).where(branch_products: {branch_id: branch_id}, products: {status: 'A'})
  end

  def self.order_from_suppliers_lists(supplier_id = 0, product_id)    
    supplier_list = where("supplier_id <> ? AND product_id = ?", supplier_id, product_id).select("DISTINCT supplier_id")
    supplier_list
  end

end

class Supplier < ActiveRecord::Base
  attr_accessible :account_id, :address, :code, :contact_email, :contact_fax,
                  :contact_mobile, :contact_person, :contact_telephone, :name,
                  :historical_data, :day, :frequency, :advance_frequency, :status, :discount_text,
                  :is_principal, :reference, :vat_inclusive

  attr_accessible :terms, :support_discount

  validates_presence_of :name, :code
  validates_uniqueness_of :code, :scope => [:account_id, :status], :unless => Proc.new { |p| p.status == "D" }
  validates_inclusion_of :status, :in => %w( A I D )

  belongs_to :account

  has_many :buyer_suppliers
  has_many :users, through: :buyer_suppliers
  has_many :ownerships
  has_many :products, through: :supplier_products
  has_many :supplier_products
  has_many :product_categories, through: :products
  has_many :distributors
  has_many :purchase_orders
  has_many :branch_supplier_configurations

  
  has_many :branches, through: :branch_supplier_configurations  
  has_many :branch_products#, through: :branches

  def self.notdeleted
    where('status <> ?', 'D').uniq
  end

  def self.active_suppliers
    where(status: 'A', is_principal: false)
  end

  def self.search_keyword(keyword, tempArg)
    if keyword
      find(:all, conditions: [ 'suppliers.id NOT IN (?) AND suppliers.name LIKE ?', tempArg, "%#{keyword}%" ])
    else
      find(:all)
    end
  end

end

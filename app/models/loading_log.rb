class LoadingLog < ActiveRecord::Base
  attr_accessible :account_id, :loading_type, :filename, :message, :status, :last_read_line, :last_known_row

  belongs_to :account
end

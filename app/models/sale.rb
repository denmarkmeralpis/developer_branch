class Sale < ActiveRecord::Base
  attr_accessible :account_id, :batch_no, :date, :branch_id, :product_id, :quantity_sold, :quantity_remained,
                  :branch_code, :product_sku

  attr_accessor   :branch_code, :product_sku

  belongs_to :account
  belongs_to :branch
  belongs_to :product
end

class BranchSupplierConfiguration < ActiveRecord::Base

  attr_accessible :branch_id, :supplier_id, :frequency_type, :frequency_week, :frequency_day, :account_id

  belongs_to :account
  belongs_to :branch
  belongs_to :supplier

  validates :branch_id, uniqueness: {scope: :supplier_id}

  # Check supplier schedule
  def self.get_schedule
    BranchSupplierConfiguration.joins(:supplier).where("suppliers.status = 'A' AND branch_supplier_configurations.next_po_date <= ?", Date.today).includes(:branch).includes(:supplier)
  end

  # Check days to stock
  def days_to_stock
    case self.frequency_type
    when 'monthly'
      30
    when 'weekly'
      7
    else
      14
    end
  end  

  def calculate_next_po_date
    check_date = self.next_po_date.nil? ? nil : self.next_po_date.to_date
    # if previous po date is today, add plus one day to compute the next
    previous_date = Date.today == check_date ? Date.today + 1.day : Date.today
    week_of_previous = previous_date.week_of_month
    week_numbers = self.frequency_week.present? ? self.frequency_week.scan(/\d+/) : [-1]

    week_numbers.each do |week_number|
      day_array = previous_date.send("all_#{self.frequency_day}s_in_month")
      day_of_previous = previous_date.wday
      day_of_next = Date.parse(self.frequency_day).wday
      # if scheduled week is same as current week
      if week_number.to_i == -1 || week_number.to_i == week_of_previous
        # if greater than or equal to day of previous save the current week and day of next
        if day_of_next >= day_of_previous
          day = day_array[week_of_previous - 1]
          new_date = Date.new(previous_date.year, previous_date.month, day)
          self.next_po_date = new_date.week_of_month == week_of_previous ? new_date : new_date - 1.week
          break
        # if weekly, just add one week then save that week and day
        elsif week_number.to_i == -1
          day = day_array[week_of_previous]
          if day.nil?
            # if it's last week of the month use the next month's day array
            if day_array[day_array.count - 1] < previous_date.day
              new_date = previous_date + 1.month
              day_array = new_date.send("all_#{self.frequency_day}s_in_month")
              day = day_array[0]
            # if this day didn't start at first week
            elsif Date.new(previous_date.year, previous_date.month, day_array[0]).week_of_month > 1
              day = day_array[week_of_previous - 1]
              new_date = previous_date
            end
            self.next_po_date = Date.new(new_date.year, new_date.month, day)
          else
            self.next_po_date = Date.new(previous_date.year, previous_date.month, day)
          end
        # if day of next is less than day of previous
        else
          # if monthly
          if week_numbers.count == 1
            new_date = previous_date + 1.month
            day_array = new_date.send("all_#{self.frequency_day}s_in_month")
            day = day_array[week_number.to_i - 1]
            self.next_po_date = Date.new(new_date.year, new_date.month, day)
          # if bimonthly
          else
            if week_numbers.index(week_number) == 0
              day = day_array[week_numbers[1].to_i - 1]
              self.next_po_date = Date.new(previous_date.year, previous_date.month, day)
              break
            else
              new_date = previous_date + 1.month
              day_array = new_date.send("all_#{self.frequency_day}s_in_month")
              day = day_array[week_numbers[0].to_i - 1]
              self.next_po_date = Date.new(new_date.year, new_date.month, day)
            end
          end
        end
        # if scheduled week is greater than current week save the scheduled week and day from calendar
      elsif week_number.to_i > week_of_previous
        day = calendar[week_number.to_i - 1][day_of_next]
        self.next_po_date = Date.new(previous_date.year, previous_date.month, day)
        break
        # if scheduled week is less than current week
      elsif week_number.to_i < week_of_previous
        # if monthly add one month and use the week_number
        if self.frequency_type == 'monthly'
          new_date = previous_date + 1.month
          calendar = (new_date).week_split
          day = calendar[week_number.to_i][day_of_next]
          self.next_po_date = Date.new(new_date.year, new_date.month, day)
        # if it is still less than and it's the last scheduled week in bimonthly use next month's first scheduled bimonthly
        elsif week_numbers.index(week_number) == 1
          new_date = previous_date + 1.month
          day_array = new_date.send("all_#{self.frequency_day}s_in_month")
          day = day_array[week_numbers[0].to_i - 1]
          self.next_po_date = Date.new(new_date.year, new_date.month, day)
        end
        # if bimonthly iterate
      end
    end
    save
  end

end

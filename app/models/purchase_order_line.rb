class PurchaseOrderLine < ActiveRecord::Base
  attr_accessible :account_id, :supplier_code, :product_id, :stock_keeping_days, :average, :average_daily_sales, :onhand_quantity, :suggested_quantity, :discount_text, :id,
                  :quantity_to_purchase, :quantity_per_unit, :units_to_purchase, :purchase_price, :subtotal, :subtotal_after_overall_discount, :adjusted_average_daily_sales,
                  :purchase_order_id, :adjusted_average_daily_sales, :ordering_unit

  scope :active_products, where(:status => 'A')
  scope :suspended_products, where(:status => 'I')

  attr_accessor :supplier_code

  belongs_to :account
  belongs_to :product
  belongs_to :purchase_order, primary_key: "reference"

end

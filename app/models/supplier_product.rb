class SupplierProduct < ActiveRecord::Base
  attr_accessible :account_id, :supplier_id, :product_id, :unit_id, :cost, :retail_price, :status, :last_discount_text

  belongs_to :account

  belongs_to  :supplier
  belongs_to  :product

  has_many :branch_products
  has_many :po_products, through: :branch_products

end

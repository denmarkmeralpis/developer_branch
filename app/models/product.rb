class Product < ActiveRecord::Base
  attr_accessible :sku, :stock_no, :barcode, :name, :cost, :retail_price, :supplier_code, :base_unit_name, :unit_content_quantity,
                  :discount_text, :ordering_unit, :status, :allow_decimal_quantities, :parent_product_id, :account_id, :product_category_id

  validates_inclusion_of :status, :in => %w( A I D )

  scope :active_products, where(:status => 'A')
  scope :suspended_products, where(:status => 'I')

  attr_accessor :supplier_code

  belongs_to :account
  belongs_to :branch_product

  has_many :suppliers, through: :supplier_products
  has_many :supplier_products
  has_many :branch_products
  has_many :branches, through: :branch_products
  has_many :sales
  has_many :purchase_order_lines
  has_many :transactions
  has_many :product_units, class_name: ProductUnit, foreign_key: :parent_product_id

  belongs_to :product_category


end

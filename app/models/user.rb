
class User < ActiveRecord::Base
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :user_access, :parent_id,
                  :last_name, :role, :status, :supervisor_id, :account_id, :password_hash, :password_salt, :reset_password_token

  attr_accessor :password

  before_save   :encrypt_password

  validates_confirmation_of :password  
  validates_presence_of     :email, :first_name, :password
  validates_uniqueness_of   :email
  validates_length_of       :password, minimum: 8


  has_one    :po_number
  has_many   :buyer_suppliers
  has_many   :roles
  has_many   :suppliers,  through: :buyer_suppliers, dependent: :destroy
  has_many   :products, through:  :suppliers
  has_many   :buyers,     class_name: User, foreign_key: :supervisor_id
  has_many   :sub_users,  class_name: User, foreign_key: :parent_id
  has_many   :branches,   through: :suppliers
  belongs_to :supervisor, class_name: User, foreign_key: :supervisor_id
  belongs_to :admin,      class_name: User, foreign_key: :id
  belongs_to :account


  has_many  :ownerships
  has_many  :product_categories, through: :ownerships  
  has_many  :purchase_orders, class_name: PurchaseOrder, foreign_key: :assigned_to_user_id

  belongs_to :user

  accepts_nested_attributes_for :roles

  attr_accessible :roles_attributes

  def self.active_supervisor_buyers(supervisor_id)
    where(supervisor_id: supervisor_id, status: 'A').map(&:id)
  end

  def self.search_keyword(keyword, tempArg)
    if keyword
      find(:all, limit: 10, conditions: [ 'suppliers.id NOT IN (?) AND suppliers.name LIKE ?', tempArg, "%#{keyword}%" ])
    else
      find(:all, limit: 10)
    end
  end

  def self.nodeleted(user)
    if user.role == 'supervisor'
      where('status <> ? and role <> ? and role <> ? and id <> ? and supervisor_id = ? and role <> ?' , 'D', 'cb_admin', 'owner', user.id, user.id, 'merchant admin')
    elsif  user.role == 'merchant admin'
      where('status <> ? and role <> ? and role <> ? and id <> ?' , 'D', 'cb_admin', 'owner', user.id)
    else
      where('status <> ? and role <> ? and id <> ?', 'D', 'cb_admin', user.id)
    end
  end

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def self.cb_admins
    user = where(role: 'cb_admin')
  end

  def self.locate_subdomain(email)
    user = find_by_email(email, conditions: ['role <> ?', 'cb_admin'] ) rescue nil
    if user.nil?
      nil
    else
      user.account.subdomain
    end
  end

  def self.is_cb_admin?
    if self.role.to_s == 'cb_admin'
      true
    else
      false
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

end

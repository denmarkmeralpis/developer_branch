class Branch < ActiveRecord::Base
  attr_accessible :account_id, :code, :name, :company_id, :status, :reference

  validates_presence_of :name

  belongs_to :account
  belongs_to :company
  belongs_to :user

  has_many :branch_products
  has_many :products, through: :branch_products  

  has_many :sales
  has_many :purchase_orders
  has_many :branch_supplier_configurations
  has_many :suppliers, through: :branch_supplier_configurations

  has_many :br_suppliers, foreign_key: :branch_id, class_name: BranchProduct

  # def filtered_suppliers(id = 0, product_id)
  #   # branch = self    
  #   # branch_products  = branch.products.map(&:id).uniq
  #   # branch_suppliers = branch.suppliers.joins(:supplier_products)
  #   # branch_suppliers = branch_suppliers.where("suppliers.id IN (?)", suppliers)
  #   # branch_suppliers = branch_suppliers.where("supplier_products.product_id IN (?)", branch_products)
  #   # branch_suppliers = branch_suppliers.where("suppliers.id <> ?", id)
  #   # branch_suppliers = branch_suppliers.uniq


  #   # new algorithm
  #   branch = self
  #   branch_products = branch.branch_products.where("supplier_id <> ? AND product_id = ?", id, product_id).select("DISTINCT supplier_id")
  # end

end

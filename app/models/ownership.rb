class Ownership < ActiveRecord::Base
  attr_accessible :account_id, :user_id, :product_category_id, :supplier_id

  belongs_to :account
  belongs_to :user
  belongs_to :product_category
  belongs_to :supplier

  validates_uniqueness_of :account_id, :scope => [:user_id, :product_category_id, :supplier_id]

end

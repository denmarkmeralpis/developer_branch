class APIKey < ActiveRecord::Base
  attr_accessible :access_token, :account_id

  validates_uniqueness_of :account_id, :access_token

  belongs_to :account

  before_create :generate_access_token

  private

  def generate_access_token
  	begin
      self.access_token = SecureRandom.hex
    end while self.class.exists?(access_token: access_token)
  end
end
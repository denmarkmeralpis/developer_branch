module API
  module V2
    class PurchaseOrdersController < ApplicationController
      before_filter :authenticate
      respond_to :xml

      def authenticate
        authenticate_token || render_unauthorized
      end

      def authenticate_token
        authenticate_or_request_with_http_basic do |username, password|
          @account_id = APIKey.find_by_access_token(username).account_id rescue nil
          @account_id.nil? ? false : true
        end
      end

      def render_unauthorized
        render xml: 'Bad credentials', status: 401
      end

      def index
        begin
          purchase_orders = paginate PurchaseOrder.where(account_id: @account_id).where("status='V' OR status='A'"), per_page: 10

          purchase_orders = paginate purchase_orders.where('updated_at < ? ', params[:before].to_date), per_page: 10 if params[:before].present?

          purchase_orders = paginate purchase_orders.where('updated_at > ? ', params[:after].to_date.end_of_day), per_page: 10 if params[:after].present?

          purchase_orders = paginate purchase_orders.where('updated_at >= ? ', params[:from].to_date), per_page: 10 if params[:from].present?

          purchase_orders = paginate purchase_orders.where('updated_at <= ? ', params[:to].to_date.end_of_day), per_page: 10 if params[:to].present?

          po_array = []
          purchase_orders.each do |purchase_order|

            po_lines = purchase_order.purchase_order_lines#(:order => "id desc")
            po_lines_array = []
            po_lines.each do |po_line|
              #product_units = po_line.product.product_units
              #ordering_unit = product_units.where(ordering_unit: '1').first if product_units.present?
              #unit = ordering_unit.nil? ? po_line.product : ordering_unit
              
              # if po_line.quantity_per_unit == po_line.product.unit_content_quantity
              #   unit = po_line.product
              # else
              #   po_line.product.product_units.each do |x|
              #     if x.quantity_per_unit == po_line.quantity_per_unit 
              #       unit = x
              #     end
              #   end
              # end

              if po_line.product.base_unit_name == po_line.ordering_unit
                unit = po_line.product
              else
                po_line.product.product_units.each do |x|
                   if x.base_unit_name == po_line.ordering_unit
                    unit = x
                  end
                end
              end 

              po_lines_array << {
                  barcode: po_line.product.barcode,
                  quantity_to_purchase: po_line.quantity_to_purchase,
                  quantity_per_unit: po_line.quantity_per_unit,
                  units_to_purchase: po_line.units_to_purchase,
                  unit_name: unit.base_unit_name,
                  unit_code: unit.sku,
                  #effective_cost: po_line.subtotal / po_line.quantity_to_purchase.to_i,
                  purchase_price: po_line.purchase_price,
                  product_code: po_line.product.sku,
                  item_discount_text: po_line.discount_text,
                  subtotal: po_line.subtotal
              }
            end

            po_array << {
                purchase_order_number: purchase_order.reference,
                branch_code: Branch.find(purchase_order.branch_id).code,
                supplier_code: Supplier.find(purchase_order.supplier_id).code,
                cancellation_date: purchase_order.cancellation_date,
                target_date: purchase_order.created_at + 7.days,
                purchase_order_date: purchase_order.created_at,
                last_updated_at: purchase_order.updated_at,
                remarks: purchase_order.remarks,
                status: purchase_order.status,
                overall_discount_text: purchase_order.overall_discount,
                gross_amount: purchase_order.total_po_cost,
                total_item_discount: purchase_order.total_item_discount,
                net_amount: purchase_order.grand_total,
                purchase_order_lines: po_lines_array
            }
          end

          if purchase_orders.present?
            render xml: po_array.to_xml(root: 'purchase_orders', dasherize: false), status: 200
          else
            head 204
          end
        rescue Exception
          head 500
        end
      end
    end
  end
end
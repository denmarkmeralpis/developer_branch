module API
  module V1
    class PurchaseOrdersController < ApplicationController
      before_filter :authenticate
      respond_to :xml

      def authenticate
        authenticate_token || render_unauthorized
      end

      def authenticate_token
        authenticate_with_http_token do |token, options|
          @account_id = APIKey.find_by_access_token(token).account_id rescue nil
          @account_id.nil? ? false : true
        end
      end

      def render_unauthorized
        render xml: 'Bad credentials', status: 401
      end

      def index
        begin
          purchase_orders = paginate PurchaseOrder.where(account_id: @account_id, barter_receive_date: nil), per_page: 50

          purchase_orders = paginate purchase_orders.where(supplier_id: Supplier.find_by_code(params[:supplierCode].to_s).id), per_page: 50 if params[:supplierCode].present? 

          purchase_orders = paginate purchase_orders.where(branch_id: Branch.find_by_code(params[:branchCode].to_s).id), per_page: 50 if params[:branchCode].present? 

          po_array = []
          purchase_orders.each do |purchase_order|
            po_array << {
              poNumber: purchase_order.reference,
              branchCode: Branch.find(purchase_order.branch_id).code,
              buyerName: User.find(purchase_order.assigned_to_user_id).last_name,
              date: purchase_order.created_at.to_s,
              supplierCode: Supplier.find(purchase_order.supplier_id).code
            }
          end

          purchase_orders_hash = {poListSummary: {total: purchase_orders.count}, po: po_array }

          if purchase_orders.present?
            render xml: purchase_orders_hash.to_xml(:root => 'poRequest'), status: 200
          else
            head 204
          end
        rescue Exception
          head 500
        end
      end

      def show
        begin
          purchase_order = PurchaseOrder.where(account_id: @account_id, reference: params[:id]).first
          purchase_order_lines = paginate PurchaseOrderLine.where(purchase_order_id: purchase_order.reference), per_page: 50
          line_items_array = []

          po_summary_hash = {
            poNumber: purchase_order.reference,
            deliverTo: Branch.find(purchase_order.branch_id).name,
            branchCode: Branch.find(purchase_order.branch_id).code,
            supplierCode: Supplier.find(purchase_order.supplier_id).code,
            poDate: purchase_order.created_at,
            cancellationDate: purchase_order.cancellation_date,
            targetDate: purchase_order.due_date,
            remark: purchase_order.remarks,
            poLinesCount: purchase_order_lines.count,
            grossAmount: purchase_order.total_po_cost,
            totalItemDiscount: purchase_order.total_item_discount,
            overallDiscountText: purchase_order.overall_discount,
            netAmount: purchase_order.grand_total
          }

          purchase_order_lines.each_with_index do |purchase_order_line, idx|
            line_items_array << {
              lineNumber: (idx + 1),
              sku: Product.find(purchase_order_line.product_id).sku,
              purchaseQuantity: purchase_order_line.quantity_to_purchase,
              quantityPerUnit: purchase_order_line.quantity_per_unit,
              unitToPurchase: purchase_order_line.units_to_purchase,
              purchasePrice: purchase_order_line.purchase_price,
              itemDiscountText: purchase_order_line.discount_text,
              subTotal: purchase_order_line.subtotal
            }
          end

          purchase_order_detailed_hash = {poSummary: po_summary_hash, lineItems: {lineItem: line_items_array} }

          if params[:id].nil?
            head 422
          elsif purchase_order.present?
            render xml: purchase_order_detailed_hash.to_xml(:root => 'po'), status: 200
          else
            head 500
          end
        rescue Exception
          head 500
        end
      end

      def received
        begin
          xml_string = params[:data]
          data_hash = Hash.from_xml(xml_string)
          purchase_order_array = []
          data_hash['poList']['po'].each do |po|
            purchase_order = PurchaseOrder.find_by_reference(po['poNumber'].to_i)
            purchase_order.barter_receive_date = DateTime.now
            purchase_order_array << purchase_order
          end

          processing_po = purchase_order_array.paginate(page: 1, per_page: 50000)
          processing_po.total_entries.times do |i|
            PurchaseOrder.import(purchase_order_array.paginate(page: (i + 1), per_page: 50000), { validate: false, on_duplicate_key_update: [:barter_receive_date] })
          end

          render xml: 'Success', status: 200 
        rescue Exception
          head 500
        end
      end
    end
  end
end
class BranchesController < ApplicationController

  add_breadcrumb "Branches", :dashboards_path

  require 'will_paginate/array'

  before_filter :check_session
  before_filter :check_branch_if_belongs_to_account, only: [:show, :assigner, :search_assigned, :search_unassigned, :unassigner, :getSched]  
  layout false, only: [:get_branches_of_supplier, :search_unassigned, :assigner, :search_assigned, :get_products_duplicate_assign]

  def show
    @name = @branch.name
    
    if @user.role == 'owner'
      @suppliers = @branch.suppliers
    else
      @suppliers = @user.suppliers.where(id: @branch.suppliers.active_suppliers.map(&:id))
    end    

    add_breadcrumb "#{@name}", branch_path(params[:id])

    if @user.role == 'supervisor'
      buyers = @account.users.active_supervisor_buyers(@user.id)
      purchase_orders = @account.purchase_orders.branch_pending_purchase_orders(buyers, @branch.id)
    elsif @user.role == 'owner'
      purchase_orders = @account.purchase_orders.branch_pending_purchase_orders([], @branch.id)
    else
      purchase_orders = @user.purchase_orders.branch_pending_purchase_orders([], @branch.id)
    end

    @purchase_orders = purchase_orders.joins(:supplier).joins(:product_category).joins(:branch).uniq
    
    respond_to do |format|
      format.html
      format.json { render json: PurchaseOrdersDatatable.new(view_context, @purchase_orders, merchant_current_user) }
    end
  end

  def search_assigned    
    @distributors = @branch.suppliers
    respond_to do |format|
      format.html
      format.json { render json: AssignedSuppliersDatatable.new(view_context, @distributors, "branch", @user) }
    end
  end

  def search_unassigned
    respond_to do |format|
      format.html
      format.json { render json: PendingSuppliersDatatable.new(view_context, :branch, @branch) }
    end
  end

  # >>>>>>>>>>>>>>>>>>>>>>>>>>>> DEFAULT SUPPLIER ASSIGNMENT START
  
  def get_products_duplicate_assign    
    branch    = @account.branches.find(params[:id])
    bp        = branch.products
    supplier  = @account.suppliers.find(params[:supplier_id])
    sp        = supplier.products.where("products.id IN (?) AND products.status = ?", bp.map(&:id), 'A').uniq.map(&:id)
    prod      = bp.where("branch_products.product_id IN (?)", sp)
    ids       = prod.where("branch_products.supplier_id IS NULL", params[:supplier_id]).map(&:id)
    ok_assign = branch.branch_products.where("branch_products.product_id IN (?)", ids)

    ok_assign.each {|i|  i.update_attributes(supplier_id: params[:supplier_id]) }

    @products_x = prod.where("branch_products.supplier_id <> ?", params[:supplier_id])
    @products_x = @products_x.to_json
  end

  def assign_default_supplier
    bp = @account.branches.find(params[:id]).branch_products
    
    if params[:product_ids].nil?
      render text: 'failed'
    end

    product_ids_arr = params[:product_ids].split(',')
    bp = bp.where("branch_products.product_id in (?)", product_ids_arr)
    
    bp.each {|i|  i.update_attributes(supplier_id: params[:supplier_id]) }
 
    if bp
      render text: 'success'
    else
      render text: 'failed'
    end

  end

  # <<<<<<<<<<<<<<<<<<<<<<<<<<<<< DEFAULT SUPPLIER ASSIGNMENT END

  def get_branches_of_supplier
    if @user.role == 'owner'
      branch_ids = User.where(parent_id: @user.id).includes(:branches).map(&:branches).flatten.uniq.map(&:id)
    elsif @user.role == 'supervisor'
      branch_ids = User.where(supervisor_id: @user.id).includes(:branches).map(&:branches).flatten.uniq.map(&:id)
    else
      branch_ids = @user.branches.map(&:id)
    end
    branches = Branch.where(id: branch_ids)
    @branches = branches.joins(:branch_supplier_configurations).where("branch_supplier_configurations.supplier_id = ?", params[:id]).uniq
    render json: @branches
  end

  def assigner

    attributes = params[:assign_params].merge(account_id: @account.id)

    if params[:update] == 'N'
      assignToBranch = @branch.branch_supplier_configurations.new(attributes)
      if assignToBranch.save
        assignToBranch.calculate_next_po_date
        render text: 'success'
      else
        render text: 'failed'
      end
    else
      toUpdate = @branch.branch_supplier_configurations.find(params[:sched_id])
      updated = toUpdate.update_attributes(params[:assign_params])
      if updated
        toUpdate.reload.calculate_next_po_date
        render text: 'success'
      else
        render text: 'failed'
      end
    end

  end

  def unassigner
    bp = @account.branches.find(params[:id]).branch_products.where("supplier_id = ?" , params[:supplier_id])
    assignToBranch = @branch.branch_supplier_configurations.where(:supplier_id => params[:supplier_id]).first
    if assignToBranch.destroy
      # bp.each do |i|
      #   i.supplier_id = nil
      #   i.save
      # end
      render text: 'success'
    else
      render text: 'failed'
    end
  end


  def getSched
    @sched = @branch.branch_supplier_configurations.find_by_supplier_id(params[:supplier_id])
    if @sched
      render text: 'success'
    else
      render text: 'failed'
    end
  end

  private

  def check_branch_if_belongs_to_account
    @branch = @account.branches.find(params[:id]) rescue nil
    if @branch.nil?
      redirect_to dashboards_path, alert: 'Branch not found! Try again!'
      return
    end
  end

  def check_session
    if !merchant_current_user.nil?
      @account = merchant_current_user.account
      @user = merchant_current_user
      @copy_po = @user.roles.map(&:name).include?('cpo')
      if @account.subdomain.to_s != request.subdomain.to_s
        session[:merchant_current_user] = nil
        redirect_to new_session_path, alert: 'Session expired. Pleas- re login'
      end
    else
      redirect_to new_session_path, notice: 'Session expired! Pleas- re login!'
    end
  end

end
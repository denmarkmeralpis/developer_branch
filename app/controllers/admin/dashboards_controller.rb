class Admin::DashboardsController < ApplicationController

  layout 'admin'
  before_filter :check_session

  def index
  end

  private

  def check_session
    if current_user.nil? || current_user.role != 'cb_admin'
      redirect_to new_admin_session_path, alert: 'Session expired. Please re-login!'
    end
  end

end
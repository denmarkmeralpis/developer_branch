class Admin::AccountsController < ApplicationController

  layout 'admin'
  before_filter :check_session

  def index
    @merchants = Account.all
  end

  def new
    @merchant = Account.new
    @merchant.users.build
  end

  def create
    @merchant = Account.new(params[:account])
    if @merchant.save
      password = params[:account][:users_attributes]['0'][:password]
      save_owner_privileges
      MerchantMailer.welcome_mailer(@merchant.users.first, @merchant.subdomain, password).deliver
      redirect_to admin_accounts_path, notice: 'New Merchant has been successfully added!'
    else
      render 'new'
    end
  end

  def run_indexing
    merchant = Account.find(params[:id])    
    merchant.update_attributes(index_status: :in_progress)
    exec_rake :run_data_indexing, account_id: merchant.id
    redirect_to admin_accounts_path, notice: "Data indexing started successfully!"
  end

  def show
    @merchant = Account.find(params[:id])
  end

  def save_owner_privileges
    role_columns = [:user_id, :name]
    roles = []
    roles << [@merchant.users.first.id, 'rpo']
    roles << [@merchant.users.first.id, 'apo']
    roles << [@merchant.users.first.id, 'cpo']
    roles << [@merchant.users.first.id, 'epo']
    roles << [@merchant.users.first.id, 'ssu']
    roles << [@merchant.users.first.id, 'spr']

    Role.import(role_columns, roles)
  end

  private

  def check_session
    if current_user.nil? || current_user.role != 'cb_admin'
      redirect_to new_admin_session_path, alert: 'Session expired. Please re login!'
    end
  end

end
class Admin::UsersController < ApplicationController

  layout 'admin'
  before_filter :check_session

  def index
    @users = User.cb_admins
  end

  def new
    @user = User.new
  end

  def create
    userAttributes = params[:user].merge(role: 'cb_admin')
    @user = User.new(userAttributes)
    if @user.save
      redirect_to admin_users_path, notice: 'Successfully created!'
    else
      render 'new'
    end
  end

  private

  def check_session
    if current_user.nil? || current_user.role != 'cb_admin'
      redirect_to new_admin_session_path, alert: 'Session expired. Please re login!'
    end
  end

end
class Admin::SessionsController < ApplicationController

  layout 'admin'

  def new
    if current_user
      redirect_to admin_dashboards_path, notice: 'Welcome back CloudBuy Administrator!'
    end
  end

  def create
    email    = params[:email]
    password = params[:password]
    user = Admin.authenticate(email, password)
    if user && user.role == 'cb_admin'
      session[:user_id] = user.id
      redirect_to admin_dashboards_path, notice: 'Welcome Admin to your CloudBuy Dashboard.'
    else
      flash.now.alert = 'Invalid username/password. Try again!'
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to new_admin_session_path
  end

end
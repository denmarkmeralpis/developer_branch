class PurchaseOrdersController < ApplicationController

  add_breadcrumb "Dashboard", :dashboards_path

  before_filter :check_session
  layout false, only: [:update_line_items, :get_branches_of_supplier, :get_prod_to_add]

  def show
    @status = params[:id]

    add_breadcrumb "Purchase Orders (#{@status.gsub(/[PCVA]/, 'P' => 'Pending', 'C' => 'Confirmed', 'V' => 'Voided', 'A' => 'Approved')})"

    if @user.role == 'buyer'
      @purchase_orders = @user.purchase_orders.where("purchase_orders.status = ?", @status)
    elsif @user.role == 'supervisor'
      buyers = @account.users.where("supervisor_id = ? AND status = ?", @user.id, 'A').map(&:id)
      @purchase_orders = @account.purchase_orders.where(:assigned_to_user_id => buyers).where("purchase_orders.status = ?", @status)
    else
      @purchase_orders = @account.purchase_orders.where("purchase_orders.status = ?", @status)
    end

    @purchase_orders = @purchase_orders.joins(:supplier).joins(:product_category).joins(:branch)    

    respond_to do |format|
      format.html
      format.json { render json: PurchaseOrdersDatatable.new(view_context, @purchase_orders.joins(:supplier).joins(:product_category), merchant_current_user) }
    end
    
  end

  def del_po_line
    po = @account.purchase_orders.find(params[:id])
    po_line = po.purchase_order_lines.find(params[:po_line_id])
    
    # PO Totals update
    price = po_line.units_to_purchase * po_line.purchase_price
    po.total_po_cost  -= price
    po.total_units    -= po_line.units_to_purchase rescue 0
    po.total_quantity -= po_line.quantity_to_purchase rescue 0
    po.gross_amount   -= po_line.subtotal
    po.total_item_discount -= (price - po_line.subtotal)
    po.grand_total    -= po_line.subtotal

    if po.save
      po_line.destroy
      # render text: 'success'
      render json: { result: :success, updated_totals: po.reload }
    else
      render json: { result: :failed }
    end
  end

  def add_po_line
    po = @account.purchase_orders.find(params[:id])
    po_prod = po.purchase_order_lines.map(&:product_id)
  

    if po_prod.include?(params[:product_id].to_i) 
      render text: 'failed'
    else
      type = @account.branch_supplier_configurations.where(:supplier_id => po.supplier_id, :branch_id => po.branch_id).first.frequency_type
      case type
      when 'weekly'
        days = 7
      when 'twice_a_month'
        days = 14
      when 'monthly'
        days = 30
      end

      #product to add
      product = @account.products.find(params[:product_id])
      order_product = @account.products.where(:parent_product_id => params[:product_id], :ordering_unit => "1").first 
      qpu = order_product.unit_content_quantity rescue 1
      #product.find_by_ordering_unit(1).unit_content_quantity rescue 1

      if order_product.nil?
        order_product = product
      end
      last_po = product.purchase_order_lines.order('id DESC').first

      ### Get Last PO Date
      last_po_date = last_po.created_at rescue nil

      ### Get Transactions
      if !last_po_date.nil?
        transaction = product.transactions.where('type_code = ? AND created_at > ?', 'POS', last_po_date).order('id DESC')
      else
        transaction = product.transactions.where('type_code = ?', 'POS').order('id DESC')
      end

      average       = BigDecimal.new(transaction.average(:quantity).to_s)
      on_hand_qty   = transaction.last.last_inventory rescue 0

      # if onhand_qty is less than 1, onhand should not be subtracted to the suggested_qty
      on_hand_qty = 0 if on_hand_qty < 1
      last_average  = BigDecimal.new(last_po.average.to_s) rescue average

      new_average   = (last_average + average) / 2
      suggested_qty = (new_average * days) - on_hand_qty
      
      if suggested_qty <= 0
        suggested_qty = 0
      end

      qtp = (suggested_qty * product.unit_content_quantity).round
      utp = qtp / qpu 

      if utp <= 0
        utp = 1
      end

      pp  = BigDecimal.new(order_product.cost.to_s)
      st  = utp * pp

      old_st = st

      discount_txt = product.discount_text rescue '0'
      discount_txt = '0' if discount_txt.nil?

      # Discount Computation
      discount_txt.split(",").each do |discount|
        if discount.last == "%"
          st -= st * (BigDecimal.new(discount) / 100)
        elsif discount.last != "%"
          st -= BigDecimal.new(discount)
        end
      end

      po_lines_attr = {
              account_id:                   @account.id,
              product_id:                   params[:product_id],
              stock_keeping_days:           days,
              average:                      average,
              average_daily_sales:          average,
              adjusted_average_daily_sales: average,
              onhand_quantity:              on_hand_qty,
              suggested_quantity:           suggested_qty,
              quantity_to_purchase:         qtp,
              quantity_per_unit:            qpu,
              units_to_purchase:            utp,
              purchase_price:               order_product.cost,
              subtotal:                     st,
              discount_text:                discount_txt,
              ordering_unit:                order_product.base_unit_name
      }
      add_po_line = po.purchase_order_lines.new(po_lines_attr)

      if add_po_line.save
        po.total_po_cost += utp * pp
        po.total_units += add_po_line.units_to_purchase rescue 0
        po.total_quantity += add_po_line.quantity_to_purchase rescue 0
        po.gross_amount += add_po_line.subtotal
        po.total_item_discount += ((utp*pp)- add_po_line.subtotal)
        po.grand_total += add_po_line.subtotal
        po.save
        @po_id = add_po_line.id

        render json: {
          result: :success,
          po_id: @po_id,
          updated_totals: po.reload
        }

        # render text: @po_id
      else
          render text: 'failed'
      end
    end
  end


  def copy_po
    #get current PO and PO lines
    curr_po = @account.purchase_orders.find(params[:purchase_order_id])
    curr_po_lines = curr_po.purchase_order_lines

    #get products of branches
    branch_product = @account.branch_products.where(:branch_id => params[:branch_id]).map(&:product_id) 

  # copy PO
    copied_po_attr = {
        branch_id:           params[:branch_id],
        supplier_id:         params[:supplier_id],
        created_by_id:       @user.id,
        assigned_to_user_id: @user.id,
        status:              'P',
        mother_ref_no:       curr_po.reference,
        reference:           DateTime.now.strftime('%Q').to_s,
        overall_discount:    curr_po.overall_discount,
        total_po_cost:       curr_po.total_po_cost,
        total_units:         curr_po.total_units,
        total_quantity:      curr_po.total_quantity,
        gross_amount:        curr_po.gross_amount,
        total_item_discount: curr_po.total_item_discount,
        grand_total:         curr_po.grand_total,
        product_category_id: curr_po.product_category_id,
        cancellation_date:   DateTime.now + 15.days
    }

    copied_po = @account.purchase_orders.create(copied_po_attr)

    #copy PO lines
    curr_po_lines.each do |po_line|
      if branch_product.include?(po_line.product_id)
        po_lines_attr = {
            account_id:                   @account.id,
            product_id:                   po_line.product_id,
            stock_keeping_days:           po_line.stock_keeping_days,
            average:                      po_line.average,
            average_daily_sales:          po_line.average_daily_sales,
            adjusted_average_daily_sales: po_line.adjusted_average_daily_sales,
            onhand_quantity:              po_line.onhand_quantity,
            suggested_quantity:           po_line.suggested_quantity,
            quantity_to_purchase:         po_line.quantity_to_purchase,
            quantity_per_unit:            po_line.quantity_per_unit,
            units_to_purchase:            po_line.units_to_purchase,
            purchase_price:               po_line.purchase_price,
            subtotal:                     po_line.subtotal,
            discount_text:                po_line.discount_text,
            ordering_unit:                po_line.ordering_unit
        }
        copied_po_line = copied_po.purchase_order_lines.create(po_lines_attr)
      else
        new_tpc = BigDecimal.new(curr_po.total_po_cost - po_line.subtotal)
        new_tu =  BigDecimal.new(curr_po.total_units - po_line.units_to_purchase)
        new_tq =  BigDecimal.new(curr_po.total_quantity - po_line.quantity_to_purchase)
        new_ga =  BigDecimal.new(curr_po.gross_amount - (po_line.units_to_purchase * po_line.purchase_price))
        new_tid = BigDecimal.new(curr_po.total_item_discount - ((po_line.units_to_purchase * po_line.purchase_price) - po_line.subtotal))
        new_gt =  BigDecimal.new(copied_po.grand_total - po_line.subtotal)
        
        po_updated_attr = {
          total_po_cost:       new_tpc,
          total_units:         new_tu,
          total_quantity:      new_tq,
          gross_amount:        new_ga,
          total_item_discount: new_tid,
          grand_total:         new_gt
        }
        copied_po.update_attributes(po_updated_attr)
      end
    end

 
    if copied_po.purchase_order_lines.empty?
      copied_po.destroy
      render text: 'failed'
    else
      render text: 'success'
    end
   
  
    
  end

  def get_prod_to_add
    if @user.role == 'buyer'
      @purchase_order       = @user.purchase_orders.includes(:purchase_order_lines).find(params[:id])
    elsif @user.role == 'supervisor'
      buyers = @account.users.where("supervisor_id = ? AND status = ?", @user.id, 'A').map(&:id)
      @purchase_order      = @account.purchase_orders.where(:assigned_to_user_id => buyers).includes(:purchase_order_lines).find(params[:id])
    else
      @purchase_order       = @account.purchase_orders.includes(:purchase_order_lines).find(params[:id])
    end

    po        = @purchase_order
    supplier  = po.supplier
    branch    = po.branch_id
    po_prod   = po.purchase_order_lines.map(&:product_id)
    products  = supplier.branch_products.where(:branch_id => branch).map(&:product_id)
    @products = @account.products.where("products.id IN (?) AND product_category_id = ? AND products.id NOT IN (?)", products, po.product_category_id, po_prod).to_json
  end


  def purchase_order_lines
    if @user.role == 'buyer'      
      @purchase_order = @user.purchase_orders
                          .includes(:purchase_order_lines)
                          .find(params[:id])
    elsif @user.role == 'supervisor'
      buyers = @account.users
                  .where("supervisor_id = ? AND status = ?", @user.id, 'A')
                  .map(&:id)
      @purchase_order = @account.purchase_orders
                  .where(:assigned_to_user_id => buyers)
                  .includes(:purchase_order_lines)
                  .find(params[:id])
    else
      @purchase_order = @account.purchase_orders.find(params[:id])
    end

    po         = @purchase_order    
    supplier   = po.supplier
    branch     = po.branch_id        

    add_breadcrumb "#{po.branch.name}", branch_path(branch)
    add_breadcrumb "Purchase Order No. #{po.reference}"

    @edit_price = @user.roles.map(&:name).include?('epo')
    @purchase_order_lines = @purchase_order.purchase_order_lines.includes(:product)

    @total_item_discount  = @purchase_order.total_item_discount # TID VIEW
    @overall_discount     = @purchase_order.overall_discount    # OD VIEW
    @total_po_cost        = @purchase_order.total_po_cost       # GTC VIEW
    @grand_total          = @purchase_order.grand_total         # NA VIEW
    @gross_amount         = @purchase_order.gross_amount        # GT VIEW    
    @total_units          = @purchase_order_lines.sum(:units_to_purchase)
    @total_quantity       = @purchase_order_lines.sum(:quantity_to_purchase)    
    @po_support_ds        = @purchase_order.supplier_support_discount.nil? ? @purchase_order.supplier.support_discount.to_f : @purchase_order.supplier_support_discount
    @po_terms             = @purchase_order.supplier_terms.nil? ? @purchase_order.supplier.terms : @purchase_order.supplier_terms

    if @purchase_order.status == 'P' || @purchase_order.status == 'C'
      respond_to do |format|
        format.html
        format.json { render json: PurchaseOrderLinesDatatable.new(view_context, @purchase_order_lines, @edit_price, merchant_current_user) }
      end
    else
      respond_to do |format|
        format.html
        format.json { render json: PurchaseOrderLinesDatatable2.new(view_context, @purchase_order_lines, @edit_price, merchant_current_user) }
      end
    end    
  end

  def update_line_items

    edit_attr = { edit_expiration: Time.zone.now + 5.minutes, current_viewer_id: @user.id }
    po = @account.purchase_orders.find(params[:id])
    if po
      if (po.edit_expiration.nil? || po.current_viewer_id.nil?) || (po.edit_expiration <= Time.zone.now) || ((po.edit_expiration >= Time.zone.now) && (po.current_viewer_id == @user.id))
        updated_po = po.update_attributes(params[:purchase_order].merge(edit_attr))
        if updated_po
          po = po.reload
          render json: {
            result: :ok,
            po: po,
            po_lines: po.purchase_order_lines.find(params[:po_line_id])
          }
        else
          render json: {
            result: :failed,
            message: 'unable to save! try again!'
          }
        end
      else
        render json: {
          result: :failed,
          message: 'There is someone currently editing the same po. try again in a while.'
        }
      end
    else
      render json: {
        result: :failed,
        message: 'unable to save! try again!'
      }
    end
  end


  def add_remarks
    purchase_order = @account.purchase_orders.find(params[:id])
    if purchase_order
      if purchase_order.update_attribute(:remarks, params[:remark])
        render json: {
            result: :ok,
            remarks: purchase_order.reload.remarks
        }
      else
        render json: {
            result: :failed
        }
      end
    else
      render json: {
          result: :failed
      }
    end

  end

  def po_line_commit
    status = params[:status]
    
    purchase_order = @account.purchase_orders.find(params[:id]) rescue false
    
    if purchase_order
      new_attr = {        
        reviewed_by_id:    @user.id , 
        overall_discount:  params[:overall_discount], 
        grand_total:       params[:net_amount], 
        cancellation_date: params[:cancel_date], 
        due_date:          params[:target_date],
        supplier_terms:    params[:supplier_terms],
        supplier_support_discount: params[:supplier_support_discount]
      }

      case status
      when 'P'
        new_attr.merge({ status: status })
      when 'A'
        new_attr.merge({ status: status })
      when 'C'
        new_attr.merge({ status: status })
      when 'V'        
        new_attr.merge({ status: status, voided_by_id: @user.id, void_reason: params[:void_reason] })
      end      

      if purchase_order.update_attributes(new_attr)        
        if status == 'V'
          redirect_to dashboards_path, notice: 'Purchase Order has been voided successfully!'
          return        
        end
        render json: { result: :ok }
      else
        render json: { result: :failed, message: "#{update_po.errors.full_messages.to_sentence}" }
      end
    else
      render json: { result: :failed, message: 'Unable to find the specific purchase order. Try again!' }
    end
  end

  def trigger_manual_po
    if params.include?(:branches)
      bParams = params[:branches]
      branchIds = []
      bParams.each { |k, v| branchIds.push v['value'] }    
      user_branches = @user.branches.where(id: branchIds).map(&:id).uniq.join(',')

      exec_rake :run_manual_po, account_id: @user.account_id, user_branches: user_branches, user_id: @user.id, email: @user.email
    else
      bParams = params[:suppliers]
      supplierIds = []
      bParams.each { |k, v| supplierIds << v['value'] }
      branch = Branch.find(params[:branch_id])
      user_suppliers = branch.suppliers.where(id: supplierIds).map(&:id).uniq.join(',')

      exec_rake :run_manual_po_per_branch, account_id: @user.account_id, branch_id: params[:branch_id], user_id: @user.id, email: @user.email, user_suppliers: user_suppliers
    end
    
    render json: { result: :processing }
  end

  private

  def check_session
    if !merchant_current_user.nil?
      @account = merchant_current_user.account
      @user = merchant_current_user
      roles = @user.roles.map(&:name)
      @edit_price = roles.include?('epo')
      @approve_void_po = roles.include?('apo')
      @copy_po = roles.include?('cpo')
      @review_po = roles.include?('rpo')
      if @account.subdomain.to_s != request.subdomain.to_s
        session[:merchant_current_user] = nil
        redirect_to new_session_path, alert: 'Session expired. Please re-login'
      end
    else
      redirect_to new_session_path, notice: 'Session expired! Please re-login!'
    end
  end

end
class SuppliersController < ApplicationController

  add_breadcrumb "Dashboard", :dashboards_path
  add_breadcrumb "Suppliers", :suppliers_path  

  before_filter :check_session
  before_filter :check_if_supplier_belongs_to_account, only: [:edit, :update, :destroy, :activate, :products, :activate_product, :get_product_categories]
  layout false, only: [:get_product_categories,  :search_unassigned, :assigner, :search_assigned, :purchase_orders]

  require "will_paginate/array"

  def index    
    respond_to do |format|
      format.html
      format.json { render json: SuppliersDatatable.new(view_context, @account, merchant_current_user) }
    end
    add_breadcrumb "index", suppliers_path
  end

  def new
    @supplier = @account.suppliers.new
  end

  def edit
  end

  def update
    if @supplier.update_attributes(params[:supplier])
      redirect_to suppliers_path, notice: 'Done updating supplier!'
    else
      render :new
    end
  end

  def create
    @supplier = @account.suppliers.new(params[:supplier])
    @supplier.is_principal = true
    if @supplier.save
      redirect_to edit_supplier_path(@supplier.id), notice: 'New supplier has been added successfully!'
    else
      render :new
    end
  end

  def destroy
    if @supplier.update_attributes(status: 'D')
      redirect_to suppliers_path, notice: 'Supplier has been deleted successfully!'
    else
      redirect_to suppliers_path, alert: 'Unable to delete supplier. Please try again!'
    end
  end

  def activate
    if @user.roles.map(&:name).include?("ssu")
      status = @supplier.status == 'I' ? 'A' : 'I'
      if @supplier.update_attributes(status: status)
        redirect_to suppliers_path, notice: 'Supplier status successfully updated!'
      else
        redirect_to suppliers_path, alert: 'Unable to update supplier status. Please try again!'
      end
    else
      redirect_to suppliers_path, alert: 'Unable to update supplier status. You are unauthorized to do this action!'
    end
  end

  def products    
    add_breadcrumb "#{@supplier.name}", products_supplier_path(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: ProductsDatatable.new(view_context, @supplier, merchant_current_user) }
    end
  end

  def purchase_orders
    if @user.role == 'buyer'
      suppliers = @user.suppliers.find(params[:id])
    elsif @user.role == 'supervisor'
      buyers = @account.users.where("supervisor_id = ? AND status = ? OR id = ?", @user.id, 'A', @user.id).map(&:id)
      ownership = @account.ownerships.where("user_id IN (?)", buyers)
      sup = ownership.suppliers.map(&:supplier_id)
      suppliers = @account.suppliers.where("supplier_id IN (?)", sup).find(params[:id])
    else
      suppliers = @account.suppliers.find(params[:id])
    end

    @po = suppliers.purchase_orders.joins(:supplier).joins(:product_category).joins(:branch)

    respond_to do |format|
      format.html
      format.json { render json: PurchaseOrdersDatatable.new(view_context, @po, merchant_current_user) } 
    end
  end

  def get_product_categories
    categories = @supplier.product_categories.uniq
    @categories = categories.to_json
  end

  def activate_product
    @product = @supplier.supplier_products.find_by_product_id(params[:product_id])

    if @product.status == 'A'
      @product.update_attributes(:status => 'I')
    else
      @product.update_attributes(:status => 'A')
    end

    redirect_to products_supplier_path(params[:id])
    flash[:notice] = 'Product status successfully updated.'
  end


  #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>ASSIGNER PORTION

  def assigner
    supplier = @account.suppliers.find(params[:supplier_id])
    supplier.principal_id = params[:id]
    
    if supplier.save
      render text: 'success'
    else
      render text: 'failed'
    end
  end

  def unassigner
    supplier = @account.suppliers.find(params[:supplier_id])
    supplier.principal_id = nil
    
    if supplier.save
      render text: 'success'
    else
      render text: 'failed'
    end
  end

  def search_assigned
    @distributors = @account.suppliers.where(status: 'A', principal_id: params[:id])
     respond_to do |format|
      format.html
      format.json { render json: AssignedSuppliersDatatable.new(view_context, @distributors, "supplier", @user) }
    end
  end

  def search_unassigned
    @distributors = @account.suppliers.where(status: 'A', principal_id: nil, is_principal: false)
    respond_to do |format|
      format.html
      format.json { render json: PendingSuppliersDatatable.new(view_context, @distributors, "supplier") }
    end
  end


  #>>>>>>>>>>>>>>>>>>>>>>>>>>>>PRIVATE METHODS
  private

  def check_if_supplier_belongs_to_account
    @supplier = @account.suppliers.find(params[:id])
    if @supplier.nil?
      redirect_to suppliers_path, notice: 'Unable to find the supplier. Please try again!'
      return
    end
  end

  def check_session
    if !merchant_current_user.nil?
      @account = merchant_current_user.account
      @user = merchant_current_user
      if @account.subdomain.to_s != request.subdomain.to_s
        session[:merchant_current_user] = nil
        redirect_to new_session_path, alert: 'Session expired. Please re-login'
      end
    else
      redirect_to new_session_path, notice: 'Session expired! Please re-login!'
    end
  end

end
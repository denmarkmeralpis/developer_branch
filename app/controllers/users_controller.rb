class UsersController < ApplicationController  

  require "will_paginate/array"

  before_filter :check_session, except: [:forget_password, :send_password, :recover_password, :reset_password]
  before_filter :check_if_user_belongs_to_account, only: [ :assigned_suppliers, :unassigned_suppliers, :assigner, :unassigner, :edit,  :destroy, :update, :activate ]
  layout false, only: [:unassigned_suppliers, :assigned_suppliers]

  def index    
    respond_to do |format|
      format.html
      format.json { render json: UsersDatatable.new(view_context, @account , merchant_current_user) }
    end    
  end

  def view_api_key
    add_breadcrumb "API Key"
    @user = merchant_current_user    
    @api_key = @user.account.api_key.access_token    
  end

  def change_password
    @user = merchant_current_user
    if request.method.to_s == 'POST'
      if @user.update_attributes(params[:user])
        redirect_to change_password_users_path, notice: 'Account has been updated successfully!'
      else
        render 'change_password'
      end
    end
  end

  def update_password
    render json: params
  end

  def new
    add_breadcrumb "Add New User"
    @user  = User.new
    @user.roles.build
  end

  def assigned_suppliers
    @distributors = @checked_user.suppliers.where(status: 'A', is_principal: false)
    respond_to do |format|
      format.html
      format.json { render json: AssignedSuppliersDatatable.new(view_context, @distributors, "user", @user) }
    end
  end

  def unassigned_suppliers    
    respond_to do |format|
      format.html
      format.json { render json: PendingSuppliersDatatable.new(view_context, :user, @checked_user) }
    end
    
  end

  def assigner

    if @checked_user.role == 'buyer'
      supervisor = @account.users.find(@checked_user.supervisor_id)
      supervisor.buyer_suppliers.create(supplier_id: params[:supplier_id])
    end

    buyerSupplier = @checked_user.buyer_suppliers.new(supplier_id: params[:supplier_id])
    if buyerSupplier.save
      category_ids_arr = params[:category_ids].split(',')
      category_ids_arr.each do |i|
        @account.ownerships.create(product_category_id: i, supplier_id: params[:supplier_id], user_id: params[:id])
      end
      render text: 'success'
    else
      render text: 'failed'
    end

  end

  def unassigner

    if @checked_user.role == 'buyer'
      supervisor = @account.users.find(@checked_user.supervisor_id)
      supervisorSupplier = supervisor.buyer_suppliers.where(supplier_id: params[:supplier_id]).first
      supervisorSupplier.destroy
    end

    buyerSupplier = @checked_user.buyer_suppliers.where(supplier_id: params[:supplier_id]).first
    ownershipTable = @account.ownerships.where(supplier_id: params[:supplier_id], user_id: params[:id])
    
    if buyerSupplier.destroy && ownershipTable.destroy_all
      render text: 'success'
    else
      render text: 'failed'
    end
  end

  def create
    @user = merchant_current_user.sub_users.new(params[:user].merge(account_id: @account.id))
    if @user.save
      redirect_to users_path, notice: 'User has been added successfully!'
    else
      render 'new'
    end
  end

  def edit
    @supervisor = @account.users.where(role: :supervisor, status: 'A').where('id != ?', params[:id])
    @user = @account.users.find(params[:id])
    @user.roles.build
    add_breadcrumb "#{@user.first_name} #{@user.last_name}"
  end

  def update
    @user = @checked_user
    user_params = params[:user]
    role_params = user_params.delete('roles_attributes')
    user_params = user_params.except('roles_attributes') if user_params.nil? == false
    if user_params.present?
      result = @user.update_attributes(user_params)
    end
    if role_params.present?
      @user.roles.destroy_all
      role_params.each do |role|
        @user.roles.create(role)
      end
      result = true
    end
    if result
      redirect_to users_path
      flash[:notice] = 'User successfully updated.'
    else
      render 'edit'
    end
  end

  def destroy
    @user = @checked_user
    @user.status = 'D'
    @user.save
    redirect_to users_path
    flash[:notice] = 'User successfully deleted.'
  end

  def activate
    @user = @checked_user
    if @user.status == 'A'
      @user.update_attribute('status', 'I')
    else
      @user.update_attribute('status', 'A')
    end
    redirect_to users_path
    flash[:notice] = 'User status successfully updated.'
  end

  def forget_password
  end

  def send_password
    user = User.find_by_email(params[:email])
    if user
      reset_password_token = loop do
        random_token = SecureRandom.urlsafe_base64(nil, false)
        break random_token unless User.exists?(reset_password_token: random_token)
      end
      if user.update_attributes(reset_password_token: reset_password_token)
        MerchantMailer.reset_password(params[:email], reset_password_token, user.account.subdomain).deliver
        render text: 'sent'
      else
        render text: 'failed'
      end
    else
      render text: 'unknown'
    end
  end

  def reset_password
    user = User.find_by_reset_password_token(params[:id])
    if user
      user.update_attributes(password: params[:password_reset])
      render text: 'success'
    else
      render text: 'failed'
    end
  end

  private

  def check_if_user_belongs_to_account
    @checked_user = @account.users.find(params[:id])
    if @checked_user.nil?
      redirect_to users_path, alert: 'Unable to find user. Try again!'
      return
    end
  end

  def check_session
    if !merchant_current_user.nil?
      @account = merchant_current_user.account
      @user = merchant_current_user
      if @account.subdomain.to_s != request.subdomain.to_s
        session[:merchant_current_user] = nil
        redirect_to new_session_path, alert: 'Session expired. Please re login'
      end
      add_breadcrumb "Dashboard", :dashboards_path
      add_breadcrumb "Users",     :users_path
    else
      redirect_to new_session_path, notice: 'Session expired! Please re login!'
    end
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :role, :supervisor_id,  :roles_attributes => [:name])
  end

end
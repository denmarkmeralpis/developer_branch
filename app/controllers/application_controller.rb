class ApplicationController < ActionController::Base

  auto_session_timeout 4.hours

  protect_from_forgery

  helper_method :current_user, :merchant_current_user

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def merchant_current_user
    @merchant_current_user ||= User.find(session[:merchant_user_id]) if session[:merchant_user_id]
  end

  private

  def exec_rake(task, options = {})
  	options[:rails_env] ||= Rails.env
  	args = options.map { |n, v| "#{n.to_s.upcase}='#{v}'" }
  	system "rake #{task} #{args.join(' ')} --trace 2>&1 >> #{Rails.root}/log/rake.log &"
  end

end

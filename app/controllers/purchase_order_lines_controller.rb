class PurchaseOrderLinesController < ApplicationController
	
	before_filter :check_session

	def order_to_suppliers
		purchaseOrderLineId = params[:purchase_order_line_id]
		purchaseOrderLine = @account.purchase_order_lines.find(purchaseOrderLineId) rescue nil
		if purchaseOrderLine.present?
			product_id  = purchaseOrderLine.product_id
			supplier_id = purchaseOrderLine.purchase_order.supplier_id
			supplier    = @account.branch_products.order_from_suppliers_lists(supplier_id, product_id)			
			@supplier   = supplier
		else
			@supplier   = []
		end
		@supplier = @supplier.includes(:supplier).map do |s|
			{
				name: s.supplier.name,
				supplier_id: s.supplier_id
			}
		end
		render json: @supplier
	end

	def transfer_to_supplier
		
		orderline  = @account.purchase_order_lines.find(params[:purchase_order_line_id])		

		if orderline.blank?
			render json: { result: :failed, message: 'Data not found! Try again!' }
		else

			# previous po
			po = orderline.purchase_order
			old_po = po

			# Get the params for matching po method
			supplier_id = params[:supplier_id]
			branch      = po.branch			
			product_id  = orderline.product.id
			category_id = po.product_category_id			

			# find matching purchase order
			purchase_order = branch.purchase_orders#.joins(:purchase_order_lines)			
			purchase_order = purchase_order.matching_purchase_order('P', category_id, supplier_id)

			# ************************
			# INITIALIZE VARS START

			# create new purchase order
			ads = orderline.average_daily_sales
			dts = orderline.stock_keeping_days
			ohq = orderline.onhand_quantity
			sq  = orderline.suggested_quantity
			qpu = orderline.quantity_per_unit
			utp = orderline.quantity_to_purchase
			pp  = orderline.purchase_price
			d   = orderline.discount_text

			# subtotal PO table
			st = (utp * pp)

			# INITIALIZE VARS END
			# ************************			

			# check if supplier has po with the same product id
			if purchase_order.present?				

				# po checking
				po_via_type = purchase_order.joins(:purchase_order_lines)
				po_via_type = po_via_type.where("purchase_order_lines.product_id = ?", product_id).uniq

				if po_via_type.present? && po_via_type.count > 1

					# choose where orderline destination from the selected pos
					render json: {
						result: :choose,
						pos: po_via_type,
						product_id: product_id
					}
					return
				elsif po_via_type.count == 1
					commit_transfer_to_supplier_method(po_via_type.first.id, orderline, product_id)
				else
					# ============================================
					# add the requested po line to this current po
					# ============================================

					# purchase order of the selected po with same category
					po = purchase_order.first.id
					po = PurchaseOrder.find(po)

					# update the requested order line to the selected po
					update_orderline = orderline

					update_orderline.purchase_order_id = po.reference
					update_orderline.save
					update_orderline.reload

					# PO Totals update desitination
				   price = update_orderline.units_to_purchase * update_orderline.purchase_price
				   po.total_po_cost  		+= price
				   po.total_units    		+= update_orderline.units_to_purchase rescue 0
				   po.total_quantity 		+= update_orderline.quantity_to_purchase rescue 0
				   po.gross_amount   		+= update_orderline.subtotal
				   po.total_item_discount  += (price - update_orderline.subtotal)
				   po.grand_total    		+= update_orderline.subtotal
				   po.save

				   # PO Totals update current				   
				   old_po.total_po_cost  		-= price
				   old_po.total_units    		-= update_orderline.units_to_purchase rescue 0
				   old_po.total_quantity 		-= update_orderline.quantity_to_purchase rescue 0
				   old_po.gross_amount   		-= update_orderline.subtotal
				   old_po.total_item_discount -= (price - update_orderline.subtotal)
				   old_po.grand_total    		-= update_orderline.subtotal
				   old_po.save

					# return success response
					render json: { result: :ok, message: "added to po with category #{po.reference}", po: old_po, supplier_name: po.supplier.name }
					return
				end

				old_po.destroy if old_po.purchase_order_lines.blank?
				po.destroy 		if old_po.purchase_order_lines.blank?

			else				
				# item discount
            d.split(',').each { |i| st -= i.last == '%' ? st * (BigDecimal.new(i) / 100) : BigDecimal.new(i) }

            # final variables
				od  = 0		
				tpc = (utp * pp)
				tid = (tpc - st)						
				gt  = (tpc - (tid + od))
				ga  = (tpc - tid)

				reference = DateTime.now.strftime('%Q').to_s.to_i

				# new purchase order attributes
				purchase_order_attr = {
					reference: reference,
					created_by_id: merchant_current_user.id,
					assigned_to_user_id: merchant_current_user.id,
					branch_id: branch.id,
					supplier_id: supplier_id,
					product_category_id: category_id,
					status: 'P',
					total_po_cost: tpc,
					total_item_discount: tid,
					overall_discount: od,
					grand_total: gt,
					gross_amount: ga,
					cancellation_date: DateTime.now + 15.days,
					due_date: DateTime.now + 2.days
				}

				# new purchase order object init
				purchase_order = @account.purchase_orders.new(purchase_order_attr)

				# save new purchase order object
				if purchase_order.save					

					# update the po line and link to the new purchase order
					orderline.purchase_order_id = purchase_order.reload.reference
					orderline.save
					orderline.reload
				    
				   # PO Totals update
				   price = orderline.units_to_purchase * orderline.purchase_price
				   po.total_po_cost  		-= price
				   po.total_units    		-= orderline.units_to_purchase rescue 0
				   po.total_quantity 		-= orderline.quantity_to_purchase rescue 0
				   po.gross_amount   		-= orderline.subtotal
				   po.total_item_discount  -= (price - orderline.subtotal)
				   po.grand_total    		-= orderline.subtotal
				   po.save				   

					# notify success transfer
					render json: { result: :ok, message: 'Cool!', po: po.reload, supplier_name: purchase_order.supplier.name }										
				else

					# notify failed transfer
					render json: { result: :failed, message: purchase_order.errors.full_messages.to_sentence }
				end

			end

		end

	end

	def commit_transfer_to_supplier
		orderline = @account.purchase_order_lines.find(params[:orderline])
		product_id = params[:product_id]
		commit_transfer_to_supplier_method(params[:purchase_order_id], orderline, product_id)
	end	
	
	private

	def commit_transfer_to_supplier_method(purchase_order_id, orderline, product_id)
		old_po			= orderline.purchase_order
		purchase_order = @account.purchase_orders.find(purchase_order_id)
		new_orderline  = purchase_order.purchase_order_lines.find_by_product_id(product_id)

		# old orderline
		orderline_qtp = orderline.quantity_to_purchase
		orderline_qpu = orderline.quantity_per_unit
		orderline_utp = orderline.units_to_purchase
		orderline_ds  = orderline.discount_text
		orderline_st  = orderline.subtotal
		orderline_pp  = orderline.purchase_price


		# check if same quantity per unit
		if orderline_qpu == new_orderline.quantity_per_unit
			
			# just add the qty
			new_orderline.quantity_to_purchase += orderline_qtp
			new_orderline.units_to_purchase     = new_orderline.quantity_to_purchase * new_orderline.quantity_per_unit												
			new_orderline_ds 							= new_orderline.discount_text.split(",")
			new_orderline_ds 							= new_orderline_ds + orderline_ds.split(",")
			new_orderline.discount_text		  	= new_orderline_ds.join(",")

			# Discount Computation and subtotal
			st = (new_orderline.units_to_purchase * new_orderline.purchase_price)
			new_orderline_ds.each do |d|
				st -= d.last == '%' ? st * (BigDecimal.new(d) / 100) : BigDecimal.new(d)							
			end

			new_orderline.subtotal = st
			new_orderline.save												
		
		else
			
			new_orderline_ds 							= new_orderline.discount_text.split(",")
			new_orderline_ds 							= new_orderline_ds + orderline_ds.split(",")
			new_orderline.quantity_to_purchase += orderline_qtp						

			st =  new_orderline.quantity_to_purchase * new_orderline.purchase_price
			
			new_orderline_ds.each do |d|
				st -= d.last == '%' ? st * (BigDecimal.new(d) / 100) : BigDecimal.new(d)							
			end						

			new_orderline.subtotal = st
			new_orderline.save


		end

		# PO totals update destination		
		price = orderline_pp * orderline_utp
		purchase_order.total_po_cost  		+= price
	   purchase_order.total_units    		+= orderline.units_to_purchase rescue 0
	   purchase_order.total_quantity 		+= orderline.quantity_to_purchase rescue 0
	   purchase_order.total_item_discount  += (price - orderline.subtotal)
	   purchase_order.gross_amount   		+= orderline.subtotal				   
	   purchase_order.grand_total    		+= orderline.subtotal
	   purchase_order.save

		# PO Totals update old					
	   old_po.total_po_cost  		-= price
	   old_po.total_units    		-= orderline.units_to_purchase rescue 0
	   old_po.total_quantity 		-= orderline.quantity_to_purchase rescue 0
	   old_po.total_item_discount -= (price - orderline.subtotal)
	   old_po.gross_amount   		-= orderline.subtotal				   
	   old_po.grand_total    		-= orderline.subtotal
	   old_po.save

		orderline.destroy

		render json: { result: :ok, po: old_po, supplier_name: purchase_order.supplier.name }
		return	
	end
	
	def check_session
		if !merchant_current_user.nil?
	      @account = merchant_current_user.account
	      @user = merchant_current_user
	      roles = @user.roles.map(&:name)
	      @edit_price = roles.include?('epo')
	      @approve_void_po = roles.include?('apo')
	      @copy_po = roles.include?('cpo')
	      @review_po = roles.include?('rpo')
	      if @account.subdomain.to_s != request.subdomain.to_s
	        session[:merchant_current_user] = nil
	        redirect_to new_session_path, alert: 'Session expired. Please re-login'
	      end
	    else
	      redirect_to new_session_path, notice: 'Session expired! Please re-login!'
	    end
	end

end
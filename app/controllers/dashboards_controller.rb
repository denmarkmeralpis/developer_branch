class DashboardsController < ApplicationController

  before_filter :check_session
  layout false, only: [:supplier_index]

  def index

    params[:show_all] = false if params[:show_all].nil? 

    pos = @account.purchase_orders.where("created_at >= ?", Time.zone.now.beginning_of_day)
    if @user.role == 'buyer'
      @branches = @user.branches
      po_count = @user.purchase_orders.where("created_at >= ?", Time.zone.now.beginning_of_day).group(:status).count
    elsif @user.role == 'supervisor'
      @branches = @user.branches
      buyers = @account.users.where("supervisor_id = ? AND status = ? OR id = ?", @user.id, 'A', @user.id).map(&:id)
      po_count = pos.where(:assigned_to_user_id => buyers).group(:status).count
    else
      @branches = @account.branches
      po_count = pos.group(:status).count 
    end

    @new_count      = 0
    @approved_count = 0
    @reviewed_count = 0
    @voided_count   = 0

    po_count.each do |status, count|
      @new_count      = count if status == 'P'
      @reviewed_count = count if status == 'C'
      @approved_count = count if status == 'A'
      @voided_count   = count if status == 'V'
    end

    if (params[:show_all].to_s == 'true')
      respond_to do |format|
        format.html
        format.json { render json: BranchesDatatable.new(view_context, @branches, merchant_current_user, true) }
      end
    else
      respond_to do |format|
        format.html
        format.json { render json: BranchesDatatable.new(view_context, @branches, merchant_current_user, false) }
      end
    end

  end

  def show
  end

  def upload
    uploaded_io = params[:upload_file]
    puts("<<<<<<<<<<<<<<<<<<<<<<<<")
    puts(Rails.root)
    puts("<<<<<<<<<<<<<<<<<<<<<<<<")
    filePath = Rails.root.join('test', 'test_data' , uploaded_io.original_filename)
    File.open(Rails.root.join('test', 'test_data' ,uploaded_io.original_filename), 'wb') do |file|
      file.write(uploaded_io.read)
    end

    if File.exists?(filePath)
     puts "======UNZIP STARTS============="
     filePath2 = Rails.root.join('test', 'test_data')

     puts "****FILE PATH 2***"
     puts filePath2

     file_name = File.join(filePath2, "*.zip")
     fileName_x = Dir.glob(file_name).first

     puts "****FILE NAME***"
     puts fileName_x

     Zip::ZipFile.open(fileName_x) { |zip_file|
        zip_file.each { |f|
          f_path=File.join(filePath2 ,f.name)
          puts "----DESTINATION-----"
          puts f_path
          FileUtils.mkdir_p(File.dirname(f_path))
          zip_file.extract(f, f_path) unless File.exist?(f_path)
        }
      }
      mac_os = File.join(filePath2,"__MACOSX")
      FileUtils.rm_rf(mac_os) 
      File.delete(fileName_x)
  
     puts "======UNZIP ENDS============="

     @account.start_import
     redirect_to upload_dashboards_path, notice: 'File successfully uploaded!'

    else
      redirect_to upload_dashboards_path, alert: 'File failed to upload!'
    end

  end

  def supplier_index

    params[:show_all2] = false if params[:show_all2].nil? 

    if @user.role == 'buyer'
      @suppliers = @user.suppliers
    elsif @user.role == 'supervisor'
      buyers = @account.users.where("supervisor_id = ? AND status = ? OR id = ?", @user.id, 'A', @user.id).map(&:id)
      ownership = @account.ownerships.where("user_id IN (?)", buyers)
      sup = ownership.map(&:supplier_id)
      @suppliers = @account.suppliers.where(id: sup)
    else
      @suppliers = @account.suppliers
    end

    if (params[:show_all2].to_s == 'true')
      respond_to do |format|
        format.html
        format.json { render json: SuppliersDatatable2.new(view_context, @suppliers, merchant_current_user, true) } 
      end
    else
      respond_to do |format|
        format.html
        format.json { render json: SuppliersDatatable2.new(view_context, @suppliers, merchant_current_user, false) } 
      end
    end

  end

  private

  def check_session
    if !merchant_current_user.nil?
      @account = merchant_current_user.account
      @user = merchant_current_user
      if @account.subdomain.to_s != request.subdomain.to_s
        session[:merchant_current_user] = nil
        redirect_to new_session_path, alert: 'Session expired. Pleas- re login'
      end
    else
      redirect_to new_session_path, notice: 'Session expired! Pleas- re login!'
    end
  end

end
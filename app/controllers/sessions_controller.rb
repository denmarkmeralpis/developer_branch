class SessionsController < ApplicationController

  auto_session_timeout_actions

  before_filter :check_session, only: [:new]

  def active
    render_session_status
  end

  def timeout
    render_session_timeout
  end

  def new
    reset_session
    @subdomain = request.subdomain
    @has_subdomain = nil
    if @subdomain.blank? == false && @subdomain != 'www'
      account = Account.find_by_subdomain(@subdomain) rescue nil
      if account.nil?
        redirect_to root_url(subdomain: false)
      else
        @has_subdomain = params[:email]
      end
    end
  end

  def locate_subdomain
    subdomain = User.locate_subdomain(params[:email])
    if subdomain.nil?
      redirect_to new_session_path, alert: 'User not registered!'
    else
      redirect_to "#{request.protocol}#{subdomain}.lvh.me:#{request.port}?email=#{params[:email]}"
    end
  end

  def create
    email    = params[:email]
    password = params[:password]
    user = User.authenticate(email, password)
    if user && user.role != 'cb_admin' && user.status != 'I'  && user.status != 'D'
      if user.account.subdomain.to_s == request.subdomain.to_s
        reset_session
        session[:merchant_user_id] = user.id
        redirect_to dashboards_url(subdomain: request.subdomain), notice: 'Welcome to your Dashboard!'
      else
        redirect_to root_url(subdomain: user.account.subdomain, email: email), alert: 'You are redirected to your CloudBuy URL!'
      end
    else
      @has_subdomain = email
      flash.now.alert = 'Invalid username/password. Try again!'
      render :new
    end
  end

  def destroy
    session[:merchant_user_id] = nil
    redirect_to root_path
  end

  private

  def check_session
    if merchant_current_user
      redirect_to dashboards_path
    end
  end

end
class TransactionsController < ApplicationController

  before_filter :check_session
  before_filter :check_if_po_lines_belongs_to_account, only: [:sales_and_inventory_data, :inventory_sales_ratio, :historical_data]

  def sales_and_inventory_data
    transactions    = @product_transactions.statistical_data('POS', @from, @to)
    result_trans    = transactions.sum(:quantity)
    graph_inventory = transactions.sum(:last_inventory).map(&:last)
    graph_labels    = result_trans.map(&:first)
    graph_sales     = result_trans.map(&:last)
    ads             = (graph_sales.sum.to_f / graph_sales.count.to_f)

    ads = 0 if ads.to_s == 'NaN'

    render json: { labels: graph_labels, sales_data: graph_sales, inventory_data: graph_inventory, avg_daily_sales: ads }
  end

  def inventory_sales_ratio
    transactions    = @product_transactions.statistical_data('POS', @from, @to)
    result_trans    = transactions.sum(:quantity)
    graph_inventory = transactions.sum(:last_inventory).map(&:last)
    graph_labels    = result_trans.map(&:first)
    graph_sales     = result_trans.map(&:last)
    newData = []
    graph_labels.each_index { |i| newData[i] = (graph_inventory[i].to_f / graph_sales[i].to_f).to_f }
    render json: { labels: graph_labels, data: newData, }
  end

  def historical_data
    transactions = @product_transactions.statistical_data('POS', @from, @to)
    summary = transactions.map {|transaction| { quantity: transaction.quantity, date: transaction.date } }
    render json: { result: :ok, data: summary }
  end

  private

  def check_if_po_lines_belongs_to_account
    po_lines = @account.purchase_order_lines.find(params[:id])
    @product_transactions = po_lines.product.transactions

    if params[:duration].to_i == 0
      # @from = Date.strptime(params[:from], '%m/%d/%y').beginning_of_day
      # @to   = Date.strptime(params[:to], '%m/%d/%y').end_of_day
      @from = params[:from].to_date.beginning_of_day
      @to   = params[:to].to_date.end_of_day
    else
      @from = params[:duration].to_i.days.ago.beginning_of_day
      @to   = Time.zone.now.end_of_day
    end

  end

  def check_session
    if !merchant_current_user.nil?
      @account = merchant_current_user.account
      @user = merchant_current_user
      if @account.subdomain.to_s != request.subdomain.to_s
        session[:merchant_current_user] = nil
        redirect_to new_session_path, alert: 'Session expired. Please re login'
      end
    else
      redirect_to new_session_path, notice: 'Session expired! Please re login!'
    end
  end

end
var opts = { bezierCurve : true, bezierCurveTension : 0.4 };
var current_line_id;
var average_daily_sales, on_hand_quantity, suggested_quantity, supplier_name, branch_name;
var ADS;   	    

var selectedTab = "";

var fromToDuration = function() {
	var _duration = $("#duration").val();
	
	if ( _duration.toString() == "0" ) {
		var from = $("#from").val();
		var to   = $("#to").val();		
		return { "from": encodeURI(from), "to": encodeURI(to), "duration": _duration, "id": current_line_id };
	} else {
		return { "from": "", "to": "", "duration": _duration, "id": current_line_id };
	}
}

var drawCanvas = function(labels, data1, data2, container, avgDailySales) {

	var SAIC = $("#sales-inv");
	var ISRC = $("#inventory-sales-ratio");	
	var HSDC = $("#historical-data");
	
	// RESET
	SAIC.html("");
	ISRC.html("");
	HSDC.html("");	

	// REDRAW
	SAIC.html("<canvas id='SAIC' height='250' width='834'></canvas>");
	ISRC.html("<canvas id='ISRC' height='250' width='834'></canvas>");
	HSDC.html("<canvas id='HSDC' height='250' width='834'></canvas>");

	var siCtx = $(container).get(0).getContext("2d");
	var siDataSets = {
		labels : labels,
		datasets : [
			{
				fillColor : "rgba(66,139,202,0.5)",
				strokeColor : "rgba(66,139,202,0.9)",
				pointColor : "rgba(19,84,141,0.9)",
				pointStrokeColor : "#fff",
				pointHighlightFill: "rgba(66,139,202,1)",
				pointHighlightStroke: "rgba(220,220,220,1)",
				scaleShowGridLines : true,
				data : data1

			},
			{
				fillColor: "rgba(220,220,220,0.6)",
				strokeColor: "rgba(220,220,220,1)",
				pointColor: "rgba(220,220,220,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill: "gray",
				pointHighlightStroke: "rgba(220,220,220,1)",
				scaleShowGridLines : true,
				data : data2
			}
		]
	}
	var siNewChart = new Chart(siCtx).Line(siDataSets, opts);
	$("#modal-average-daily-sales").text(avgDailySales);
	if(action_state == 1) {
		$(".ads-" + current_line_id).text(avgDailySales);
		$(".dts-" + current_line_id).keyup();
	}	
}

$(".graph-btn").click(function() {	
	
	current_line_id = $(this).attr('data-line-id');    
	
	$("#graphs-modal").modal("show");    

	average_daily_sales = $(".ads-" + current_line_id).text();
	on_hand_quantity = $(".ohq-" + current_line_id).val();
	suggested_quantity = $(".sq-" + current_line_id).text();
	supplier_name = $("#supplier-name").text();
	branch_name = $("#branch-name").text();
	srp = $("#srp-" + current_line_id).text();
	last_received_qty = $("#last-received-qty-" + current_line_id).text();
	last_received_date = $("#last-received-date-" + current_line_id).text();
	days_to_stock = $(".dts-" + current_line_id).val();
   product_name = $(".product-name-" + current_line_id).text();	

	// Setting up details on modal
	$("#modal-po-date").text("As of " + $("#po-date").text());
	$("#modal-branch-name").text(branch_name);
	$("#modal-supplier-name").text(supplier_name);
	$("#modal-average-daily-sales").text(average_daily_sales);
	$("#modal-on-hand-quantity").text(on_hand_quantity);
	$("#modal-suggested-quantity").text(suggested_quantity);
	$("#modal-srp").text(srp);
	$("#modal-last-received-qty").text(last_received_qty);
	$("#modal-last-received-date").text(last_received_date);	
	$("#modal-product-name").text(product_name);

	// init duartion
	var first_opts = $("#duration option:first");
   first_opts.attr("value", days_to_stock);
   first_opts.text("Last " + days_to_stock + " days");
	
   // select duration
   $("#duration").val(days_to_stock);

	// Click the sales and inventory
	$("#sinv-btn").click();		

	// hide date cont
	$(".date-cont").css('display', 'none');
});

// SALES AND INVENTORY
$("#sinv-btn").click(function() { 	
	parameters = fromToDuration();

	$.get('/transactions/sales-and-inventory', parameters, function(response) { 		

		var label = new Array();
	    var data1 = new Array();
	    var data2 = new Array();	 	    

		label = response.labels;
		data1 = response.sales_data;
		data2 = response.inventory_data;
		ADS   = response.average_daily_sales;		
		
		drawCanvas(label, data1, data2, "#SAIC", ADS);
	});

	selectedTab = "#sinv-btn";
});

// INVENTORY SALES RATIO
$("#isr-btn").click(function() {	
	parameters = fromToDuration();	

	$.get('/transactions/inventory-sales-ratio', parameters, function(DATA) {
		
		var label = DATA.labels;
		var data1 = DATA.data;
		var data2 = [];		

		// Draw the graph to canvas
		drawCanvas(label, data1, data2, "#ISRC", ADS);		
	});

	selectedTab = "#isr-btn";
});

// HISTORICAL DATA
$("#hd-btn").click(function() {	
	parameters = fromToDuration();

	$.get('/transactions/historical-data', parameters, function(DATA) {
		var label = new Array();
		var data1 = new Array();				
		for(var i=0; i < DATA.data.length; i++) {
			label.push(DATA.data[i].date);
			data1.push(DATA.data[i].quantity);
		}		
		drawCanvas(label, data1, [], "#HSDC", ADS);
	});

	selectedTab = "#hd-btn";
});

$("#duration").change(function () {
	var durationValue = $(this).val();
	if ( durationValue == 0 ) {
		$(".date-cont").css('display', 'block');		
	} else {
		$(".date-cont").css('display', 'none');		
	}
	$(selectedTab).click();
});

var dateFromTo = function() {
	$(selectedTab).click();
}

// var product_name;
// var supplier_name;
// var branch_name;
// var average_daily_sales;
// var on_hand_quantity;
// var suggested_quantity;
// var purchase_order_date;
// var current_line_id;
// var duration;
// var from = '';
// var to   = '';

// var cache_labels_si  = '';
// var cache_data1_si   = '';
// var cache_data2_si   = '';
// var cache_labels_isr = '';
// var cache_data_isr   = '';
// var cache_labels_hd  = '';
// var cache_data1_hd   = '';
// var cache_ads        = '';
// var current_tab_id   = '';
// var opts = { bezierCurve : true, bezierCurveTension : 0.4 }

// // Duration

// $("#duration").change(function(){
//     cache_reset();
//     duration = $(this).val();
//     var dateContainer = $(".date-cont");

//     if ( duration == 0 ) {
//         dateContainer.css("display", "block");

//         min_date = moment().subtract('years',3);
//         $("#from").datetimepicker({ pickTime: false , minDate: min_date});
//         $("#to").datetimepicker({ pickTime: false}); 


//         $('#from').datetimepicker();
//         $('#to').datetimepicker();
//     } else {
//         from = '';
//         to   = '';
//         dateContainer.css("display", "none");
//         $(current_tab_id).click();
//     }
// });

// // Listener for onChange in Date Picker "(FROM & TO)"
// $("#from").on("dp.change",function (e) { $('#to').data("DateTimePicker").setMinDate(e.date); });

// $("#from, #to").on('dp.change', function(e){
//     cache_reset();
//     from = $("#from input").val();
//     to   = $('#to input').val();
//     $(current_tab_id).click();
// });

// // Initialize Everything
// var cache_reset = function(){
//     cache_labels_si = '';
//     cache_data1_si  = '';
//     cache_data2_si  = '';
//     cache_labels_hd = '';
//     cache_data1_hd  = '';
//     cache_data_isr  = '';
//     cache_labels_isr = '';
// }

// var initialize_modal_details = function(){
//     product_name        = $(".product-name-" + current_line_id).text();
//     supplier_name       = $("#supplier-name").text();
//     branch_name         = $("#branch-name").text();
//     average_daily_sales = $(".ads-" + current_line_id).attr('data-value');
//     on_hand_quantity    = $(".ohq-" + current_line_id).val();
//     suggested_quantity  = $(".sq-" + current_line_id).attr('data-value');
//     purchase_order_date = "As of " + $("#po-date").text();
	
//     // Put Details to the modal
//     $("#modal-product-name").text(product_name);
//     $("#modal-supplier-name").text(supplier_name);
//     $("#modal-branch-name").text(branch_name);
//     $("#modal-average-daily-sales").text(average_daily_sales);
//     $("#modal-on-hand-quantity").text(on_hand_quantity);
//     $("#modal-suggested-quantity").text(suggested_quantity);
//     $("#modal-po-date").text(purchase_order_date);

//     $("#sinv-btn").click();
// }

// $(".graph-btn").click(function(){    
//     current_line_id = $(this).attr("data-line-id");
	
//     var days_to_stock = $(".dts-" + current_line_id).val();
	
//     var first_opts = $("#duration option:first");

//     first_opts.attr("value", days_to_stock);

//     first_opts.text("Last " + days_to_stock + " days");

//     duration = $(".date-cont");

//     duration.css("display", "none");

//     duration = $("#duration").val(days_to_stock);

//     duration = days_to_stock;

//     cache_reset();
	
//     initialize_modal_details();
//     $("#graphs-modal").modal("show", {keyboard: false});
// });

// // Sales & Inventory

// var get_canvas_data_si = function(duration, from, to){

//     if (cache_data1_si == '' || cache_data2_si == '' || cache_labels_si == '' || cache_ads == '') {
//         $.get("/transactions/sales-and-inventory", {"id": current_line_id, "duration": duration, "from": from, "to": to}, function(siDataResult){
//             start_drawing_si(siDataResult.labels, siDataResult.sales_data, siDataResult.inventory_data, siDataResult.avg_daily_sales);
//             cache_labels_si = siDataResult.labels;
//             cache_data1_si  = siDataResult.sales_data;
//             cache_data2_si  = siDataResult.inventory_data;
//             cache_ads       = siDataResult.avg_daily_sales;
//         });
//     } else {
//         start_drawing_si(cache_labels_si, cache_data1_si, cache_data2_si, cache_ads);
//     }
// }

// var redraw_canvas_si = function() {
//     var newSiCanvas = "<canvas id='si-graph-container' height='250' width='834'></canvas>"
//     $("#sales-inventory").html("");
//     $("#sales-inventory").html(newSiCanvas);
// }

// var start_drawing_si = function(_labels, _data1, _data2, _ads) {
//     $("#modal-average-daily-sales").text(_ads);

//     var ads_view = $(".ads-" + current_line_id);

//     ads_view.html('');
//     ads_view.html(_ads);
//     ads_view.removeAttr('data-value');
//     ads_view.attr('data-value', _ads)
//     $(".dts-" + current_line_id).keyup();

//     redraw_canvas_si();
	
//     var siCtx = $("#si-graph-container").get(0).getContext("2d");
//     var siDataSets = {
//         labels : _labels,
//         datasets : [
//             {
//                 fillColor : "rgba(66,139,202,0.5)",
//                 strokeColor : "rgba(66,139,202,0.9)",
//                 pointColor : "rgba(19,84,141,0.9)",
//                 pointStrokeColor : "#fff",
//                 pointHighlightFill: "rgba(66,139,202,1)",
//                 pointHighlightStroke: "rgba(220,220,220,1)",
//                 scaleShowGridLines : true,
//                 data : _data1

//             },
//             {
//                 fillColor: "rgba(220,220,220,0.6)",
//                 strokeColor: "rgba(220,220,220,1)",
//                 pointColor: "rgba(220,220,220,1)",
//                 pointStrokeColor : "#fff",
//                 pointHighlightFill: "gray",
//                 pointHighlightStroke: "rgba(220,220,220,1)",
//                 scaleShowGridLines : true,
//                 data : _data2
//             }
//         ]
//     }
//     var siNewChart = new Chart(siCtx).Line(siDataSets, opts);
// }

// $("#sinv-btn").click(function(){
//     current_tab_id = "#sinv-btn";
//     get_canvas_data_si(duration, from, to);
// });


// // Inventory Sales Ratio

// $("#isr-btn").click(function(){
//     current_tab_id = "#isr-btn";
//     get_canvas_data_isr(duration, from, to)
// });

// var get_canvas_data_isr = function(duration, from, to) {
//     if(cache_data_isr == '' || cache_labels_isr == '') {
//         $.get("/transactions/inventory-sales-ratio", {"id": current_line_id, "duration": duration, "from": from, "to": to}, function(isrDataResult){
//             start_drawing_isr(isrDataResult.labels, isrDataResult.data);
//             cache_labels_isr = isrDataResult.labels;
//             cache_data_isr   = isrDataResult.data;
//         });
//     } else {
//         start_drawing_isr(cache_labels_isr, cache_data_isr);
//     }
// }

// var start_drawing_isr = function(_labels, _data1) {
//     reset_canvas_isr();
//     var isrCtx = $("#isr-graph-container").get(0).getContext("2d");
//     var isrDataSets = {
//         labels : _labels,
//         datasets : [
//             {
//                 fillColor: "rgba(220,220,220,0.6)",
//                 strokeColor: "rgba(220,220,220,1)",
//                 pointColor: "rgba(220,220,220,1)",
//                 pointStrokeColor : "#fff",
//                 pointHighlightFill: "gray",
//                 pointHighlightStroke: "rgba(220,220,220,1)",
//                 scaleShowGridLines : true,
//                 data : _data1
//             }
//         ]
//     }
//     var isrNewChart = new Chart(isrCtx).Line(isrDataSets, opts);
// }

// var reset_canvas_isr = function() {
//     var newIsrCanvas = "<canvas id='isr-graph-container' height='250' width='834'></canvas>";
//     $("#inventory-sales-ratio").html("");
//     $("#inventory-sales-ratio").html(newIsrCanvas);
// }

// // Historical Data

// var redraw_canvas_hd = function(labels, data){
//     var newCanvas = "<canvas id='hd-graph-container' height='250' width='834'></canvas>";
//     $("#historical-data").html("");
//     $("#historical-data").html(newCanvas);

//     if ( cache_data1_hd == '' || cache_labels_hd == '' ) {

//         cache_labels_hd = labels;
//         cache_data1_hd  = data;

//         start_drawing(labels, data);
//     } else {
//         start_drawing(cache_labels_hd, cache_data1_hd);
//     }
// }

// var start_drawing = function(_labels, _data) {
//     var hdCtx  = $("#hd-graph-container").get(0).getContext("2d");
//     var dataSets = {
//         labels : _labels,
//         datasets : [
//             {
//                 fillColor : "rgba(66,139,202,0.5)",
//                 strokeColor : "rgba(66,139,202,0.9)",
//                 pointColor : "rgba(19,84,141,0.9)",
//                 pointStrokeColor : "#fff",
//                 pointHighlightFill: "rgba(66,139,202,1)",
//                 pointHighlightStroke: "rgba(220,220,220,1)",
//                 scaleShowGridLines : true,
//                 data : _data

//             }
//         ]
//     }

//     myNewChart = new Chart(hdCtx).Line(dataSets, opts);
// }

// var get_historical_data = function(duration, from, to) {

//     var hdLabels = new Array();
//     var hdData   = new Array();

//     $.get('/transactions/historical-data', {"id": current_line_id, "duration": duration, "from": from, "to": to}, function(hdDataReturn) {
//         var hd = hdDataReturn.data;
//         for(var i=0; i < hd.length; i++) {
//             hdLabels.push(hd[i].date);
//             hdData.push(hd[i].quantity);
//         }
//         redraw_canvas_hd(hdLabels, hdData);
//     });
// }

// $("#hd-btn").click(function(event) {
//     current_tab_id = "#hd-btn";
//     event.preventDefault();
//     get_historical_data(duration, from, to);
// });

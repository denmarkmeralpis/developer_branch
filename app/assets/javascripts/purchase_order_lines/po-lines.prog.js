var _ORIG_NA_;
var _dts, _ohq, _sq, _qtp, _qpu, _utp, _pp, _d, _st, _gt, _tid, _gtc, _od, _na, curr_subtotal, curr_grandtotal, curr_total_item_discount,new_grandtotal,
    new_total_item_discount,new_gross_total_cost, new_net_total, curr_gross_total, ordering_unit;

var roundNumber = function(number, decimal_points) {
    if(!decimal_points) return Math.round(number);
    if(number == 0) {
        var decimals = "";
        for(var i=0;i<decimal_points;i++) decimals += "0";
        return "0."+decimals;
    }

    var exponent = Math.pow(10,decimal_points);
    var num = Math.round((number * exponent)).toString();
    return num.slice(0,-1*decimal_points) + "." + num.slice(-1*decimal_points)
}

var temp = function(){
    _dts     = $('.dts-' + current_line_id).val();
    _ohq     = $('.ohq-' + current_line_id).val();
    _sq      = $('.sq-' + current_line_id).attr('data-value');
    _qtp     = $('.qtp-' + current_line_id).val();
    _qpu     = $('.qpu-' + current_line_id).val();
    _utp     = $('.utp-' + current_line_id).val();
    _pp      = $('.pp-' + current_line_id).val();
    _d       = $('.d-' + current_line_id).val();
    _st      = $('.subtotal-' + current_line_id).text();
    _gt      = $("#grand-total").text();
    _tid     = $("#total-item-discount").text();
    _gtc     = $("#gross-total-cost").text();
    _od      = $("#od").val();
    _na      = $("#net-amount").text()
    curr_gross_total = parseFloat($('#gross-total-cost').text().replace(/[^\d.-]/g, ''));
    curr_total_item_discount = parseFloat($('#total-item-discount').text().replace(/[^\d.-]/g, ''));
    curr_grandtotal = parseFloat($('#grand-total').text().replace(/[^\d.-]/g, ''));
}

var revert_back = function(){
    $('.dts-' + current_line_id).val(_dts);
    $('.ohq-' + current_line_id).val(_ohq);
    $('.sq-' + current_line_id).text(_sq);
    $('.qtp-' + current_line_id).val(_qtp);
    $('.qpu-' + current_line_id).val(_qpu);
    $('.utp-' + current_line_id).val(_utp);
    $('.pp-' + current_line_id).val(_pp);
    $('.d-' + current_line_id).val(_d);
    $('.subtotal-' + current_line_id).text(_st);
    $("#grand-total").text(_gt);
    $("#total-item-discount").text(_tid);
    $("#gross-total-cost").text(_gtc);
    $("#od").val(_od);
    $("#net-amount").text(_na);
    table.ajax.reload();
}

var input_disabler = function() {
    $(".input-po").attr('disabled', 'disabled');
    $(".input-po").css('border', '1px solid transparent');
}

var to_currency = function(amount) {
    var i = parseFloat(amount);
    if(isNaN(i)) { i = 0.00; }
    var minus = '';
    if(i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if(s.indexOf('.') < 0) { s += '.00'; }
    if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s;
    return s;
}

input_disabler();

var action_state      = 0;
var current_line_id   = 0;
var previous_clicked  = 0;

// Edit Action

$(".edit-action-btn").click(function(){
    if ( action_state == 0 )
    {
        var line_id  = $(this).attr("data-line-id");
        current_line_id = line_id;
        curr_subtotal = $(".subtotal-" + current_line_id).text().replace(/[^\d.-]/g, '');
        curr_grandtotal = $("#grand-total").text().replace(/[^\d.-]/g, '');
        curr_total_item_discount = parseFloat($('#total-item-discount').text().replace(/[^\d.-]/g, ''));
        action_state = 1;

        // Chage the action button style
        $('#delete-btn-' + line_id).css('visibility', 'visible');
        $('#edit-btn-' + line_id).css('display', 'none');
        $('#cancel-btn-' + line_id).css('display', 'block');
        $('#save-btn-' + line_id).css('display', 'block');
        $('#graph-btn-' + line_id).css('visibility', 'visible');
        $('#order-to-supplier-btn-' + line_id).css('visibility', 'visible');
        // Disable all the input fields & enable only the selected id
        input_disabler();
        $(".input-" + line_id).removeAttr('disabled');
        $(".input-" + line_id).css('border', '1px solid #6E7C87');


    }
    else
    {        
        swal("Oops...", "Please save your current work and try again!", "error");
        return;
    }
    temp();
});


// Save Action

$(".save-action-btn").click(function(){
    if ( action_state == 1 ) {

        var line_id = $(this).attr("data-line-id");
        var d       = $('.d-' + line_id).val();
        var sq      = $('.sq-' + line_id).text();//attr('data-value');
        var pp      = $('.pp-' + line_id).val();
        var ads     = $('.ads-' + line_id).attr('data-value');
        var dts     = $('.dts-' + line_id).val();
        var ohq     = $('.ohq-' + line_id).val();
        var qtp     = $('.qtp-' + line_id).val();
        var qpu     = $('.qpu-' + line_id).val();
        var utp     = $('.utp-' + line_id).val();
        var st      = $('.subtotal-' + line_id).text().replace(/[^\d.-]/g, '');
        var od      = $("#od").val();
        var cancellation_date = $("#cancellation_date").val();
        var target_date = $("#target_date").val();
        var params = { "po_line_id": line_id }


        if (utp == '0')
        {
            if (confirm("You ordered zero quantities. Do you wish to delete this line?") == true) 
            {
                $.post("/purchase-orders/" + id + "/del_po_line", params)
                        .done(function( data ) {
                            if ( data.result.toString() == 'success' ) {

                                $("#gross-total-cost").text(data.updated_totals.total_po_cost);
                                $("#total-item-discount").text(data.updated_totals.total_item_discount);
                                $("#grand-total").text(data.updated_totals.gross_amount);
                                $("#od").val(data.updated_totals.overall_discount);
                                $("#net-amount").text(data.updated_totals.grand_total);                 
                                
                                table.ajax.reload();
                            } else {
                                swal("Oops...", "Something went wrong. Try again!", "error");
                            }
                    });
            } 
            else 
            {
                alert("Enter value for UNITS TO PURCHASE");
            }
        }
        else
        {
            if (d == "")
            d = $('.d-' + line_id).val(0);
            if (utp == "")
                utp = $('.utp-' + line_id).val(0);
            if (pp == "")
                pp = $('.pp-' + line_id).val();

            //alert(qpu);

            action_state = 0;

            $('#edit-btn-' + line_id).css('display', 'block');
            $('#save-btn-' + line_id).css('display', 'none');
            $('#cancel-btn-' + line_id).css('display', 'none');
            $('#graph-btn-' + line_id).css('visibility', 'hidden');
            $('#delete-btn-' + line_id).css('visibility', 'hidden');
            $('#order-to-supplier-btn-' + line_id).css('visibility', 'hidden');

            var po_id = $("#gross-total-cost").attr('data-id');
            var newAttr = {
                "id": po_id,
                "po_line_id": line_id,
                "purchase_order": {
                    // "cancel_date": cancellation_date,
                    // "due_date": target_date,
                    "gross_amount": new_grandtotal,
                    "total_po_cost": new_gross_total_cost,
                    "overall_discount": od,
                    "total_item_discount": new_total_item_discount,
                    "grand_total": new_net_total,
                    "purchase_order_lines_attributes": {
                        "id": line_id,
                        "suggested_quantity": sq,
                        "adjusted_average_daily_sales": ads,
                        "stock_keeping_days": dts,
                        "onhand_quantity": ohq,
                        "quantity_to_purchase": qtp,
                        "ordering_unit": ordering_unit,
                        "quantity_per_unit": qpu,
                        "units_to_purchase": utp,
                        "purchase_price": pp,
                        "discount_text": d,
                        "subtotal": st
                    }
                }
            }

            $.post('/purchase-orders/update-line-items', newAttr, function(data){
                console.log(data);
                if(data['result'] == 'ok') {

                    var updated_obj = data['po_lines'];

                    $('.pp-' + line_id).val(updated_obj['purchase_price']);
                    $('.dts-' + line_id).val(updated_obj['stock_keeping_days']);
                    $('.ohq-' + line_id).val(updated_obj['onhand_quantity']);
                    $('.qtp-' + line_id).val(updated_obj['quantity_to_purchase']);
                    $('.qpu-' + line_id).val(updated_obj['quantity_per_unit']);
                    $('.utp-' + line_id).val(updated_obj['units_to_purchase']);
                    $('.d-' + line_id).val(updated_obj['discount_text']);
                    $('.ads-' + line_id).val(updated_obj['average_daily_sales']);
                    $('.subtotal-' + line_id).text("P " + updated_obj['subtotal']);
                    input_disabler();
                } else {                
                    swal("Oops...", data['message'], "error");
                    revert_back();
                    input_disabler();
                }
            });
            table.ajax.reload();
        }
    }
});

// Cancel Action

$(".cancel-action-btn").click(function(){
    input_disabler();
    action_state = 0;
    $('#edit-btn-' + current_line_id).css('display', 'block');
    $('#save-btn-' + current_line_id).css('display', 'none');
    $('#cancel-btn-' + current_line_id).css('display', 'none');
    $('#graph-btn-' + current_line_id).css('visibility', 'hidden');
    $('#delete-btn-' + current_line_id).css('visibility', 'hidden');
    $('#order-to-supplier-btn-' + current_line_id).css('visibility', 'hidden');

    revert_back();
});

// Add bind to qty to purchase for realtime update

var refresher = function(){

    qpu = $(".qpu-" + current_line_id).val();
    qtp = $(".qtp-" + current_line_id).val();
    pp  = $(".pp-" + current_line_id).val();
     d   = $('.d-' + current_line_id).val();

    utp = qtp / qpu;
    utp = Math.ceil(utp);

    // QTP
    if ( previous_clicked == 0 ) {
        $(".utp-" + current_line_id).val(utp);
        $(".qtp" + current_line_id).val(qtp);
    }
    // UTP
    else {
        utp = $(".utp-" + current_line_id).val();
        if (utp == "")
            utp = 0;
        qtp = utp * qpu;
        $(".qtp-" + current_line_id).val(qtp);
    }

    //compute new gross total cost
    var x = parseFloat(_utp) * parseFloat(_pp);
    var y = parseFloat(utp) * parseFloat(pp);
    var diff = x - y;
    new_gross_total_cost = curr_gross_total - diff;
    $("#gross-total-cost").text(to_currency(new_gross_total_cost));

    //compute new total item discount
    var old_dtxt = _d;
    var new_dtxt = d;

    var old_discount = old_dtxt.split(',');
    var new_discount = new_dtxt.split(',');
    var old_item_discount = 0;
    var new_item_discount = 0;
    var delimiter; 

    if (d == '')
    {
        $('.d-' + current_line_id).val(0);
        $('.subtotal-' + current_line_id).text("P " + to_currency(y));
        new_total_item_discount = curr_subtotal - y;
        diff2 = curr_total_item_discount + new_total_item_discount;
        $("#total-item-discount").text(to_currency( diff2 ));
        $("#grand-total").text(to_currency( new_gross_total_cost + diff2));
        update_grand_total_discount();
    }



    var_x = x;

    for (var i = 0; i < old_discount.length; i++) {
        delimiter = old_discount[i].charAt(old_discount[i].length - 1);
        if (delimiter == '%')
        {
            old_item_discount += var_x * (parseFloat(old_discount[i]) / 100);
            var_x  = var_x  - (var_x  * (parseFloat(new_discount[i]) / 100))
        }
        else if (delimiter != '%')
            old_item_discount += parseFloat(old_discount[i]);
    }

    var_y = y;

    for (var i = 0; i < new_discount.length; i++) {
        delimiter = new_discount[i].charAt(new_discount[i].length - 1);
        if (delimiter == '%')
        {
            new_item_discount += var_y * (parseFloat(new_discount[i]) / 100);
            var_y = var_y - (var_y * (parseFloat(new_discount[i]) / 100))
        }
        else if (delimiter != '%')
        {
            new_item_discount += parseFloat(new_discount[i]);
            var_y -= parseFloat(new_discount[i]);
        }
    }


    new_total_item_discount = curr_total_item_discount - (old_item_discount - new_item_discount);
    $("#total-item-discount").text(to_currency(new_total_item_discount));
    $('.subtotal-' + current_line_id).text("P " + roundNumber(to_currency(var_y), 2));
    new_grandtotal = curr_grandtotal - (var_x - var_y);
    $("#grand-total").text(to_currency(new_grandtotal));
    update_grand_total_discount();
}

$(".qtp").keyup(function(){
    previous_clicked = 0;
    refresher();
});

$(".qpu").change(function(){
    var cost = $('option:selected', this).attr('cost');
    ordering_unit = $('option:selected', this).attr('unit_name');
    $(".pp-" + current_line_id).val(cost);
    refresher();
});

$(".utp").keyup(function(){
    previous_clicked = 1;
    refresher();
});

$(".pp").keyup(function(){
    refresher();
})

var sq_updater = function(ohq){
    previous_clicked = 0;
    dts = parseFloat($('.dts-' + current_line_id).val());
    ads = parseFloat($('.ads-' + current_line_id).text());

    sq = (dts * ads) - ohq;

    if (sq < 0)
        sq = 0;

    $(".sq-" + current_line_id).text(sq);
    $(".qtp-" + current_line_id).val(sq)

    refresher();
}

$(".dts").keyup(function(){
    ohq = $(".ohq-" + current_line_id).val();
    sq_updater(ohq);
});

$(".ohq").keyup(function(){
    ohq = $(this).val();
    sq_updater(ohq);
});

// $(".d").keyup(function(){
//     refresher();
// });

$(".d").keyup(function() {
    var _OLD_GTC = parseFloat(curr_gross_total);
    var _OLD_GT  = parseFloat(curr_grandtotal);
    var _OLD_TID = parseFloat(curr_total_item_discount); 

    var _UNITS_TO_PURCHASE = $(".utp-" + current_line_id).val();
    var _PURCHASE_PRICE    = $(".pp-" + current_line_id).val();    
    var _WITHOUT_DISCOUNT_SUBTOTAL = (_UNITS_TO_PURCHASE * _PURCHASE_PRICE);    
    var _INPUT_DISCOUNT = $(this).val();
    var _WITH_DISCOUNT_SUBTOTAL = _WITHOUT_DISCOUNT_SUBTOTAL;
    var _DS;    
    var _TYPE;
    var _OLD_LINE_ITEM_DISCOUNT = _d;
    var _CURRENT_DISCOUNT  = _INPUT_DISCOUNT.split(",");    
    
    _OLD_LINE_ITEM_DISCOUNT = _OLD_LINE_ITEM_DISCOUNT.split(",");
    _CURRENT_ITEM_DISCOUNT  = _WITHOUT_DISCOUNT_SUBTOTAL;    

    for(var k=0; k < _OLD_LINE_ITEM_DISCOUNT.length; ++k) {
        _DSS = _OLD_LINE_ITEM_DISCOUNT[k].trim().split("%");
        _TYPEE = _OLD_LINE_ITEM_DISCOUNT[k].trim();
        if(_TYPEE.indexOf('%') == -1) {
            _CURRENT_ITEM_DISCOUNT -= parseFloat(_DSS[0]);
        } else {
            _CURRENT_ITEM_DISCOUNT -= _CURRENT_ITEM_DISCOUNT * (parseFloat(_DSS[0]) / 100)
        }
    }

    var __OLD_ITEM_DISCOUNT = _WITHOUT_DISCOUNT_SUBTOTAL - _CURRENT_ITEM_DISCOUNT;

    if ( _INPUT_DISCOUNT == "" ) {
        _INPUT_DISCOUNT = "0";
    }

    // Catching errors!
    try 
    {
        for( var i = 0; i < _CURRENT_DISCOUNT.length; i++ ) {
            _DS = _CURRENT_DISCOUNT[i].trim().split("%");            
            _TYPE = _CURRENT_DISCOUNT[i].trim();            
            // check discount type [percentage or not]            
            if(_TYPE.indexOf('%') == -1) {                
                _WITH_DISCOUNT_SUBTOTAL -= parseFloat(_DS[0]);                
            } else {                
                _WITH_DISCOUNT_SUBTOTAL -= _WITH_DISCOUNT_SUBTOTAL * (parseFloat(_DS[0]) / 100);
            }
        }

        // Output the subtotal
        if ( isNaN(_WITH_DISCOUNT_SUBTOTAL) ) 
            $(".subtotal-" + current_line_id).text("Invalid discount!");
        else 
            $(".subtotal-" + current_line_id).text("P " + roundNumber(_WITH_DISCOUNT_SUBTOTAL, 2).toString());
    } 
    catch(err)
    {
        $(".subtotal-" + current_line_id).text("Invalid discount!");
    }    

    var _TOTAL_ITEM_DISCOUNT     = _WITHOUT_DISCOUNT_SUBTOTAL - _WITH_DISCOUNT_SUBTOTAL;
    var _NEW_TOTAL_ITEM_DISCOUNT = (parseFloat(_tid.replace(/[^\d.-]/g, '')) -  __OLD_ITEM_DISCOUNT) + _TOTAL_ITEM_DISCOUNT;
    var _NEW_GRAND_TOTAL         = _OLD_GTC - _NEW_TOTAL_ITEM_DISCOUNT;

    var _OLD_OD = $("#od").val().replace(/[^\d.-]/g, '');
    _OLD_OD = parseFloat(_OLD_OD);

    var _NEW_NET_TOTAL = (_OLD_GTC - (_NEW_TOTAL_ITEM_DISCOUNT + _OLD_OD));

    update_totals(_NEW_TOTAL_ITEM_DISCOUNT, _NEW_GRAND_TOTAL, _NEW_NET_TOTAL);

    $("#supplier-support-discount").keyup();

});

// update totals

var update_totals = function(_new_total_item_discount, _new_grand_total, _new_net_total) {    
    
    $("#net-amount").text("P " + roundNumber(_new_net_total, 2));
    $("#grand-total").text("P " + roundNumber(_new_grand_total, 2));
    $("#total-item-discount").text("P " + roundNumber(_new_total_item_discount, 2));

    new_grandtotal = _new_grand_total;
    new_total_item_discount = _new_total_item_discount;
    new_net_total = _new_net_total;

}

$("#od").keyup(function(){
    update_grand_total_discount();
});

var update_grand_total_discount = function(){
    var _grand_total = $("#grand-total").text();
    var grand_total = _grand_total.replace(/[^\d.-]/g, '');


    over_all_ds = $("#od").val();
    over_all_ds = over_all_ds.split(',');

    if (over_all_ds == '') {
        $("#od").val(0);
        $("#net-amount").text("P " + to_currency(grand_total));
        return;
    }

    for (var i=0; i < over_all_ds.length; i++) {
        dl = over_all_ds[i].charAt(over_all_ds[i].length - 1);
        if (dl == '%')
            grand_total -=  grand_total * (parseFloat(over_all_ds[i]) / 100);
        else if (dl != '%')
            grand_total -= parseFloat(over_all_ds[i]);
    }

    $("#net-amount").text("P " + to_currency(grand_total));
    new_net_total = grand_total;
}



// Remarks

$("#save-remarks").click(function(){
    var textarea = $("#remarks-textarea");
    $.post('add-remarks/', { "remark": textarea.val() }, function(data) {
        if (data["result"] == 'ok')
        {
            textarea.val(data['remarks']);
            $("#rem").text(textarea.val());
            $("#dismiss").click();
            $("#dismiss").click();
        }
        else
        {
            swal("Oops...", "Something went wrong. Try again!", "error");
        }
    });
});

// Void
$("#voidPo").click(function(event){
    event.preventDefault();
    var href = $(this).attr('href');
    var void_reason = prompt("Please enter your void reason:", "Wrong PO");
    if ( void_reason )
        $(this).attr('href', href + "&void_reason=" + void_reason);
    else
        return false;
});

$(".del-po-line").click(function(){
    var line_id  = $(this).attr("data-line-id");
    var params = { "po_line_id": line_id }
    var confirmText = "Are you sure?";

    swal({   
        title: confirmText,   
        text: "Your will not be able to recover or undo this action!",   

        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes, delete it!" },  
        function(){   
            $.post("/purchase-orders/" + id + "/del_po_line", params)
                .done(function( data ) {
                    if ( data.result.toString() == 'success' ) {

                        $("#gross-total-cost").text(data.updated_totals.total_po_cost);
                        $("#total-item-discount").text(data.updated_totals.total_item_discount);
                        $("#grand-total").text(data.updated_totals.gross_amount);
                        $("#od").val(data.updated_totals.overall_discount);
                        $("#net-amount").text(data.updated_totals.grand_total);                 
                        
                        table.ajax.reload();
                    } else {
                        swal("Oops...", "Something went wrong. Try again!", "error");
                    }
            });
        }
    ); 
});

$("#supplier-support-discount").keyup(function() {
    
    var ____GRAND_TOTAL = $("#grand-total").text().replace(/[^\d.-]/g, '');
    var ____OVERALL_DS  = $("#od").val();
    var SUPPORT_DISCOUNT 

    ____OVERALL_DS = ____OVERALL_DS.split(",");
    // ds

    for( var x=0; x < ____OVERALL_DS.length; x++ ) {
        ____OD = ____OVERALL_DS[x].trim().split('%');
        ____TY = ____OVERALL_DS[x].trim();

        if ( ____TY.indexOf('%') == -1 ) {
            ____GRAND_TOTAL -= parseFloat(____OD[0]);        
        } else {
            ____GRAND_TOTAL -= (____GRAND_TOTAL * parseFloat(____OD[0] / 100));
        }
    }

    var _net_amount      = ____GRAND_TOTAL;
    var SUPPORT_DISCOUNT = $(this).val();
    SUPPORT_DISCOUNT     = SUPPORT_DISCOUNT.split(",");

    for( var c=0; c < SUPPORT_DISCOUNT.length; ++c ) {
        _SD = SUPPORT_DISCOUNT[c].trim().split("%");
        _TP = SUPPORT_DISCOUNT[c].trim()

        if( _TP.indexOf('%') == -1 ) {
            _net_amount -= parseFloat(_SD[0]);
        } else {
            _net_amount -= _net_amount * (parseFloat(_SD[0]) / 100);
        }
    }    

    if ( isNaN(_net_amount) ) 
        $("#net-amount").text("Invalid discount!");
    else 
        $("#net-amount").text("P " + roundNumber(_net_amount, 2).toString());    
});


$(document).ready(function () {        
    $("#supplier-support-discount").keyup();
});







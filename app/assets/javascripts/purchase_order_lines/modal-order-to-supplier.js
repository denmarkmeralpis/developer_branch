var global_id;
var global_product_id;

$(".order-to-supplier-btn").click(function() {		
	global_id = $(this).attr("data-line-id");	
	$(this).button('loading');
	$.get("/purchase-order-lines/order-to-suppliers", { "purchase_order_line_id": global_id }, function(suppliers) {
		if (suppliers.length == 0) 
		{
			$("#order-to-supplier-btn-" + global_id).button('reset');
			swal("No Supplier found!", "No alternate supplier for this product!", "error");
			return;			
		} else 
		{
			$("#order-to-supplier-supp-id").find("option").remove();
			var opts = "";
			
			for(var i=0; i < suppliers.length; i++) {
				opts += "<option value='" + suppliers[i].supplier_id + "'>";
				opts += 		suppliers[i].name;
				opts += "</option>";
			}			
			$("#order-to-supplier-supp-id	").append(opts);
			$("#order-to-supplier-modal").modal("show");
			$("#order-to-supplier-btn-" + global_id).button('reset');
		}		
	});	
});

$("#confirm-transfer-button").click(function(){
	
	var supplier_id = $("#order-to-supplier-supp-id").val();

	$.post("/purchase-order-lines/transfer-to-supplier", { "purchase_order_line_id": global_id, "supplier_id": supplier_id }, function(data) {

		global_product_id = data.product_id;

		if ( data.result.toString() == "ok" ) {			
			
			swal("Successful Request!", "Purchase Order sent to" + data.supplier_name, "success");

			$("#grand-total").text(data.po.gross_amount);
		   $("#total-item-discount").text(data.po.total_item_discount);
		   $("#gross-total-cost").text(data.po.total_po_cost);
		   $("#od").val(data.po.overall_discount);
		   $("#net-amount").text(data.po.grand_total);

		 	table.ajax.reload();

		} 
		else if ( data.result.toString() == "choose" ) {

			var po = data.pos;
			var options = "";

			// Reinit select form
			$("#purchase-order-select").find("option").remove();

			// create options on select form based on return
			for( var i = 0; i < po.length; i++ ) {
				options += "<option value='" + po[i].id + "'>";
				options += 		po[i].reference;
				options += "</option>";
			}

			// appending on the select form
			$("#purchase-order-select").append(options);

			// open modal
			$("#choose-purchase-order-modal").modal("show");
		}
		else {
			swal("Unable to process...", "Something is not right with the process", "error");
			console.log(data);
		}

	});

});

// /commit-transfer-to-supplier

// listener for po destination modal
$("#confirm-order-select").click(function(){
	var purchase_order_id = $("#purchase-order-select").val();	
	var params = { 
		purchase_order_id: purchase_order_id,
		orderline: global_id,
		product_id: global_product_id
	}
	$.ajax({
	  type: "POST",
	  url: "/purchase-order-lines/commit-transfer-to-supplier",
	  data: params
	})
	  .done(function( msg ) {
	    if ( msg.result.toString() == 'ok' ) {
	    	table.ajax.reload();
	    	swal("Successful Request!", "Purchase Order sent to" + msg.supplier_name, "success");
	    } else {
	    	swal("Unable to process...", "Something is not right with the process", "error");	
	    }
	  });
});




var overall_discount, net_amount;

var init_totals = function(){
	overall_discount 	= encodeURI($("#od").val());
	net_amount       	= $("#net-amount").text().replace(/[^\d.-]/g, '');
	cancellation_date = $("#cancellation_date").val();
   target_date 		= $("#target_date").val();
   supplier_terms    = encodeURI($("#supplier-terms").val());
   supplier_support_ds = encodeURI($("#supplier-support-discount").val());

	return ("&overall_discount=" + overall_discount + "&net_amount=" + net_amount + "&cancel_date=" + cancellation_date + "&target_date=" + target_date + "&supplier_support_discount=" + supplier_support_ds + "&supplier_terms=" + supplier_terms);
}

$("#approvePo").click(function(){		
	append_url = init_totals();
	swal({   
		title: "Approve Purchase Order?",   
		text: "This action cannot be undo.",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, Approve it!",   
		closeOnConfirm: false }, 
		function(){
			$.post("po-line-commit?status=A" + append_url, function(returnX){
							
				if(returnX.result == "ok") 
				{
					document.location.href = "/dashboards";
				} 
				else {
					swal("Oops...", returnX.message, "error");
				}
			});		
		}
	);
});

$("#savePo").click(function(){		
	append_url = init_totals();
	var status = $(this).attr("data-status");
	swal({   
		title: "Save Purchase Order?",   
		text: "This action cannot be undone.",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, Save it!",   
		closeOnConfirm: false }, 
		function(){			
			$.post("po-line-commit?status=" + status + append_url, function(returnX){
							
				if(returnX.result == "ok") 
				{
					document.location.href = "/dashboards";
				} 
				else {
					swal("Oops...", returnX.message, "error");
				}
				

			});		
		}
	);
});

$("#confirmPo").click(function() {
	append_url = init_totals();		
	swal({   
		title: "Confirm Purchase Order?",   
		text: "This action cannot be undone.",   
		type: "warning",   
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, mark it as Confirmed PO!",   
		closeOnConfirm: false }, 
		function(){
			$.post("po-line-commit?status=C" + append_url, function(returnX){
				if(returnX.result == "ok") 
				{
					document.location.href = "/dashboards";
				} 
				else 
				{
					swal("Oops...", returnX.message, "error");
				}
			});		
		}
	);
});



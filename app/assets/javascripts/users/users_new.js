$(document).ready(function() { 
    var value = $("#user_types").val();
    if (value == 'buyer')
	{
	  $("#rpo").attr('checked','checked');
	  $("#supervisor").css('display', 'block');
	}
	else
	{
	  $("#supervisor").css('display', 'none');
	  $("#rpo").attr('checked','checked');
	  $("#apo").attr('checked','checked');
	  $("#cpo").attr('checked','checked');
	  $("#epo").attr('checked','checked');
	  $("#ssu").attr('checked','checked');
	  $("#spr").attr('checked','checked');
	}
 });

$("#user_types").change(function(){
 	var value = $(this).val();
	if (value == 'buyer')
	{
	  $("#rpo").attr('checked','checked');
	  $("#apo").removeAttr('checked');
	  $("#cpo").removeAttr('checked');
	  $("#epo").removeAttr('checked');
	  $("#ssu").removeAttr('checked');
	  $("#spr").removeAttr('checked');
	  $("#supervisor").css('display', 'block');
	}
	else
	{
	  $("#supervisor").css('display', 'none');
	  $("#rpo").attr('checked','checked');
	  $("#apo").attr('checked','checked');
	  $("#cpo").attr('checked','checked');
	  $("#epo").attr('checked','checked');
	  $("#ssu").attr('checked','checked');
	  $("#spr").attr('checked','checked');
	}
});


$(document).ready(function() {
    $("#rpo").change(function() {
        if($(this).is(":checked")) 
      	  $("#apo").attr("disabled", true);
        else
        {
          $("#apo").removeAttr("disabled");  
          $("#rpo").attr("disabled", true);  
        }        
    });

     $("#apo").change(function() {
        if($(this).is(":checked")) 
      	  $("#rpo").attr("disabled", true);
        else
        {
          $("#rpo").removeAttr("disabled");  
          $("#apo").attr("disabled", true);  
        }        
    });
}); 




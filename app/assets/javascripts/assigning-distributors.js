var global_supplier_id;

$(".assign-btn").click(function(){
  var supplier_id = $(this).attr('data-supplier-id');
  params = { "supplier_id": supplier_id}
  $.post("/suppliers/" + supplier_id_params + "/assigner", params)
   .done(function( data ) {
      if ( data == 'success' ) {
        table.ajax.reload();
        table2.ajax.reload();
      } else {        
        swal("Oops...", "Failed to load. Please try again!", "error");
      }
   }); 
});

$(".unassign-btn").click(function(){  
  var supplier_id = $(this).attr('data-supplier-id');

  params = { "supplier_id": supplier_id}
  $.post("/suppliers/" + supplier_id_params + "/unassigner", params)
   .done(function( data ) {
      if ( data == 'success' ) {
        table.ajax.reload();
        table2.ajax.reload();
      } else {
        swal("Oops...", "Failed to load. Please try again!", "error");
      }
   }); 
});

$(".assign-btn-b").click(function(){  

  $("#supplier_id").val($(this).attr('data-supplier-id'));
  $("#view").val($(this).attr('view'));
  $('#twiceamonth-schedule-type').css('display', 'none');
  $('#monthly-schedule-type').css('display', 'none');
  $("#schedule-type").val('weekly');
  $("#day-schedule-type").val('monday');
  $("#twiceamonth-schedule-type").val('week1_week3');
  $("#monthly-schedule-type").val('week1');
});

$(".unassign-btn-b").click(function(){

    $(this).button('loading');  

    $(this).css("background-color", "#428bca");
    $(this).css("border-color", "#357ebd");

    var supplierId = $(this).attr('data-supplier-id');
    var params = { "supplier_id": supplierId }
    $.post("/branches/" + branch_id_params + "/unassigner", params)
     .done(function( data ) {
        if ( data == 'success' ) {
            table.ajax.reload();
            table2.ajax.reload();
        } else {
          swal("Oops...", "Failed to load. Please try again!", "error");
        }
     });
  });


$(".sched-btn").click(function(){

  if ($(this).attr('frequency_type') == 'weekly')
  {
    $('#twiceamonth-schedule-type').css('display', 'none');
    $('#monthly-schedule-type').css('display', 'none');
  }
  else if ( $(this).attr('frequency_type') == 'monthly' )
  {
    $("#monthly-schedule-type").css('display', 'block');
    $('#twiceamonth-schedule-type').css('display', 'none');
  }
  else if ($(this).attr('frequency_type') == 'twice_a_month')
  {
    $('#twiceamonth-schedule-type').css('display', 'block');
    $('#monthly-schedule-type').css('display', 'none');
  }

  $("#sched_id").val($(this).attr('sched_id'));
  $("#supplier_id").val($(this).attr('data-supplier-id'));
  $("#schedule-type").val($(this).attr('frequency_type'));
  $("#monthly-schedule-type").val($(this).attr('frequency_week'));
  $("#twiceamonth-schedule-type").val($(this).attr('frequency_week'));
  $("#day-schedule-type").val($(this).attr('frequency_day'));
  $("#view").val($(this).attr('view'));

});

$(".assign-btn-u").click(function() {
  supplierId = $(this).attr('data-supplier-id');
  global_supplier_id = supplierId;
  $("#load-product-categories").load("/suppliers/get_product_categories?id=" + supplierId, function(data){
    $('#assign-supplier').modal('show');
  });
});

$(".unassign-btn-u").click(function(){
  var supplierId = $(this).attr('data-supplier-id');
  var params = { "supplier_id": supplierId }

  var unassign_btn = $("#unassign-btn-" + supplierId);
  unassign_btn.button("loading");

  unassign_btn.css("background-color", "#428bca");
  unassign_btn.css("border-color", "#357ebd");


  $.post("/users/" + user_id_params + "/unassigner", params)
   .done(function( data ) {
      if ( data == 'success' ) {
        table.ajax.reload();
        table2.ajax.reload();
      } else {
        swal("Oops...", "Failed to load. Please try again!", "error");
      }
   });
});
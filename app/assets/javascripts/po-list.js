$('#purchase_orders').dataTable({
	bPaginate: paginate,
    sPaginationType: "full_numbers",
    bProcessing: true,
    bServerSide: true,
    sAjaxSource: $('#purchase_orders').data('source'),
      aoColumnDefs: [{'bSortable': false, 'aTargets': [ 0, 1,  3, 4, 5 , 6]}]
});


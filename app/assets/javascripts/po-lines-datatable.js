//var col = $('#review').html().trim();
var col = $("#review").attr('data-review-po');
var status = $("#review").attr('data-status');
var id = $("#review").attr('data-attr-id');
var line_count = $("#review").attr('po-lines');


if (col == "false" || status == 'A' || status == 'V') {
    var column = [
        { 'bSortable': false, 'aTargets': [ 0, 2, 3, 4, 5 ]
        }
    ]
}
else
{
    var column = [
        { 'bSortable': false, 'aTargets': [ 0, 2, 3, 4, 5 ] }
    ]
}

if (parseInt(line_count) > 10)
    paginate = true;
else
    paginate = false;

var table = $('#po-lines').DataTable({
    bPaginate: paginate,
    sPaginationType: "full_numbers",
    bProcessing: true,
    bServerSide: true,
    sAjaxSource: $('#po-lines').data('source'),
    aoColumnDefs: column
});  


$("#add-po-line").click(function(){
    $("#get_prod_to_add").load("/purchase-orders/" + id + "/get_prod_to_add", function(data){
        $("#add-line-modal").modal("show");
        console.log(data);
    });
});











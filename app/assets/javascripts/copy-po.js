var supplier_id = '';
var po_id = '';

$('.copy-po').click(function(){
    supplier_id = $(this).attr("data-supplier-id");
    branch_id = $(this).attr("data-branch-id");
    po_id =  $(this).attr("data-po-id");

    var html_content = "";

    $('#copy-po-modal').modal("show");

    $.get("/branches/get_branches_of_supplier", {"id": supplier_id, "branch_id": branch_id}, function(return_data){

        if (return_data.length < 1)
            html_content += "<div>No available branches.</div>";
        else
        {
            html_content += "<select id='getbranchselected' class='form-control'>";

            for(var i = 0; i < return_data.length; i++ ) {
                html_content += "<option value='" + return_data[i]['id'] + "'>" + return_data[i]['name'] + "</option>";
            }

            html_content += "</select>";

        }

        $("#load-branches").html(html_content);
    });
});


$("#trigger-copy-po").click(function(){
  var branch_id = $('#getbranchselected').val();
  var params = { "supplier_id": supplier_id, "purchase_order_id": po_id, "branch_id": branch_id }
  $.post("/purchase-orders/copy_po", params)
      .done(function( data ) {
          if ( data == 'success' ) {
              $('#copy-po-modal').modal("hide");              
              swal("Success!", "Purchase Order successully copied!", "success");
              location.reload();
          } else {
              $('#copy-po-modal').modal("hide");
              swal("Oops...", "No products to copy!", "error");
          }
      });
});


var po_count;
var new_po_count;

$("#schedule-type").change(function(){
    var value = $(this).val();
    if (value == 'weekly')
    {
        $('#twiceamonth-schedule-type').css('display', 'none');
        $('#monthly-schedule-type').css('display', 'none');
    }
    else if ( value == 'monthly' )
    {
        $("#monthly-schedule-type").css('display', 'block');
        $('#twiceamonth-schedule-type').css('display', 'none');
    }
    else if (value == 'twice_a_month')
    {
        $('#twiceamonth-schedule-type').css('display', 'block');
        $('#monthly-schedule-type').css('display', 'none');
    }
});

var supplierId; 
var json;

$("#close-btn").click(function(){
    var getVal = $("#schedule-type").val();
    supplierId = $("#supplier_id").val();
    var frequencyDay = $("#day-schedule-type").val();
    var frequencyTwiceAMonth = $("#twiceamonth-schedule-type").val();
    var frequencyMonthly = $("#monthly-schedule-type").val();
    var params;    

    if (getVal == 'weekly') {
        params = {
            "assign_params": {
                "supplier_id": supplierId,
                "frequency_type": getVal,
                "frequency_day": frequencyDay
            }
            , "update":   $('#view').val()
            , "sched_id": $('#sched_id').val()
            , "supplier_id": supplierId
        }
    } else if ( getVal == 'monthly' ) {
        params = {
            "assign_params": {
                "supplier_id": supplierId,
                "frequency_type": getVal,
                "frequency_week": frequencyMonthly,
                "frequency_day": frequencyDay
            }
            , "update": $('#view').val()
            , "sched_id": $('#sched_id').val()
            , "supplier_id": supplierId
        }
    } else {
        params = {
            "assign_params": {
                "supplier_id": supplierId,
                "frequency_type": getVal,
                "frequency_week": frequencyTwiceAMonth,
                "frequency_day": frequencyDay,
            }
            , "update": $('#view').val()
            , "sched_id": $('#sched_id').val()
            , "supplier_id": supplierId
        }
    }

    var btn = $("#assign-btn-" + supplierId);

    btn.button("loading");
    btn.css("background-color", "#f0ad4e");
    btn.css("border-color", "#eea236");

    $.post( " /branches/" + branch_id_params + "/assigner", params)
        .done(function( data ) {
            if ( data == 'success' )
            {
                table.ajax.reload();
                table2.ajax.reload();
                $("#load-products").load( "/branches/" + branch_id_params + "/get_products_duplicate_assign?supplier_id=" + supplierId, function(data){
                    if (json != '[]')
                        $("#assign-supp-modal").modal('show');
                });   
            }
        }
    );
});

$("#assign-supp-btn").on( 'click', function () { 
    var products = $('#product-list').val();
    var params = { "supplier_id": supplierId, "product_ids": products }
    $.post(" /branches/" + branch_id_params + "/assign_default_supplier", params)
      .done(function(data){
        if ( data == 'success' ) {          
          swal("Good job!", "Successful Assignment!", "success");
        } else {
          swal("Oops...", "Assignment Failed!", "error");          
        }
    });
});


//DATATABLE

var col = $('#review h2').html().trim();
if (col == "false") {
    var column = [
        {
            'bSortable': false, 'aTargets': [ 0, 1, 3, 4, 5 , 6],
            'bVisible': false, 'aTargets': [6]
        }
    ]
}
else{
    var column = [
        {
            'bSortable': false, 'aTargets': [ 0, 1,  3, 4, 5 , 6]
        }
    ]
}



$("#manual-po-form").on('submit', function( event ){

    // Add header notice    

    event.preventDefault();
    var params   = $(this).serializeArray();

    new_po_count = $("#purchase_orders").DataTable();    
    new_po_count = new_po_count.context[0].fnRecordsTotal();

    if ( params.length == 0 ) {
        swal("Oops...", "Unable to process request.", "error");
    } else {
        $.post('/purchase-orders/manual-po', { "suppliers": params, "branch_id": branch_id_params }, function(data) {
            if (data['result'] == 'processing'){

                $("#po-status-label").text("Computation in progress...");

                swal("Success!", "Purchase Order sent! We will send you an email once done!", "success");
            }                                
            else {
                swal("Oops...", "Unable to process request. Please try again in a while.", "error");
            }
            $("#manual-po-modal").modal('hide');
            // location.reload();
        });        
    }

});
var table = $('#pending-distributors').DataTable({
    sPaginationType: "full_numbers",
    bProcessing: true,
    bServerSide: true,
    sAjaxSource: $('#pending-distributors').data('source')
});

var type = $("#review").attr('data-owner-type');
if (type != "owner") {
    var column = [{ 'bVisible': false, 'aTargets': [4]}];
}
else
    var column = [];

var table2 = $('#assigned-distributors').DataTable({
    sPaginationType: "full_numbers",
    bProcessing: true,
    bServerSide: true,
    sAjaxSource: $('#assigned-distributors').data('source'),
    aoColumnDefs: column
});

var line_count = $("#review").attr('po-count');
if (parseInt(line_count) > 10)
    paginate = true;
else
    paginate = false;

var po_datatableX = $('#purchase_orders').DataTable({
    bPaginate: paginate,
    sPaginationType: "full_numbers",
    bProcessing: true,
    bServerSide: true,
    sAjaxSource: $('#purchase_orders').data('source'),
    aoColumnDefs: column    
});



$("#sp_tab").click(function(){    
    document.location.href = "#suppliers_tab";
    table2.ajax.reload();
});

$("#po_tab").click(function(){    
    document.location.href = "#purchase_orders_tab";    
    
    po_datatableX.ajax.reload();    

    if (new_po_count != po_datatableX.context[0].fnRecordsTotal()) {
        $("#po-status-label").text("");
    } 

});

$(document).ready(function(){
    var hash = window.location.hash;
    if ( hash != "" ) {
        $(hash.toString() + "_select").click();
    }
});
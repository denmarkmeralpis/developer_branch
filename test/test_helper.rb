def aax(n)
  time_start = Time.now
  abc = ActiveRecord::Base.connection
  inserts = []
  n.times do
    inserts.push "(1, 'aaa', 'a1', NOW(), NOW())"
  end
  sql = "INSERT INTO product_categories (`account_id`, `name`, `code`, `created_at`, `updated_at`) VALUES #{inserts.join(", ")}"
  abc.execute(sql)
  Time.now - time_start
end

# aax(10000)

def t(n)
  columns = [:score, :node_id, :user_id]
  values = []
  TIMES.times do
    values.push [3, 2, 1]
  end

  UserNodeScore.import columns, values
end

def test2
  user = User.find(4)
  branch = user.branches.find(2)
  user.suppliers.joins(:branches).where("branch_supplier_configurations.branch_id = ?", branch.id)
end
# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141029062049) do

  create_table "account_configurations", :force => true do |t|
    t.integer  "account_id"
    t.string   "value",      :default => ""
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "accounts", :force => true do |t|
    t.string   "name"
    t.string   "subdomain"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.datetime "last_data_index"
    t.string   "index_status",    :default => "done"
  end

  create_table "api_keys", :force => true do |t|
    t.integer  "account_id"
    t.string   "access_token", :null => false
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "branch_products", :force => true do |t|
    t.string   "branch_id",                                                                          :null => false
    t.string   "product_id",                                                                         :null => false
    t.string   "supplier_id"
    t.integer  "account_id"
    t.decimal  "cost",                               :precision => 10, :scale => 0
    t.string   "discount_text"
    t.string   "status",                :limit => 1,                                :default => "A"
    t.datetime "created_at",                                                                         :null => false
    t.datetime "updated_at",                                                                         :null => false
    t.decimal  "incoming_quantity",                  :precision => 10, :scale => 0, :default => 0
    t.datetime "last_arrival_quantity"
    t.datetime "last_arrival_date"
  end

  add_index "branch_products", ["branch_id", "supplier_id"], :name => "index_branch_products_on_branch_id_and_supplier_id"
  add_index "branch_products", ["branch_id"], :name => "index_branch_products_on_branch_id"
  add_index "branch_products", ["supplier_id"], :name => "index_branch_products_on_supplier_id"

  create_table "branch_supplier_configurations", :force => true do |t|
    t.integer  "account_id"
    t.string   "branch_id"
    t.string   "supplier_id"
    t.string   "frequency_type"
    t.string   "frequency_week"
    t.string   "frequency_day"
    t.datetime "next_po_date"
    t.datetime "last_po_date"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "branch_supplier_configurations", ["branch_id"], :name => "index_branch_supplier_configurations_on_branch_id"
  add_index "branch_supplier_configurations", ["supplier_id"], :name => "index_branch_supplier_configurations_on_supplier_id"

  create_table "branches", :force => true do |t|
    t.integer  "account_id"
    t.string   "code"
    t.string   "name"
    t.string   "status",     :limit => 1
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "branches", ["account_id"], :name => "index_branches_on_account_id"

  create_table "buyer_suppliers", :force => true do |t|
    t.integer  "user_id"
    t.string   "supplier_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "buyer_suppliers", ["user_id", "supplier_id"], :name => "index_buyer_suppliers_on_user_id_and_supplier_id"

  create_table "companies", :force => true do |t|
    t.integer  "account_id"
    t.string   "name"
    t.string   "code"
    t.string   "reference"
    t.string   "status",     :limit => 1, :default => "A"
    t.string   "address"
    t.string   "telephone"
    t.string   "remark"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  add_index "companies", ["account_id"], :name => "index_companies_on_account_id"

  create_table "ownerships", :force => true do |t|
    t.integer  "account_id"
    t.integer  "user_id"
    t.string   "supplier_id"
    t.string   "product_category_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "ownerships", ["account_id"], :name => "index_ownerships_on_account_id"
  add_index "ownerships", ["user_id", "supplier_id", "product_category_id"], :name => "user_uspplier_categ_index"

  create_table "product_categories", :force => true do |t|
    t.integer  "account_id"
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "product_categories", ["account_id"], :name => "index_product_categories_on_account_id"

  create_table "products", :force => true do |t|
    t.integer  "account_id"
    t.string   "parent_product_id"
    t.string   "product_category_id"
    t.string   "sku"
    t.string   "stock_no"
    t.string   "name"
    t.string   "status",                   :limit => 1
    t.string   "barcode"
    t.string   "base_unit_name"
    t.string   "discount_text"
    t.string   "ordering_unit"
    t.decimal  "cost",                                  :precision => 10, :scale => 0
    t.decimal  "retail_price",                          :precision => 10, :scale => 0
    t.boolean  "allow_decimal_quantities"
    t.decimal  "unit_content_quantity",                 :precision => 10, :scale => 0
    t.datetime "created_at",                                                           :null => false
    t.datetime "updated_at",                                                           :null => false
  end

  add_index "products", ["account_id", "sku"], :name => "index_products_on_account_id_and_sku"
  add_index "products", ["account_id"], :name => "index_products_on_account_id"
  add_index "products", ["parent_product_id"], :name => "index_products_on_parent_product_id"
  add_index "products", ["sku"], :name => "index_products_on_sku"

  create_table "purchase_order_lines", :force => true do |t|
    t.integer  "account_id"
    t.integer  "purchase_order_id",               :limit => 8
    t.string   "product_id"
    t.integer  "stock_keeping_days"
    t.string   "discount_text"
    t.decimal  "average_daily_sales",                          :precision => 15, :scale => 4
    t.decimal  "adjusted_average_daily_sales",                 :precision => 15, :scale => 4
    t.decimal  "onhand_quantity",                              :precision => 15, :scale => 4
    t.decimal  "suggested_quantity",                           :precision => 15, :scale => 4
    t.decimal  "quantity_to_purchase",                         :precision => 15, :scale => 4
    t.decimal  "quantity_per_unit",                            :precision => 15, :scale => 4
    t.decimal  "units_to_purchase",                            :precision => 15, :scale => 4
    t.decimal  "purchase_price",                               :precision => 15, :scale => 4
    t.text     "remarks"
    t.decimal  "subtotal",                                     :precision => 15, :scale => 4
    t.decimal  "subtotal_after_overall_discount",              :precision => 15, :scale => 4
    t.decimal  "average",                                      :precision => 15, :scale => 4
    t.datetime "created_at",                                                                  :null => false
    t.datetime "updated_at",                                                                  :null => false
    t.string   "ordering_unit",                                                               :null => false
  end

  add_index "purchase_order_lines", ["account_id", "purchase_order_id"], :name => "index_purchase_order_lines_on_account_id_and_purchase_order_id"
  add_index "purchase_order_lines", ["account_id"], :name => "index_purchase_order_lines_on_account_id"
  add_index "purchase_order_lines", ["purchase_order_id"], :name => "index_purchase_order_lines_on_purchase_order_id"

  create_table "purchase_orders", :force => true do |t|
    t.integer  "account_id"
    t.string   "branch_id"
    t.string   "supplier_id"
    t.integer  "created_by_id"
    t.integer  "reviewed_by_id"
    t.integer  "approved_by_id"
    t.string   "void_reason"
    t.integer  "reference",                 :limit => 8
    t.string   "status"
    t.string   "computation_options",       :limit => 256
    t.string   "overall_discount"
    t.string   "mother_ref_no"
    t.string   "order_status"
    t.string   "remarks"
    t.datetime "date"
    t.datetime "computation_start_date"
    t.datetime "computation_end_date"
    t.datetime "due_date"
    t.datetime "cancel_date"
    t.decimal  "total_po_cost",                            :precision => 20, :scale => 2
    t.decimal  "total_units",                              :precision => 20, :scale => 2
    t.decimal  "total_quantity",                           :precision => 20, :scale => 2
    t.decimal  "gross_amount",                             :precision => 20, :scale => 2
    t.decimal  "total_item_discount",                      :precision => 20, :scale => 2
    t.decimal  "grand_total",                              :precision => 20, :scale => 2
    t.boolean  "vat_inclusive",                                                           :default => true
    t.datetime "cancellation_date"
    t.string   "terms",                                                                   :default => "paid"
    t.string   "product_category_id"
    t.integer  "voided_by_id"
    t.string   "action_state",                                                            :default => "available"
    t.integer  "editing_by_id"
    t.integer  "assigned_to_user_id"
    t.integer  "current_viewer_id"
    t.datetime "edit_expiration"
    t.datetime "barter_receive_date"
    t.datetime "created_at",                                                                                       :null => false
    t.datetime "updated_at",                                                                                       :null => false
    t.string   "supplier_support_discount"
    t.string   "supplier_terms"
  end

  add_index "purchase_orders", ["account_id"], :name => "index_purchase_orders_on_account_id"
  add_index "purchase_orders", ["assigned_to_user_id"], :name => "index_purchase_orders_on_assigned_to_user_id"
  add_index "purchase_orders", ["branch_id", "assigned_to_user_id"], :name => "index_purchase_orders_on_branch_id_and_assigned_to_user_id"
  add_index "purchase_orders", ["branch_id"], :name => "index_purchase_orders_on_branch_id"
  add_index "purchase_orders", ["product_category_id"], :name => "index_purchase_orders_on_product_category_id"

  create_table "roles", :force => true do |t|
    t.integer "user_id"
    t.string  "name"
  end

  add_index "roles", ["user_id"], :name => "index_roles_on_user_id"

  create_table "supplier_category_schedules", :force => true do |t|
    t.integer  "account_id"
    t.string   "supplier_id"
    t.string   "product_category_id"
    t.integer  "branch_schedule_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "supplier_products", :force => true do |t|
    t.integer  "account_id"
    t.string   "supplier_id"
    t.string   "product_id"
    t.decimal  "cost",                            :precision => 10, :scale => 0
    t.decimal  "retail_price",                    :precision => 10, :scale => 0
    t.string   "last_discount_text"
    t.string   "status",             :limit => 1,                                :default => "A"
    t.datetime "created_at",                                                                      :null => false
    t.datetime "updated_at",                                                                      :null => false
  end

  add_index "supplier_products", ["product_id"], :name => "index_supplier_products_on_product_id"
  add_index "supplier_products", ["supplier_id", "product_id"], :name => "index_supplier_products_on_supplier_id_and_product_id"
  add_index "supplier_products", ["supplier_id"], :name => "index_supplier_products_on_supplier_id"

  create_table "suppliers", :force => true do |t|
    t.integer  "account_id"
    t.string   "principal_id",                   :default => "0"
    t.string   "name"
    t.string   "code"
    t.string   "reference"
    t.string   "discount_text"
    t.string   "contact_person"
    t.string   "contact_email"
    t.string   "contact_mobile"
    t.string   "contact_telephone"
    t.string   "contact_fax"
    t.string   "address"
    t.string   "frequency"
    t.string   "advance_frequency"
    t.string   "day"
    t.string   "status",            :limit => 1, :default => "A"
    t.boolean  "is_principal",                   :default => false
    t.boolean  "vat_inclusive",                  :default => true
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
    t.string   "terms",                          :default => "0"
    t.string   "support_discount",               :default => "0"
  end

  add_index "suppliers", ["account_id", "code"], :name => "index_suppliers_on_account_id_and_code"
  add_index "suppliers", ["account_id"], :name => "index_suppliers_on_account_id"
  add_index "suppliers", ["code"], :name => "index_suppliers_on_code"

  create_table "transactions", :force => true do |t|
    t.integer  "account_id"
    t.string   "branch_id"
    t.string   "product_id"
    t.decimal  "first_inventory",  :precision => 10, :scale => 0
    t.decimal  "quantity",         :precision => 10, :scale => 0
    t.decimal  "last_inventory",   :precision => 10, :scale => 0
    t.date     "date"
    t.string   "transaction_code"
    t.string   "type_code"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
  end

  add_index "transactions", ["account_id", "branch_id", "product_id"], :name => "index_transactions_on_account_id_and_branch_id_and_product_id"
  add_index "transactions", ["account_id"], :name => "index_transactions_on_account_id"
  add_index "transactions", ["branch_id"], :name => "index_transactions_on_branch_id"
  add_index "transactions", ["product_id"], :name => "index_transactions_on_product_id"

  create_table "users", :force => true do |t|
    t.integer  "parent_id"
    t.integer  "supervisor_id"
    t.integer  "account_id"
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "reset_password_token"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "role"
    t.string   "status",               :default => "A"
    t.string   "user_access"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email"
  add_index "users", ["parent_id"], :name => "index_users_on_parent_id"
  add_index "users", ["role"], :name => "index_users_on_role"
  add_index "users", ["supervisor_id"], :name => "index_users_on_supervisor_id"

end

class CreateBranchSupplierConfigurationsTable < ActiveRecord::Migration
	def up
		create_table :branch_supplier_configurations do |t|
			t.integer :account_id
			t.string  :branch_id
			t.string  :supplier_id
			t.string  :frequency_type
			t.string  :frequency_week
			t.string  :frequency_day
			t.datetime :next_po_date
			t.datetime :last_po_date
			t.timestamps
		end

		add_index :branch_supplier_configurations, :branch_id
		add_index :branch_supplier_configurations, :supplier_id
  	end

  	def down
  		remove_index :branch_supplier_configurations, :branch_id
  		remove_index :branch_supplier_configurations, :supplier_id

  		drop_table :branch_supplier_configurations
  	end
end

class AddColumnsToAccount < ActiveRecord::Migration
	def up
		add_column :accounts, :last_data_index, :datetime
		add_column :accounts, :index_status, :string, default: 'done'
  	end
  	
  	def down
  		remove_column :accounts, :last_data_index
  		remove_column :accounts, :index_status
  	end
end

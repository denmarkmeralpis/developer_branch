class CreateSupplierProductsTable < ActiveRecord::Migration
  def up
  	create_table :supplier_products, id: false do |t|
  		t.string :id, null: false
  		t.integer :account_id
  		t.string :supplier_id
  		t.string :product_id
  		t.decimal :cost
  		t.decimal :retail_price
  		t.string :last_discount_text
  		t.string :status, limit: 1, default: 'A'
  		t.timestamps
  	end

  	execute "ALTER TABLE supplier_products ADD PRIMARY KEY (id)"

  	add_index :supplier_products, :supplier_id
  	add_index :supplier_products, :product_id
  	add_index :supplier_products, [:supplier_id, :product_id]

  end

  def down
  	remove_index :supplier_products, :supplier_id
  	remove_index :supplier_products, :product_id
  	remove_index :supplier_products, [:supplier_id, :product_id]

  	drop_table :supplier_products
  end
end

class CreateBranchesTable < ActiveRecord::Migration
  def up
  	create_table :branches, id: false do |t|
  		t.string  :id, null: false  		
  		t.integer :account_id
  		t.string  :code
  		t.string  :name
  		t.string  :status, limit: 1
  		t.timestamps
  	end

  	add_index :branches, :account_id

  	execute "ALTER TABLE branches ADD PRIMARY KEY(id);"
  end

  def down
  	remove_index :branches, :account_id
    
  	drop_table :branches
  end
end

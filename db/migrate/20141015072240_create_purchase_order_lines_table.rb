class CreatePurchaseOrderLinesTable < ActiveRecord::Migration
  def up
  	create_table :purchase_order_lines do |t|  		
      t.integer  "account_id"
      t.integer  "purchase_order_id",               :limit => 8
      t.string  "product_id"
      t.integer  "stock_keeping_days"
      t.string   "discount_text"
      t.decimal  "average_daily_sales",                          :precision => 15, :scale => 4
      t.decimal  "adjusted_average_daily_sales",                 :precision => 15, :scale => 4
      t.decimal  "onhand_quantity",                              :precision => 15, :scale => 4
      t.decimal  "suggested_quantity",                           :precision => 15, :scale => 4
      t.decimal  "quantity_to_purchase",                         :precision => 15, :scale => 4
      t.decimal  "quantity_per_unit",                            :precision => 15, :scale => 4
      t.decimal  "units_to_purchase",                            :precision => 15, :scale => 4
      t.decimal  "purchase_price",                               :precision => 15, :scale => 4
      t.text     "remarks"
      t.decimal  "subtotal",                                     :precision => 15, :scale => 4
      t.decimal  "subtotal_after_overall_discount",              :precision => 15, :scale => 4
      t.decimal  "average",                                      :precision => 15, :scale => 4
      t.datetime "created_at",                                                                  :null => false
      t.datetime "updated_at",                                                                  :null => false
  		t.timestamps
  	end

  	add_index :purchase_order_lines, :account_id
  	add_index :purchase_order_lines, :purchase_order_id
  	add_index :purchase_order_lines, [:account_id, :purchase_order_id]
  end

  def down
  	remove_index :purchase_order_lines, :account_id
  	remove_index :purchase_order_lines, :purchase_order_id
  	remove_index :purchase_order_lines, [:account_id, :purchase_order_id]

  	drop_table :purchase_order_lines
  end
end

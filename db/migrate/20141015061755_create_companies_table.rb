class CreateCompaniesTable < ActiveRecord::Migration
  def up
  	create_table :companies, id: false do |t|
  		t.string :id, null: false
  		t.integer :account_id
  		t.string :name
  		t.string :code
  		t.string :reference
  		t.string :status, limit: 1, default: 'A'
  		t.string :address
  		t.string :telephone
  		t.string :remark
  		t.timestamps
  	end

  	execute "ALTER TABLE companies ADD PRIMARY KEY (id)"

  	add_index :companies, :account_id  	
  end

  def down
  	remove_index :companies, :account_id
  	drop_table :companies
  end
end

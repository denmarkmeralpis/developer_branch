class CreateProductCategoriesTable < ActiveRecord::Migration
  def up
  	create_table :product_categories, id: false do |t|
  		t.string  :id, null: false
  		t.integer :account_id
  		t.string  :name
  		t.string  :code
  		t.timestamps
  	end

  	add_index :product_categories, :account_id
  	execute "ALTER TABLE product_categories ADD PRIMARY KEY (id)"
  end

  def down
  	remove_index :product_categories, :account_id

  	drop_table :product_categories
  end
end

class AddMissingColumns < ActiveRecord::Migration
  def up
  	add_column :branch_products, :incoming_quantity, :decimal, default: 0
  	add_column :branch_products, :last_arrival_quantity, :datetime
  	add_column :branch_products, :last_arrival_date, :datetime

  	add_column :suppliers, :terms, :string, default: "0"
  	add_column :suppliers, :support_discount, :string, default: "0"  	
  end

  def down
  	remove_column :branch_products, :incoming_quantity
  	remove_column :branch_products, :last_arrival_quantity
  	remove_column :branch_products, :last_arrival_date
  	
  	remove_column :suppliers, :support_discount
  	remove_column :suppliers, :terms
  end
end

class CreateProductsTable < ActiveRecord::Migration
  def up
  	create_table :products, id: false do |t|
  		t.string  :id, null: false
  		t.integer :account_id
  		t.string  :parent_product_id
  		t.string  :product_category_id
  		t.string  :sku
  		t.string  :stock_no
  		t.string  :name
  		t.string  :status, limit: 1
  		t.string  :barcode
  		t.string  :base_unit_name
  		t.string  :discount_text
  		t.string  :ordering_unit
  		t.decimal :cost
  		t.decimal :retail_price
  		t.boolean :allow_decimal_quantities
  		t.decimal :unit_content_quantity
  		t.timestamps
  	end

  	add_index :products, :account_id
  	add_index :products, :parent_product_id
  	add_index :products, :sku

  	add_index :products, [:account_id, :sku]

  	execute "ALTER TABLE products ADD PRIMARY KEY (id)"
  end

  def down

  	remove_index :products, :account_id
  	remove_index :products, :parent_product_id
  	remove_index :products, :sku

  	drop_table :products

  end
end

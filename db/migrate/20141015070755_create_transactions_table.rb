class CreateTransactionsTable < ActiveRecord::Migration
  def up
  	create_table :transactions do |t|
  		t.integer :account_id
  		t.string  :branch_id
  		t.string  :product_id
  		t.decimal :first_inventory
  		t.decimal :quantity
  		t.decimal :last_inventory
  		t.date :date
  		t.string :transaction_code
  		t.string :type_code
  		t.timestamps
  	end

  	add_index :transactions, :product_id
  	add_index :transactions, :account_id
  	add_index :transactions, :branch_id
  	add_index :transactions, [:account_id, :branch_id, :product_id]
  end

  def down
  	remove_index :transactions, :product_id
  	remove_index :transactions, :account_id
  	remove_index :transactions, :branch_id
  	remove_index :transactions, [:account_id, :branch_id, :product_id]
  	
  	drop_table :transactions
  end
end

class CreateOwnershipsTable < ActiveRecord::Migration
  def up
  	create_table :ownerships do |t|
  		t.integer :account_id
  		t.integer :user_id
  		t.string :supplier_id
  		t.string :product_category_id
  		t.timestamps
  	end

  	add_index :ownerships, :account_id
  	add_index :ownerships, [:user_id, :supplier_id, :product_category_id], name: :user_uspplier_categ_index
  end

  def down

  	remove_index :ownerships, :account_id
  	remove_index :ownerships, [:user_id, :supplier_id, :product_category_id]

  	drop_table :ownerships

  end
end

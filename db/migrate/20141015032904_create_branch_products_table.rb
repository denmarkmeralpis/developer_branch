class CreateBranchProductsTable < ActiveRecord::Migration
   def up
   	create_table :branch_products, id: false do |t|
   		t.string  :id, 			null: false   		
   		t.string  :branch_id,	null: false
   		t.string  :product_id,  null: false
   		t.string  :supplier_id
   		t.integer :account_id
   		t.decimal :cost
   		t.string  :discount_text
   		t.string  :status, limit: 1, default: 'A'
   		t.timestamps
   	end

      add_index :branch_products, :branch_id
      add_index :branch_products, :supplier_id
      add_index :branch_products, [:branch_id, :supplier_id]

      execute "ALTER TABLE branch_products ADD PRIMARY KEY(id);"
   end   
 
   def down
      remove_index :branch_products, :branch_id
      remove_index :branch_products, :supplier_id
      remove_index :branch_products, [:branch_id, :supplier_id]

   	drop_table :branch_products
   end
end

class CreateSuppliersTable < ActiveRecord::Migration
  def up
  	create_table :suppliers, id: false do |t|
  		t.string :id, null: false
  		t.integer :account_id
  		t.string :principal_id, default: '0'
  		t.string :name
  		t.string :code
  		t.string :reference
  		t.string :discount_text
  		t.string :contact_person
  		t.string :contact_email
  		t.string :contact_mobile
  		t.string :contact_telephone
  		t.string :contact_fax
  		t.string :address
  		t.string :frequency
  		t.string :advance_frequency
  		t.string :day
  		t.string :status, limit: 1, default: 'A'
  		t.boolean :is_principal, default: false
  		t.boolean :vat_inclusive, default: true
  		t.timestamps  		
  	end

  	execute "ALTER TABLE suppliers ADD PRIMARY KEY (id)"

  	add_index :suppliers, :account_id
  	add_index :suppliers, :code
  	add_index :suppliers, [:account_id, :code]  	
  end

  def down
  	remove_index :suppliers, :account_id
  	remove_index :suppliers, :code
  	remove_index :suppliers, [:account_id, :code]
  	
  	drop_table :suppliers
  end
end

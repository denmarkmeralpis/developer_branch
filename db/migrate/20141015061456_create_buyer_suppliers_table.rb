class CreateBuyerSuppliersTable < ActiveRecord::Migration
  def up
  	create_table :buyer_suppliers do |t|
  		t.integer :user_id
  		t.string  :supplier_id
  		t.timestamps
  	end

  	add_index :buyer_suppliers, [:user_id, :supplier_id]
  end

  def down
  	remove_index :buyer_suppliers, [:user_id, :supplier_id]

  	drop_table :buyer_suppliers
  end
end

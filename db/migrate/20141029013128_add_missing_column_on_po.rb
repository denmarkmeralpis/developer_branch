class AddMissingColumnOnPo < ActiveRecord::Migration
  
  def up
  	add_column :purchase_orders, :supplier_support_discount, :string, :default => nil  	
  	add_column :purchase_orders, :supplier_terms, :string, :default => nil
  end

  def down
  	remove_column :purchase_orders, :supplier_support_discount  	
  	remove_column :purchase_orders, :supplier_terms
  end

end

class ChangePurchaseOrderColumn < ActiveRecord::Migration
  def up
  	change_column :purchase_orders, :product_category_id, :string
  end

  def down
  	change_column :purchase_orders, :product_category_id, :int
  end
end

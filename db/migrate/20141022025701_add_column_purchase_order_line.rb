class AddColumnPurchaseOrderLine < ActiveRecord::Migration
	def up
		add_column :purchase_order_lines, :ordering_unit, :string, null: false
	end
  	
	def down
		remove_column :purchase_order_lines, :ordering_unit, :string
	end
end

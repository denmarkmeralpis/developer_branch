class CreateUsersTable < ActiveRecord::Migration
  def up
  	create_table :users do |t|
  		t.integer :parent_id
  		t.integer :supervisor_id
  		t.integer :account_id
  		t.string  :email
  		t.string :password_hash
  		t.string :password_salt
  		t.string :reset_password_token
  		t.string :first_name
  		t.string :last_name
  		t.string :role
  		t.string :status, default: 'A'
  		t.string :user_access
  		t.timestamps  		
  	end

  	add_index :users, :supervisor_id
  	add_index :users, :parent_id
  	add_index :users, :email
  	add_index :users, :role
  end

  def down
  	remove_index :users, :supervisor_id
  	remove_index :users, :parent_id
  	remove_index :users, :email
  	remove_index :users, :role

  	drop_table :users
  end
end

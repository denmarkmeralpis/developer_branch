class CreateAPIKeysTable < ActiveRecord::Migration
  	def up
  		create_table :api_keys do |t|
  			t.integer :account_id
  			t.string :access_token, null: false  			
  			t.timestamps
  		end
  	end

  	def down
  		drop_table :api_keys
	end
end

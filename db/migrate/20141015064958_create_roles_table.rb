class CreateRolesTable < ActiveRecord::Migration
  def up
  	create_table :roles do |t|
  		t.integer :user_id
  		t.string  :name  		
  	end

  	add_index :roles, :user_id
  end

  def down
  	remove_index :roles, :user_id
  	drop_table :roles
  end
end

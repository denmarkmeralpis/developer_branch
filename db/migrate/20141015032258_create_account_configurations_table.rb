class CreateAccountConfigurationsTable < ActiveRecord::Migration
	def up
		create_table :account_configurations do |t|
			t.integer :account_id
			t.string  :value, default: ''
			t.timestamps
		end
  	end

  	def down
  		drop_table :account_configurations
  	end
end

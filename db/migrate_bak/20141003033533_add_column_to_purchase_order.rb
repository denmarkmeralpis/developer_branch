class AddColumnToPurchaseOrder < ActiveRecord::Migration
  def up
  	add_column :purchase_orders, :barter_receive_date, :datetime
  end

  def down
  	remove_column :purchase_orders, :barter_receive_date, :datetime
  end
end

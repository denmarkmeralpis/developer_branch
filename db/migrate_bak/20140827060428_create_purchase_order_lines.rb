class CreatePurchaseOrderLines < ActiveRecord::Migration
  def self.up
    create_table :purchase_order_lines do |t|
      t.integer :account_id
      t.integer :purchase_order_id
      t.integer :product_id
      t.integer :stock_keeping_days
      t.string  :computation_options
      t.string  :discount_text
      t.string  :unit_name,                       limit: 30
      t.date    :computation_start_date
      t.date    :computation_end_date
      t.decimal :average_daily_sales,             :precision => 15, :scale => 4
      t.decimal :adjusted_average_daily_sales,    :precision => 15, :scale => 4
      t.decimal :onhand_quantity,                 :precision => 15, :scale => 4
      t.decimal :suggested_quantity,              :precision => 15, :scale => 4
      t.decimal :ordered_quantity,                :precision => 15, :scale => 4
      t.decimal :unit_content_quantity,           :precision => 15, :scale => 4
      t.decimal :unit_quantity,                   :precision => 15, :scale => 4
      t.decimal :unit_original_cost,              :precision => 15, :scale => 4
      t.decimal :unit_actual_cost,                :precision => 15, :scale => 4
      t.decimal :subtotal,                        :precision => 15, :scale => 4
      t.decimal :subtotal_after_overall_discount, :precision => 15, :scale => 4
      t.text    :remarks
      t.timestamps
    end
    add_index :purchase_order_lines, :account_id
    add_index :purchase_order_lines, :purchase_order_id
    add_index :purchase_order_lines, [:purchase_order_id, :product_id]
  end
  def self.down
    remove_index :purchase_order_lines, :account_id
    remove_index :purchase_order_lines, [:purchase_order_id, :product_id]
    remove_index :purchase_order_lines, :purchase_order_id
    drop_table :purchase_order_lines
  end
end

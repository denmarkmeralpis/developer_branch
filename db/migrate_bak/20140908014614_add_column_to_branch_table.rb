class AddColumnToBranchTable < ActiveRecord::Migration
  def self.up
    add_column :branches, :company_id, :integer
    add_column :branches, :reference, :string

    add_index :branches, :company_id
  end
  def self.down
    remove_index :branches, :company_id

    remove_column :branches, :company_id
    remove_column :branches, :reference
  end
end

class CreateCompanies < ActiveRecord::Migration

  def self.up
    create_table :companies do |t|
      t.integer :account_id
      t.string  :name, limit: 100
      t.string  :code, limit: 30
      t.string  :reference, limit: 30
      t.string  :status, limit: 1, default: 'A'
      t.timestamps
    end
  end

  def self.down
    drop_table :companies
  end

end

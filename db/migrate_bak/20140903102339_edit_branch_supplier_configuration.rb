class EditBranchSupplierConfiguration < ActiveRecord::Migration

  def self.up
    remove_column :branch_supplier_configurations, :advance_frequency
    remove_column :branch_supplier_configurations, :frequency
    remove_column :branch_supplier_configurations, :day

    add_column :branch_supplier_configurations, :frequency_type, :string
    add_column :branch_supplier_configurations, :frequency_week, :string
    add_column :branch_supplier_configurations, :frequency_day,  :string
  end

  def self.down
    add_column :branch_supplier_configurations, :advance_frequency, :string
    add_column :branch_supplier_configurations, :frequency, :string
    add_column :branch_supplier_configurations, :day, :string

    remove_column :branch_supplier_configurations, :frequency_type
    remove_column :branch_supplier_configurations, :frequency_week
    remove_column :branch_supplier_configurations, :frequency_day
  end

end

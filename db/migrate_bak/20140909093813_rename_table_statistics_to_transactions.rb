class RenameTableStatisticsToTransactions < ActiveRecord::Migration
  def self.up
    rename_table :statistics, :transactions
    add_column :purchase_order_lines, :average, :decimal, default: 0.0
    add_column :branch_supplier_configurations, :next_run_date, :datetime
  end

  def self.down
    drop_table :transactions
    #rename_table :transactions, :statistics
    #remove_column :purchase_order_lines, :average
    #remove_column :branch_supplier_configurations
  end
end

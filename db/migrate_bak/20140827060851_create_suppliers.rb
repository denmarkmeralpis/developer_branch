class CreateSuppliers < ActiveRecord::Migration
  def self.up
    create_table :suppliers do |t|
      t.integer :account_id
      t.integer :principal_id
      t.string  :name,              limit: 100
      t.string  :code,              limit: 30
      t.string  :reference,         limit: 30
      t.string  :discount_text,     limit: 256
      t.string  :contact_person,    limit: 100
      t.string  :contact_email,     limit: 100
      t.string  :contact_mobile,    limit: 30
      t.string  :contact_telephone, limit: 30
      t.string  :contact_fax,       limit: 30
      t.string  :address
      t.integer :frequency
      t.integer :advance_frequency
      t.string  :day
      t.string  :historical_data
      t.string  :status,            limit: 1, default: 'A'
      t.boolean :is_principal,      default: false
      t.timestamps
    end

    add_index :suppliers, [:account_id, :code]
  end

  def self.down
    remove_index :suppliers, [:account_id, :code]

    drop_table :suppliers
  end
end

class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.integer :account_id
      t.integer :parent_product_id,   default: ''
      t.string  :sku,                 limit: 30
      t.string  :stock_no,            limit: 30
      t.string  :name,                limit: 100
      t.string  :status,              limit: 1, default: 'A'
      t.string  :barcode
      t.string  :base_unit_name
      t.string  :discount_text
      t.string  :ordering_unit
      t.decimal :cost,                precision: 15, scale: 4, default: 0.0
      t.decimal :retail_price,        precision: 15, scale: 4, default: 0.0
      t.boolean :allow_decimal_quantities, default: false
      t.timestamps
    end
    add_index :products, [:account_id, :sku]
  end

  def self.down
    remove_index :products, [:account_id, :sku]

    drop_table :products
  end
end

class CreateSupplierProducts < ActiveRecord::Migration
  def self.up
    create_table :supplier_products do |t|
      t.integer :account_id
      t.integer :supplier_id
      t.integer :product_id
      t.integer :unit_id
      t.decimal :cost,               precision: 15, scale: 4
      t.decimal :retail_price,       precision: 15, scale: 4
      t.string  :last_discount_text, limit: 256 # Ace ?
      t.string  :status,             limit: 1, default: 'A'
      t.timestamps
    end

    add_index :supplier_products, [:account_id, :supplier_id, :product_id, :unit_id], :name => 'index_supplier_products_account_id_supplier_id_etc'
  end

  def self.down
    remove_index :supplier_products, :name => 'index_supplier_products_account_id_supplier_id_etc'

    drop_table :supplier_products
  end
end

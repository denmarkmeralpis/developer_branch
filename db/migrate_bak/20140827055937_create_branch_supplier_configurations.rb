class CreateBranchSupplierConfigurations < ActiveRecord::Migration

  def self.up
    create_table :branch_supplier_configurations do |t|
      t.integer :account_id
      t.integer :branch_id
      t.integer :supplier_id
      t.integer :frequency
      t.integer :advance_frequency
      t.string  :day
      t.timestamps
    end

    add_index :branch_supplier_configurations, :branch_id
    add_index :branch_supplier_configurations, :supplier_id

  end

  def self.down
    remove_index :branch_supplier_configurations, :branch_id
    remove_index :branch_supplier_configurations, :supplier_id

    drop_table :branch_supplier_configurations
  end

end

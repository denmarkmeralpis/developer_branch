class AddColumnsToTransactionTable < ActiveRecord::Migration
  def self.up
    add_column :transactions, :transaction_code, :string
    add_column :transactions, :type_code, :string
  end


  def self.down
    #remove_column :transactions, :transaction_code
    #remove_column :transactions, :type_code
  end
end

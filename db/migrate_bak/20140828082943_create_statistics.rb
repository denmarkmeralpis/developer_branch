class CreateStatistics < ActiveRecord::Migration
  def self.up
    create_table :statistics do |t|
      t.integer :account_id #
      t.integer :branch_id #
      t.integer :product_id #
      # t.string  :type_code, default: ''
      # t.string  :doc_id
      t.decimal :first_inventory
      t.decimal :quantity
      t.decimal :last_inventory
      t.date    :date
      t.timestamps
    end

    #add_index :statistics, :account_id
    #add_index :statistics, :branch_id
    #add_index :statistics, :product_id
  end
  def self.down
    #remove_index :statistics, :account_id
    #remove_index :statistics, :branch_id
    #remove_index :statistics, :product_id

    drop_table :statistics
  end
end

class CreateSales < ActiveRecord::Migration
  def self.up
    create_table :sales do |t|
      t.integer :account_id
      t.integer :branch_id
      t.integer :product_id
      t.integer :batch_no
      t.decimal :quantity_sold,       precision: 10, scale: 0
      t.decimal :quantity_remained,   precision: 10, scale: 0
      t.date    :date
      t.timestamps
    end

    add_index :sales, [:account_id, :batch_no]
    add_index :sales, [:account_id, :date, :branch_id, :product_id]
  end

  def self.down
    remove_index :sales, [:account_id, :batch_no]
    remove_index :sales, [:account_id, :date, :branch_id, :product_id]

    drop_table :sales
  end
end

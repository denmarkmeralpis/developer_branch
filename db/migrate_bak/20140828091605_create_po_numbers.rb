class CreatePoNumbers < ActiveRecord::Migration
  def self.up
    create_table :po_numbers do |t|
      t.integer :purchase_order_id
      t.integer :user_id
      t.integer :count
      t.timestamps
    end
  end

  def self.down
    drop_table :po_numbers
  end
end

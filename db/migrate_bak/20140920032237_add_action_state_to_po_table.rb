class AddActionStateToPoTable < ActiveRecord::Migration
  def self.up
  	add_column :purchase_orders, :voided_by_id, :integer
  	add_column :purchase_orders, :action_state, :string, default: 'available'
  	add_column :purchase_orders, :editing_by_id, :integer
  end

  def self.down
  	remove_column :purchase_orders, :action_state
  	remove_column :purchase_orders, :editing_by_id
  end
end

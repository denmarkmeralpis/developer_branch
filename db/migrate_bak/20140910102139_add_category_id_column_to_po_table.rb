class AddCategoryIdColumnToPoTable < ActiveRecord::Migration
  def self.up
    add_column :purchase_orders, :product_category_id, :integer
  end
  def self.down
    #remove_column :purchase_orders, :product_category_id
  end
end

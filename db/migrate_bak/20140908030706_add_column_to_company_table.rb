class AddColumnToCompanyTable < ActiveRecord::Migration
  def self.up
    add_column :companies, :address,    :string
    add_column :companies, :telephone,  :string
    add_column :companies, :remark,     :string
  end

  def self.down
    remove_column :companies, :address
    remove_column :companies, :telephone
    remove_column :companies, :remark
  end
end

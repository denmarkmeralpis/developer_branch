class CreateRoles < ActiveRecord::Migration

  def self.up
    create_table :roles do |t|
      t.integer :user_id
      t.string  :name
      t.timestamps
    end
    add_index :roles, :user_id
  end

  def self.down
    remove_index :roles, :user_id
    drop_table :roles
  end

end

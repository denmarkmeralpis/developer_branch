# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141009035045) do

  create_table "account_configurations", :force => true do |t|
    t.integer  "account_id"
    t.string   "value",      :default => ""
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "accounts", :force => true do |t|
    t.string   "name"
    t.string   "subdomain"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "api_keys", :force => true do |t|
    t.string   "access_token", :null => false
    t.integer  "account_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "branch_products", :force => true do |t|
    t.integer  "account_id"
    t.integer  "branch_id"
    t.integer  "product_id"
    t.integer  "supplier_id"
    t.string   "status",      :limit => 1, :default => "A"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  create_table "branch_supplier_configurations", :force => true do |t|
    t.integer  "account_id"
    t.integer  "branch_id"
    t.integer  "supplier_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "frequency_type"
    t.string   "frequency_week"
    t.string   "frequency_day"
    t.datetime "next_run_date"
    t.datetime "last_po_date"
    t.datetime "next_po_date"
  end

  add_index "branch_supplier_configurations", ["branch_id"], :name => "index_branch_supplier_configurations_on_branch_id"
  add_index "branch_supplier_configurations", ["supplier_id"], :name => "index_branch_supplier_configurations_on_supplier_id"

  create_table "branches", :force => true do |t|
    t.integer  "account_id"
    t.string   "code",       :limit => 30
    t.string   "name",       :limit => 100
    t.string   "status",     :limit => 1
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.integer  "company_id"
    t.string   "reference"
  end

  add_index "branches", ["company_id"], :name => "index_branches_on_company_id"

  create_table "buyer_suppliers", :force => true do |t|
    t.integer  "user_id"
    t.integer  "supplier_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "buyer_suppliers", ["supplier_id"], :name => "index_buyer_suppliers_on_supplier_id"
  add_index "buyer_suppliers", ["user_id"], :name => "index_buyer_suppliers_on_user_id"

  create_table "companies", :force => true do |t|
    t.integer  "account_id"
    t.string   "name",       :limit => 100
    t.string   "code",       :limit => 30
    t.string   "reference",  :limit => 30
    t.string   "status",     :limit => 1,   :default => "A"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "address"
    t.string   "telephone"
    t.string   "remark"
  end

  create_table "loading_logs", :force => true do |t|
    t.integer  "account_id"
    t.string   "loading_type"
    t.string   "filename"
    t.string   "status"
    t.string   "message"
    t.integer  "last_read_line"
    t.string   "last_known_row"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "loading_logs", ["account_id"], :name => "index_loading_logs_on_account_id"

  create_table "ownerships", :force => true do |t|
    t.integer  "account_id"
    t.integer  "user_id"
    t.integer  "supplier_id"
    t.integer  "product_category_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "po_numbers", :force => true do |t|
    t.integer  "purchase_order_id"
    t.integer  "user_id"
    t.integer  "count"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "product_categories", :force => true do |t|
    t.integer  "account_id"
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "products", :force => true do |t|
    t.integer  "account_id"
    t.integer  "parent_product_id",                                                      :default => 0
    t.string   "sku",                      :limit => 30
    t.string   "stock_no",                 :limit => 30
    t.string   "name",                     :limit => 100
    t.string   "status",                   :limit => 1,                                  :default => "A"
    t.string   "barcode"
    t.string   "base_unit_name"
    t.string   "discount_text"
    t.string   "ordering_unit"
    t.decimal  "cost",                                    :precision => 15, :scale => 4, :default => 0.0
    t.decimal  "retail_price",                            :precision => 15, :scale => 4, :default => 0.0
    t.boolean  "allow_decimal_quantities",                                               :default => false
    t.datetime "created_at",                                                                                :null => false
    t.datetime "updated_at",                                                                                :null => false
    t.decimal  "unit_content_quantity",                   :precision => 10, :scale => 0, :default => 1
    t.integer  "product_category_id"
  end

  add_index "products", ["account_id", "sku"], :name => "index_products_on_account_id_and_sku"

  create_table "purchase_order_lines", :force => true do |t|
    t.integer  "account_id"
    t.integer  "purchase_order_id",               :limit => 8
    t.integer  "product_id"
    t.integer  "stock_keeping_days"
    t.string   "discount_text"
    t.decimal  "average_daily_sales",                          :precision => 15, :scale => 4
    t.decimal  "adjusted_average_daily_sales",                 :precision => 15, :scale => 4
    t.decimal  "onhand_quantity",                              :precision => 15, :scale => 4
    t.decimal  "suggested_quantity",                           :precision => 15, :scale => 4
    t.decimal  "quantity_to_purchase",                         :precision => 15, :scale => 4
    t.decimal  "quantity_per_unit",                            :precision => 15, :scale => 4
    t.decimal  "units_to_purchase",                            :precision => 15, :scale => 4
    t.decimal  "purchase_price",                               :precision => 15, :scale => 4
    t.text     "remarks"
    t.decimal  "subtotal",                                     :precision => 15, :scale => 4
    t.decimal  "subtotal_after_overall_discount",              :precision => 15, :scale => 4
    t.decimal  "average",                                      :precision => 15, :scale => 4
    t.datetime "created_at",                                                                  :null => false
    t.datetime "updated_at",                                                                  :null => false
  end

  create_table "purchase_orders", :force => true do |t|
    t.integer  "account_id"
    t.integer  "branch_id"
    t.integer  "supplier_id"
    t.integer  "created_by_id"
    t.integer  "reviewed_by_id"
    t.integer  "approved_by_id"
    t.string   "void_reason"
    t.integer  "reference",              :limit => 8
    t.string   "status"
    t.string   "computation_options",    :limit => 256
    t.string   "overall_discount"
    t.string   "mother_ref_no"
    t.string   "order_status"
    t.string   "remarks"
    t.datetime "date"
    t.datetime "computation_start_date"
    t.datetime "computation_end_date"
    t.datetime "due_date"
    t.datetime "cancel_date"
    t.decimal  "total_po_cost",                         :precision => 20, :scale => 2
    t.decimal  "total_units",                           :precision => 20, :scale => 2
    t.decimal  "total_quantity",                        :precision => 20, :scale => 2
    t.decimal  "gross_amount",                          :precision => 20, :scale => 2
    t.decimal  "total_item_discount",                   :precision => 20, :scale => 2
    t.decimal  "grand_total",                           :precision => 20, :scale => 2
    t.boolean  "vat_inclusive",                                                        :default => true
    t.datetime "cancellation_date"
    t.string   "terms",                                                                :default => "paid"
    t.datetime "created_at",                                                                                    :null => false
    t.datetime "updated_at",                                                                                    :null => false
    t.integer  "product_category_id"
    t.integer  "voided_by_id"
    t.string   "action_state",                                                         :default => "available"
    t.integer  "editing_by_id"
    t.integer  "assigned_to_user_id"
    t.integer  "current_viewer_id"
    t.datetime "edit_expiration"
    t.datetime "barter_receive_date"
  end

  add_index "purchase_orders", ["account_id"], :name => "index_purchase_orders_on_account_id"
  add_index "purchase_orders", ["branch_id"], :name => "index_purchase_orders_on_branch_id"

  create_table "roles", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "roles", ["user_id"], :name => "index_roles_on_user_id"

  create_table "sales", :force => true do |t|
    t.integer  "account_id"
    t.integer  "branch_id"
    t.integer  "product_id"
    t.integer  "batch_no"
    t.decimal  "quantity_sold",     :precision => 10, :scale => 0
    t.decimal  "quantity_remained", :precision => 10, :scale => 0
    t.date     "date"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
  end

  add_index "sales", ["account_id", "batch_no"], :name => "index_sales_on_account_id_and_batch_no"
  add_index "sales", ["account_id", "date", "branch_id", "product_id"], :name => "index_sales_on_account_id_and_date_and_branch_id_and_product_id"

  create_table "supplier_products", :force => true do |t|
    t.integer  "account_id"
    t.integer  "supplier_id"
    t.integer  "product_id"
    t.integer  "unit_id"
    t.decimal  "cost",                              :precision => 15, :scale => 4
    t.decimal  "retail_price",                      :precision => 15, :scale => 4
    t.string   "last_discount_text", :limit => 256
    t.string   "status",             :limit => 1,                                  :default => "A"
    t.datetime "created_at",                                                                        :null => false
    t.datetime "updated_at",                                                                        :null => false
  end

  add_index "supplier_products", ["account_id", "supplier_id", "product_id", "unit_id"], :name => "index_supplier_products_account_id_supplier_id_etc"

  create_table "suppliers", :force => true do |t|
    t.integer  "account_id"
    t.integer  "principal_id"
    t.string   "name",              :limit => 100
    t.string   "code",              :limit => 30
    t.string   "reference",         :limit => 30
    t.string   "discount_text",     :limit => 256
    t.string   "contact_person",    :limit => 100
    t.string   "contact_email",     :limit => 100
    t.string   "contact_mobile",    :limit => 30
    t.string   "contact_telephone", :limit => 30
    t.string   "contact_fax",       :limit => 30
    t.string   "address"
    t.integer  "frequency"
    t.integer  "advance_frequency"
    t.string   "day"
    t.string   "historical_data"
    t.string   "status",            :limit => 1,   :default => "A"
    t.boolean  "is_principal",                     :default => false
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.boolean  "vat_inclusive",                    :default => true
  end

  add_index "suppliers", ["account_id", "code"], :name => "index_suppliers_on_account_id_and_code"

  create_table "transactions", :force => true do |t|
    t.integer  "account_id"
    t.integer  "branch_id"
    t.integer  "product_id"
    t.decimal  "first_inventory",  :precision => 10, :scale => 0
    t.decimal  "quantity",         :precision => 10, :scale => 0
    t.decimal  "last_inventory",   :precision => 10, :scale => 0
    t.date     "date"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.string   "transaction_code"
    t.string   "type_code"
  end

  create_table "users", :force => true do |t|
    t.integer  "supervisor_id"
    t.integer  "account_id"
    t.integer  "sign_in_count",          :default => 0
    t.string   "email",                  :default => ""
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "reset_password_token"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "role"
    t.string   "status",                 :default => "A"
    t.string   "user_access"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.integer  "parent_id"
  end

  add_index "users", ["email"], :name => "index_users_on_email"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token"
  add_index "users", ["supervisor_id"], :name => "index_users_on_supervisor_id"

end

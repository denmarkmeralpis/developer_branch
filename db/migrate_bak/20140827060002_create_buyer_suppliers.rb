class CreateBuyerSuppliers < ActiveRecord::Migration

  def self.up
    create_table :buyer_suppliers do |t|
      t.integer :user_id
      t.integer :supplier_id
      t.timestamps
    end

    add_index :buyer_suppliers, :supplier_id
    add_index :buyer_suppliers, :user_id
  end

  def self.down
    remove_index :buyer_suppliers, :supplier_id
    remove_index :buyer_suppliers, :user_id

    drop_table :buyer_suppliers
  end

end

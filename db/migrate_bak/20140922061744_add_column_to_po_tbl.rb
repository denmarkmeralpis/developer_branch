class AddColumnToPoTbl < ActiveRecord::Migration
  def self.up
  	add_column :purchase_orders, :current_viewer_id, :integer
  	add_column :purchase_orders, :edit_expiration, :datetime
  end
  def self.down
  	remove_column :purchase_orders, :current_viewer_id
  	remove_column :purchase_orders, :edit_expiration
  end
end

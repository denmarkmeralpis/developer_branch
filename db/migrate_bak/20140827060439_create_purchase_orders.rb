class CreatePurchaseOrders < ActiveRecord::Migration
  def self.up
    create_table :purchase_orders do |t|
      t.integer  :account_id
      t.integer  :branch_id
      t.integer  :supplier_id
      t.integer  :purchase_order_no
      t.integer  :created_by_id
      t.integer  :reviewed_by_id
      t.integer  :approved_by_id
      t.string   :void_reason
      t.string   :reference,            limit: 30
      t.string   :status
      t.string   :computation_options,  limit: 256
      t.string   :overall_discount
      t.string   :mother_ref_no
      t.string   :order_status
      t.string   :remarks
      t.datetime :date
      t.datetime :computation_start_date
      t.datetime :computation_end_date
      t.datetime :due_date
      t.datetime :cancel_date
      t.decimal  :total_po_cost,       precision: 20, scale: 2
      t.decimal  :total_units,         precision: 20, scale: 2
      t.decimal  :total_quantity,      precision: 20, scale: 2
      t.decimal  :gross_amount,        precision: 20, scale: 2
      t.decimal  :total_item_discount, precision: 20, scale: 2
      t.decimal  :grand_total,         precision: 20, scale: 2
      t.boolean  :vat_inclusive,       default: true
      t.datetime :cancellation_date
      t.string   :terms,               default: 'paid'
      t.timestamps
    end

    add_index :purchase_orders, :account_id
    add_index :purchase_orders, :branch_id
  end
  def self.down
    remove_index :purchase_orders, :account_id
    remove_index :purchase_orders, :branch_id

    drop_table :purchase_orders
  end
end

class RemovePoNumberFromPurchaseOrders < ActiveRecord::Migration
  def up
  	remove_column :purchase_orders, :purchase_order_no
  end

  def down
  	add_column :purchase_orders, :purchase_order_no, :integer
  end
end

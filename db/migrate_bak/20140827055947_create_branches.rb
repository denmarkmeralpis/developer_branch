class CreateBranches < ActiveRecord::Migration

  def self.up
    create_table :branches do |t|
      t.integer :account_id
      t.string  :code,   limit: 30
      t.string  :name,   limit: 100
      t.string  :status, limit: 1
      t.timestamps
    end
  end

  def self.down
    drop_table :branches
  end

end

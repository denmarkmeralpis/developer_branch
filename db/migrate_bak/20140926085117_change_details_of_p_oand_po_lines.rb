class ChangeDetailsOfPOandPoLines < ActiveRecord::Migration
  def up
  	change_column :purchase_orders, :reference, :bigint
  	change_column :purchase_order_lines, :purchase_order_id, :bigint
  end

  def down
  	change_column :purchase_orders, :reference, :string
  	change_column :purchase_order_lines, :purchase_order_id, :integer
  end
end

class CreateLoadingLogs < ActiveRecord::Migration
  def self.up
    create_table :loading_logs do |t|
      t.integer :account_id
      t.string  :loading_type
      t.string  :filename
      t.string  :status
      t.string  :message
      t.integer :last_read_line
      t.string  :last_known_row
      t.timestamps
    end

    add_index :loading_logs, :account_id
  end

  def self.down
    remove_index :loading_logs, :account_id

    drop_table :loading_logs
  end
end

class AddColToProduct < ActiveRecord::Migration

  def self.up
    add_column :products, :unit_content_quantity, :decimal, default: 1

  end

  def self.down
    remove_column :products, :unit_content_quantity
  end

end

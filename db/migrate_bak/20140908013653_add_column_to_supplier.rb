class AddColumnToSupplier < ActiveRecord::Migration

  def self.up
    add_column :suppliers, :vat_inclusive, :boolean, default: true

  end

  def self.down
    remove_column :suppliers, :vat_inclusive
  end
end

class CreateBranchProducts < ActiveRecord::Migration

  def change
    create_table :branch_products do |t|
      t.integer :account_id
      t.integer :branch_id
      t.integer :product_id
      t.integer :supplier_id
      t.string  :status, limit: 1, default: 'A'
      t.timestamps
    end
  end

  def self.down
    drop_table :branch_products
  end

end

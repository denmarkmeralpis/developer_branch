class AddColumnToBranchSupplierConfigurations < ActiveRecord::Migration
  
  def self.up
  	add_column :branch_supplier_configurations, :last_po_date, :datetime
  	add_column :branch_supplier_configurations, :next_po_date, :datetime
  end
  
  def self.down
  	remove_column :branch_supplier_configurations, :last_po_date
  	remove_column :branch_supplier_configurations, :next_po_date
  end

end

class CreateProductCategories < ActiveRecord::Migration

  def self.up
    create_table :product_categories do |t|
      t.integer :account_id
      t.string  :name
      t.string  :code
      t.timestamps
    end
  end

  def self.down
    drop_table :product_categories
  end

end

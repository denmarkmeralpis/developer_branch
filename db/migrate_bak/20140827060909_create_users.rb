class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.integer  :supervisor_id
      t.integer  :account_id
      t.integer  :sign_in_count,          default: 0
      t.string   :email,                  default: ''
      t.string   :password_hash
      t.string   :password_salt
      t.string   :reset_password_token
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      t.string   :first_name
      t.string   :last_name
      t.string   :role
      t.string   :status,                default: 'A'
      t.string   :user_access
      t.datetime :reset_password_sent_at
      t.datetime :remember_created_at
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.timestamps
    end

    add_index :users, :email
    add_index :users, :reset_password_token
    add_index :users, :supervisor_id
  end

  def self.down
    remove_index :users, :email
    remove_index :users, :reset_password_token
    remove_index :users, :supervisor_id

    drop_table :users
  end
end

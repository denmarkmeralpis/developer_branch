require File.expand_path('../boot', __FILE__)

require 'rails/all'

if defined?(Bundler)
  Bundler.require(*Rails.groups(:assets => %w(development test)))
end

module IrippleCloudbuy
  class Application < Rails::Application

    config.time_zone = 'Asia/Manila'
    config.encoding = "utf-8"
    config.filter_parameters += [:password]
    config.active_support.escape_html_entities_in_json = true
    config.active_record.whitelist_attributes = true
    config.assets.enabled = true
    config.assets.version = '1.0'
  end

  class Application < Rails::Application
    config.assets.paths << "#{Rails}/vendor/assets/fonts"
  end

end

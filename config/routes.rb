IrippleCloudbuy::Application.routes.draw do

  root to: 'sessions#new'

  resources :suppliers do
    member do
      get :activate
      get :products
      get :purchase_orders
      get :activate_product
      post :assigner
      post :unassigner
      get :search_unassigned, path: '/search-unassigned'
      get :search_assigned,   path: '/search-assigned' 
      get :purchase_orders
    end
    collection do
      get :get_product_categories      
    end
  end

  resources :branches do
    member do
      post :assigner
      post :unassigner
      post :getSched
      post :assign_default_supplier
      get  :get_products_duplicate_assign  
      get :search_unassigned, path: '/search-unassigned'
      get :search_assigned,   path: '/search-assigned'
    end

    collection do
      get   :get_branches_of_supplier
    end
  end

  resources :purchase_orders, path: '/purchase-orders' do
    member do
      post   :add_po_line
      post   :del_po_line
      get    :get_prod_to_add 
      get   :purchase_order_lines, path: '/purchase-order-lines'
      post  :po_line_commit, path: '/po-line-commit'
      post  :add_remarks, path: '/add-remarks'
    end

    collection do
      post  :update_line_items, path: '/update-line-items'
      post  :trigger_manual_po, path: '/manual-po'
      post  :copy_po
    end
  end

  resources :purchase_order_lines, path: '/purchase-order-lines' do    
    collection do
      get  :order_to_suppliers, path: '/order-to-suppliers'
      post :transfer_to_supplier, path: '/transfer-to-supplier'
      post :commit_transfer_to_supplier, path: '/commit-transfer-to-supplier'
    end
  end

  resources :dashboards do    
    collection do
      get :supplier_index
      post :upload
    end
  end

  resources :users do
    member do
      get   :activate
      get   :assigned_suppliers
      get   :unassigned_suppliers
      get   :recover_password, path: '/recover-password'
      post  :assigner
      post  :unassigner
    end
    collection do
      get   :view_api_key, path: '/view-api-key'
      get   :forget_password, path: '/forget-password'
      get   :change_password, path: '/change-password'
      put   :change_password, path: '/change-password'
      post  :send_password
      post  :reset_password
    end
  end

  resources :sessions do
    collection do
      post :locate_subdomain
    end
  end

  namespace :admin do
    get '' => 'sessions#new'
    resources :sessions
    resources :dashboards
    resources :accounts do
      member {
        get :run_indexing
      }
    end
    resources :users    
  end

  resources :transactions do
    collection {
      get :historical_data, path: '/historical-data'
      get :sales_and_inventory_data, path: '/sales-and-inventory'
      get :inventory_sales_ratio, path: '/inventory-sales-ratio'
    }
  end

  namespace :api do
    namespace :v1 do
      resources :purchase_orders, only: [:index,:show, :received], path: '/purchase_orders' do
        collection do
          post 'received'
        end
      end
    end
    namespace :v2 do
      resources :purchase_orders, only: [:index], path: '/purchase_orders'
    end
  end

  match 'active'  => 'sessions#active',  via: :get
  match 'timeout' => 'sessions#timeout', via: :get


end
